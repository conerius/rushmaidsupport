export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';

export const GET_TOTAL_CLEANING = 'GET_TOTAL_CLEANING';

export const SET_BOOKING_DATE = 'SET_BOOKING_DATE';

export const SET_BOOKING_TIME = 'SET_BOOKING_TIME';

export const GET_BOOKING_TOTAL_PRICE = 'GET_BOOKING_TOTAL_PRICE';

export const CREATE_BOOKING_REQUEST = 'CREATE_BOOKING_REQUEST';
export const CREATE_BOOKING_SUCCESS = 'CREATE_BOOKING_SUCCESS';
export const CREATE_BOOKING_FAIL = 'CREATE_BOOKING_FAIL';

export const CREATE_FREE_BOOKING_REQUEST = 'CREATE_FREE_BOOKING_REQUEST';
export const CREATE_FREE_BOOKING_SUCCESS = 'CREATE_FREE_BOOKING_SUCCESS';
export const CREATE_FREE_BOOKING_FAIL = 'CREATE_FREE_BOOKING_FAIL';

export const SET_BOOKING_PROMO_CODE = 'SET_BOOKING_PROMO_CODE';
export const SET_BOOKING_PROMO_CODE_ID = 'SET_BOOKING_PROMO_CODE_ID';

export const SET_BOOKING_DISCOUNT = 'SET_BOOKING_DISCOUNT';
