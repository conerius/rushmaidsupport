import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

firebase.initializeApp({
  apiKey: 'AIzaSyBtiYTh9xpyyCTGyYJZlb2qPK5MqINOpUc',
  authDomain: 'rushmaid-test.firebaseapp.com',
  databaseURL: 'https://rushmaid-test.firebaseio.com',
  projectId: 'rushmaid-test',
  storageBucket: 'rushmaid-test.appspot.com',
  messagingSenderId: '576088688234',
  appId: '1:576088688234:web:4dceef8165ad377dd46c13',
  measurementId: 'G-F5780DTLF2',
});

// const firebaseConfig = {
//   apiKey: 'AIzaSyAyzlrfZRMc1jluKfhVyWaAsa6J40MiBik',
//   authDomain: 'rushmaid-7c161.firebaseapp.com',
//   databaseURL: 'https://rushmaid-7c161.firebaseio.com',
//   projectId: 'rushmaid-7c161',
//   storageBucket: 'rushmaid-7c161.appspot.com',
//   messagingSenderId: '1082119479870',
//   appId: '1:1082119479870:web:ce1621a4c9c6508be1a76f',
//   measurementId: 'G-1PBTPRYD76',
// };

export const getUser = (uid) => firebase.database().ref(`/users/${uid}`);

export const auth = firebase.auth();
export const database = firebase.database();
export const authUninvoked = firebase.auth;
