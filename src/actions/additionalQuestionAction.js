import {
  GET_BOOKING_ADDRESS,
  GET_QUESTIONS,
} from '../constants/additionalQuestionConstants';

import { geocodeByAddress, getLatLng } from 'react-places-autocomplete';

export const handleSelectComplete = (value) => async (dispatch) => {
  const result = await geocodeByAddress(value);
  const latlng = await getLatLng(result[0]);

  dispatch({
    type: GET_BOOKING_ADDRESS,
    payload: {
      address: value,
      coordinates: latlng,
    },
  });
};

export const getQuestionsAll =
  (questions, pets, airBnb, zipCode) => async (dispatch) => {
    dispatch({
      type: GET_QUESTIONS,
      payload: {
        questions,
        pets,
        airBnb,
        zipCode,
      },
    });
  };
