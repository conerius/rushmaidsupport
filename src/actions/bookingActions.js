import axios from 'axios';
import {
  INCREMENT,
  DECREMENT,
  GET_TOTAL_CLEANING,
  SET_BOOKING_DATE,
  SET_BOOKING_TIME,
  GET_BOOKING_TOTAL_PRICE,
  CREATE_BOOKING_REQUEST,
  CREATE_BOOKING_SUCCESS,
  CREATE_BOOKING_FAIL,
  CREATE_FREE_BOOKING_REQUEST,
  CREATE_FREE_BOOKING_SUCCESS,
  CREATE_FREE_BOOKING_FAIL,
  SET_BOOKING_PROMO_CODE,
  SET_BOOKING_PROMO_CODE_ID,
  SET_BOOKING_DISCOUNT,
} from '../constants/bookingConstants';

import moment from 'moment';

const API_URL = process.env.REACT_APP_API_URL;

const setMessage = (message, history) => {
  const confirm = window.confirm(message);
  if (confirm) {
    // window.open(data.data, "_blank");
    history.push('/AllAppointments');
  } else {
    return false;
  }
};

export const increment = (id) => (dispatch) => {
  dispatch({
    type: INCREMENT,
    payload: id,
  });
};

export const decrement = (id) => (dispatch) => {
  dispatch({
    type: DECREMENT,
    payload: id,
  });
};

export const getTotalCleaning = (history) => (dispatch, getState) => {
  const { bookings } = getState();

  let total = 0;

  bookings.tabs.forEach((tab) => {
    total = total + tab.total;
  });

  dispatch({
    type: GET_TOTAL_CLEANING,
    payload: total,
  });

  history.push(`/ExtraServices/${bookings.tabs[0].num}`);
};

export const setBookingDate = (date) => (dispatch) => {
  dispatch({
    type: SET_BOOKING_DATE,
    payload: date,
  });
};

export const setBookingTime = (time) => (dispatch) => {
  dispatch({
    type: SET_BOOKING_TIME,
    payload: time,
  });
};

export const getBookingTotalPrice =
  (totalCleaning, extraServicesTotal, pets, airBnb, discount) => (dispatch) => {
    let totalPrice = 0;

    totalPrice =
      totalCleaning + extraServicesTotal + (pets ? 10 : 0) + (airBnb ? 10 : 0);

    if (discount) {
      let totalValue = (discount / 100) * totalPrice;
      totalPrice = totalPrice - totalValue;
    }

    dispatch({
      type: GET_BOOKING_TOTAL_PRICE,
      payload: totalPrice,
    });
  };

export const createBooking =
  (
    customer,
    user,
    date,
    address,
    lng,
    lat,
    numOfBedrooms,
    numOfBathrooms,
    numOfKitchens,
    numOfLivingAreas,
    numOfBalconies,
    additionalServiceIDs,
    price,
    promoCodeId,
    questions,
    zipCode,
    history
  ) =>
  async (dispatch, getState) => {
    try {
      dispatch({
        type: CREATE_BOOKING_REQUEST,
      });

      const { userInfo } = getState();

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${userInfo.user.token}`,
        },
      };

      const timestamp = moment(date).unix();

      const booking = {
        stripe: {
          amount: 50,
          source: customer.default_source,
          description: 'iOS customer booking',
          receiptEmail: user.email,
          customer: customer.id,
        },
        booking: {
          date: timestamp,
          address: address,
          longitude: lng,
          latitude: lat,
          numberOfBedrooms: numOfBedrooms,
          numberOfBathrooms: numOfBathrooms,
          numberOfKitchens: numOfKitchens,
          numberOfLivingAreas: numOfLivingAreas,
          numberOfBalconies: numOfBalconies,
          additionalServiceIDs: additionalServiceIDs,
          price: price,
          userUID: user.uid,
          status: 'notCleaning',
          details: '',
          promo_code_id: promoCodeId ? promoCodeId : 'autoID',
          questions: questions,
          zipCode: zipCode.toString(),
        },
      };

      const { data } = await axios.post(
        `${API_URL}/booking/create`,
        booking,
        config
      );

      dispatch({
        type: CREATE_BOOKING_SUCCESS,
        payload: {
          data: data.data,
          message: data.message,
        },
      });

      setMessage(data.message, history);
    } catch (error) {
      dispatch({
        type: CREATE_BOOKING_FAIL,
        payload: error.response && error.response.data.message,
      });
    }
  };

export const createFreeBooking =
  (
    user,
    date,
    address,
    lng,
    lat,
    numOfBedrooms,
    numOfBathrooms,
    numOfKitchens,
    numOfLivingAreas,
    numOfBalconies,
    additionalServiceIDs,
    price,
    promoCodeId,
    questions,
    zipCode,
    history
  ) =>
  async (dispatch, getState) => {
    try {
      dispatch({
        type: CREATE_FREE_BOOKING_REQUEST,
      });

      const { userInfo } = getState();

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${userInfo.user.token}`,
        },
      };

      const timestamp = moment(date).unix();

      const booking = {
        booking: {
          date: timestamp,
          address: address,
          longitude: lng,
          latitude: lat,
          numberOfBedrooms: numOfBedrooms,
          numberOfBathrooms: numOfBathrooms,
          numberOfKitchens: numOfKitchens,
          numberOfLivingAreas: numOfLivingAreas,
          numberOfBalconies: numOfBalconies,
          additionalServiceIDs: additionalServiceIDs,
          price: price,
          userUID: user.uid,
          status: 'notCleaning',
          details: '',
          promo_code_id: promoCodeId ? promoCodeId : 'autoID',
          questions: questions,
          zipCode: zipCode.toString(),
        },
      };

      const { data } = await axios.post(
        `${API_URL}/booking/create-payless`,
        booking,
        config
      );

      dispatch({
        type: CREATE_FREE_BOOKING_SUCCESS,
        payload: {
          data: data.data,
          message: data.message,
        },
      });

      setMessage(data.message, history);
    } catch (error) {
      dispatch({
        type: CREATE_FREE_BOOKING_FAIL,
        payload: error.response && error.response.data.message,
      });
    }
  };

export const setBookingPromoCode = (promoCode) => async (dispatch) => {
  dispatch({
    type: SET_BOOKING_PROMO_CODE,
    payload: promoCode,
  });
};

export const setBookingPromoCodeId = (id) => async (dispatch) => {
  dispatch({
    type: SET_BOOKING_PROMO_CODE_ID,
    payload: id,
  });
};

export const setBookingDiscount = (dicount) => async (dispatch) => {
  dispatch({
    type: SET_BOOKING_DISCOUNT,
    payload: dicount,
  });
};
