import axios from 'axios';
import {
  LOGIN_USER_REQUEST,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  REGISTER_USER_REQUEST,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_FAIL,
  UPDATE_PROFILE_REQUEST,
  UPDATE_PROFILE_SUCCESS,
  UPDATE_PROFILE_FAIL,
  UPDATE_IMAGE_REQUEST,
  UPDATE_IMAGE_SUCCESS,
  UPDATE_IMAGE_FAIL,
} from '../constants/authConstants';

import { auth, database, authUninvoked } from '../config/firebase';

import getTokens, { getRefreshToken } from '../localStorage-helper';

const API_URL = process.env.REACT_APP_API_URL;

export const login = (email, password, history) => async (dispatch) => {
  try {
    dispatch({
      type: LOGIN_USER_REQUEST,
    });

    // const user = await auth.signInWithEmailAndPassword(email, password);

    // const config = {
    //   headers: {
    //     'Content-Type': 'application/json',
    //     Authorization: `Bearer ${user.user.za}`,
    //   },
    // };

    // const newToken = await user.user.getIdToken(true);

    const { data } = await axios.post(`${API_URL}/auth/sign-in/support`, {
      email,
      password,
    });

    // console.log(data);

    const token = data.data.customToken;

    // console.log(token);

    if (data?.data?.user?.role === 'support') {
      auth
        .signInWithCustomToken(token)
        .then((userCredential) => {
          userCredential?.user
            ?.getIdToken(/* forceRefresh */ true)
            .then((idToken) => {
              const user = userCredential?.user;

              const responseUser = {
                // ...data.data,
                token: token,
                refreshToken: user.refreshToken,
                idToken: idToken,
                uid: user?.uid,
              };

              // console.log(responseUser);

              window.location.href = '/list-of-all-jobs';
              dispatch({
                type: LOGIN_USER_SUCCESS,
                payload: responseUser,
              });

              localStorage.setItem(
                'userRushmaidSupport',
                JSON.stringify(responseUser)
              );
            })
            .catch(function (error) {
              console.log(error);
            });
          // Signed in
        })
        .catch((error) => {
          const errorCode = error.code;
          const errorMessage = error.message;
          dispatch({
            type: LOGIN_USER_FAIL,
            payload: errorMessage,
          });
        });
    }

    // if (responseUser.role === 'user') {
    //   if (!responseUser.phoneVerified) {
    //     window.location.href = '/phoneNumber';
    //   } else {
    //     window.location.href = '/BookNow';
    //     dispatch({
    //       type: LOGIN_USER_SUCCESS,
    //       payload: responseUser,
    //     });
    //   }
    //   localStorage.setItem('userRushmaid', JSON.stringify(responseUser));
    // } else if (responseUser.role === 'admin') {
    //   dispatch({
    //     type: LOGIN_USER_FAIL,
    //     payload: 'This account is not allowed to log in with admin role',
    //   });
    // } else if (responseUser.role === 'maid') {
    //   dispatch({
    //     type: LOGIN_USER_FAIL,
    //     payload: 'This account is not allowed to log in with maid role',
    //   });
    // }
  } catch (error) {
    console.log(error);
    dispatch({
      type: LOGIN_USER_FAIL,
      payload: error.message && error.message,
    });
  }
};

export const refreshToken = () => async () => {
  try {
    const token = getRefreshToken();

    await auth
      .signInWithCustomToken(token)
      .then((userCredential) => {
        userCredential?.user
          ?.getIdToken(/* forceRefresh */ true)
          .then((idToken) => {
            const user = userCredential?.user;

            const responseUser = {
              // ...data.data,
              token: token,
              refreshToken: user.refreshToken,
              idToken: idToken,
            };

            console.log(responseUser);

            localStorage.setItem(
              'userRushmaidSupport',
              JSON.stringify(responseUser)
            );
          })
          .catch(function (error) {
            console.log(error);
          });
        // Signed in
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
      });
  } catch (er) {
    console.log(er);
  }
};

const asyncLocalStorageRemove = {
  setItem: function () {
    return Promise.resolve().then(function () {
      localStorage.removeItem('userRushmaidSupport');
    });
  },
};

export const logout = () => {
  auth.signOut().then(() => {
    asyncLocalStorageRemove.setItem('userRushmaidSupport').then(function () {
      window.location.href = '/';
    });
  });
};

export const signUp =
  (email, password, userObject, history) => async (dispatch) => {
    try {
      dispatch({
        type: REGISTER_USER_REQUEST,
      });

      const data = await auth.createUserWithEmailAndPassword(email, password);

      const {
        user: { uid },
      } = data;

      const usersRef = database.ref('users');

      delete userObject.password;

      usersRef
        .child(uid)
        .set({
          ...userObject,
          uid,
          role: 'user',
        })

        .catch((e) => {
          console.log(e.message);
          //setAuthError(e.message);
        });

      const userObj = {
        ...userObject,
        uid,
        role: 'user',
      };

      dispatch({
        type: REGISTER_USER_SUCCESS,
        payload: userObj,
      });

      history.push('/zipCode');

      localStorage.setItem('userRushmaidSupport', JSON.stringify(userObj));
    } catch (error) {
      dispatch({
        type: REGISTER_USER_FAIL,
        payload: error && error.message,
      });
    }
  };

export const appleLogin = () => async (dispatch) => {
  try {
    const provider = new authUninvoked.OAuthProvider('apple.com');

    dispatch({
      type: LOGIN_USER_REQUEST,
    });

    const user = await auth.signInWithPopup(provider);

    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${user.user.za}`,
      },
    };

    const newToken = await user.user.getIdToken(true);

    const { data } = await axios.get(
      `${API_URL}/user/${user.user.uid}`,
      config
    );

    const responseUser = {
      token: newToken,
      email: user.user.email,
      firstName: data.data.firstName ? data.data.firstName : null,
      imageURL: data.data.imageURL ? data.data.imageURL : null,
      lastName: data.data.lastName ? data.data.lastName : null,
      phoneNumber: user.user.phoneNumber,
      phoneVerified: false,
      referralCode: '',
      role: 'user',
      uid: user.user.uid,
      zipCode: data.data.zipCode ? data.data.zipCode : null,
    };

    dispatch({
      type: LOGIN_USER_SUCCESS,
      payload: responseUser,
    });

    localStorage.setItem('userRushmaidSupport', JSON.stringify(responseUser));

    if (!responseUser.phoneNumber) {
      window.location.href = '/phoneNumber';
    } else {
      window.location.href = '/';
    }
  } catch (error) {
    dispatch({
      type: LOGIN_USER_FAIL,
      payload: error.message && error.message,
    });
  }
};

export const facebookLogin = () => async (dispatch) => {
  try {
    const provider = new authUninvoked.FacebookAuthProvider();

    dispatch({
      type: LOGIN_USER_REQUEST,
    });

    const user = await auth.signInWithPopup(provider);

    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${user.user.za}`,
      },
    };

    const newToken = await user.user.getIdToken(true);

    const { data } = await axios.get(
      `${API_URL}/user/${user.user.uid}`,
      config
    );

    if (data.data) {
      const responseUser = {
        token: newToken,
        email: data.data.email,
        firstName: data.data.firstName,
        imageURL: data.data.imageURL,
        lastName: data.data.lastName,
        phoneNumber: user.user.phoneNumber,
        phoneVerified: false,
        referralCode: '',
        role: 'user',
        uid: user.user.uid,
        zipCode: data.data.zipCode,
      };

      dispatch({
        type: LOGIN_USER_SUCCESS,
        payload: responseUser,
      });

      if (!responseUser.phoneNumber) {
        window.location.href = '/phoneNumber';
      } else {
        window.location.href = '/';
      }

      localStorage.setItem('userRushmaidSupport', JSON.stringify(responseUser));
    } else {
      const responseUser = {
        token: newToken,
        email: user.user.email,
        firstName: user.user.displayName.split(' ')[0],
        imageURL: user.user.photoURL,
        lastName: user.user.displayName.split(' ')[1],
        phoneNumber: user.user.phoneNumber,
        phoneVerified: false,
        referralCode: '',
        role: 'user',
        uid: user.user.uid,
        zipCode: null,
      };

      dispatch({
        type: LOGIN_USER_SUCCESS,
        payload: responseUser,
      });

      localStorage.setItem('userRushmaidSupport', JSON.stringify(responseUser));
    }

    // if (!responseUser.phoneNumber) {
    //   window.location.href = '/phoneNumber';
    // } else {
    // window.location.href = '/';
    // }
  } catch (error) {
    dispatch({
      type: LOGIN_USER_FAIL,
      payload: error.message && error.message,
    });
  }
};

export const updateUserProfile =
  (fullName, email, zip, user) => async (dispatch, getState) => {
    try {
      dispatch({
        type: UPDATE_PROFILE_REQUEST,
      });

      const { userInfo } = getState();

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${userInfo.user.token}`,
        },
      };

      const firstName = fullName.split(' ')[0];
      const lastName = fullName.split(' ')[1];

      const newUser = {
        firstName,
        lastName,
        email,
        zipCode: zip ? zip : userInfo.user.zipCode,
        userId: userInfo.user.uid,
        imageUrl: user.imageURL,
      };

      const { data } = await axios.put(`${API_URL}/user`, newUser, config);

      const responseUser = {
        token: userInfo.user.token,
        email: data.data.email,
        firstName: data.data.firstName,
        imageURL: userInfo.user.imageURL,
        lastName: data.data.lastName,
        phoneNumber: userInfo.user.phoneNumber,
        phoneVerified: userInfo.user.phoneVerified,
        referralCode: userInfo.user.referralCode,
        role: userInfo.user.role,
        uid: userInfo.user.uid,
        zipCode: data.data.zipCode,
      };

      dispatch({
        type: UPDATE_PROFILE_SUCCESS,
        payload: responseUser,
      });

      localStorage.setItem('userRushmaidSupport', JSON.stringify(responseUser));
    } catch (error) {
      console.log(error);
      dispatch({
        type: UPDATE_PROFILE_FAIL,
        payload: error.response && error.response.data.message,
      });
    }
  };

export const updateProfileImage =
  (extension, dataUrl, user) => async (dispatch, getState) => {
    try {
      dispatch({
        type: UPDATE_IMAGE_REQUEST,
      });

      const { userInfo } = getState();

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${userInfo.user.token}`,
        },
      };

      const dataImg = {
        extension,
        data: dataUrl,
        userId: user.uid,
      };

      const { data } = await axios.post(
        `${API_URL}/user/image`,
        dataImg,
        config
      );

      dispatch({
        type: UPDATE_IMAGE_SUCCESS,
        payload: data.data,
      });
    } catch (error) {
      dispatch({
        type: UPDATE_IMAGE_FAIL,
        payload: error.response && error.response.data.message,
      });
    }
  };
