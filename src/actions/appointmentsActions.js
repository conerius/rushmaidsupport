import axios from 'axios';

import {
  GET_APPOINTMENTS_REQUEST,
  GET_APPOINTMENTS_SUCCESS,
  GET_APPOINTMENTS_FAIL,
} from '../constants/appointmentConstants';

const API_URL = process.env.REACT_APP_API_URL;

export const getAppointments = () => async (dispatch, getState) => {
  try {
    dispatch({
      type: GET_APPOINTMENTS_REQUEST,
    });

    const { userInfo } = getState();

    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userInfo.user.token}`,
      },
    };

    const { data } = await axios.get(`${API_URL}/booking/get/all`, config);

    dispatch({
      type: GET_APPOINTMENTS_SUCCESS,
      payload: data.data,
    });
  } catch (error) {
    dispatch({
      type: GET_APPOINTMENTS_FAIL,
      payload: error.response && error.response.data.message,
    });
  }
};
