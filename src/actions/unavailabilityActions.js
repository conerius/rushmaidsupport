import axios from 'axios';
import { auth } from '../config/firebase';

import {
  GET_UNAVAILABILITY_REQUEST,
  GET_UNAVAILABILITY_SUCCESS,
  GET_UNAVAILABILITY_FAIL,
} from '../constants/unavailabilityConstants';
import { refreshToken } from './authActions';

const API_URL = process.env.REACT_APP_API_URL;

export const getUnavailability =
  (id, from, to) => async (dispatch, getState) => {
    try {
      dispatch({
        type: GET_UNAVAILABILITY_REQUEST,
      });
      const { userInfo } = getState();
      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${userInfo.user.idToken}`,
        },
      };

      // refreshToken();

      const { data } = await axios.get(
        `${API_URL}/unavailability?maidId=${id}&from=${from}&to=${to}`
        // config
      );
      console.log(data);
      dispatch({
        type: GET_UNAVAILABILITY_SUCCESS,
        payload: data.data,
      });
    } catch (error) {
      dispatch({
        type: GET_UNAVAILABILITY_FAIL,
        payload: error.response && error.response.data.message,
      });
    }
  };
