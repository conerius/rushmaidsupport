import axios from 'axios';
import { auth, database } from '../config/firebase';

import {
  GET_CONVERSATIONS_REQUEST,
  GET_CONVERSATIONS_SUCCESS,
  GET_CONVERSATIONS_FAIL,
  SEND_IMAGE_FAIL,
  SEND_IMAGE_REQUEST,
  SEND_IMAGE_SUCCESS,
  SEND_MESSAGE_REQUEST,
  SEND_MESSAGE_SUCCESS,
  SEND_MESSAGE_FAIL,
  ADD_ADMIN_FAIL,
  ADD_ADMIN_REQUEST,
  ADD_ADMIN_SUCCESS,
} from '../constants/chatConstants';

import { CLEAR_ALERT, CLEAR_MESSAGE } from '../constants/alertConstants';

import { refreshToken } from './authActions';

const API_URL = process.env.REACT_APP_API_URL;

export const getConversations = () => async (dispatch, getState) => {
  try {
    dispatch({
      type: GET_CONVERSATIONS_REQUEST,
    });
    const { userInfo } = getState();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userInfo.user.idToken}`,
      },
    };

    // refreshToken();
    const ref = database.ref('users/conversations');

    const query = ref;

    query.once('value', function (snapshot) {
      snapshot.forEach(function (child) {
        console.log(child.val());
      });
    });

    // console.log();
    dispatch({
      type: GET_CONVERSATIONS_SUCCESS,
      // payload: data.data,
    });
  } catch (error) {
    dispatch({
      type: GET_CONVERSATIONS_FAIL,
      payload: error.response && error.response.data.message,
    });
  }
};

export const setLastMessageRead =
  (user, conversation) => async (dispatch, getState) => {
    try {
      // dispatch({
      //   type: SET_MESSAGE_READ_REQUEST,
      // });

      const { userInfo } = getState();

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${userInfo.user.token}`,
        },
      };

      const { data } = await axios.put(
        `${API_URL}/chat/message/read`,
        {
          userId: user,
          conversationId: conversation,
        },
        config
      );

      console.log();
      // dispatch({
      //   type: SET_MESSAGE_READ_SUCCESS,
      //   // payload: data.data,
      // });
    } catch (error) {
      // dispatch({
      //   type: SET_MESSAGE_READ_FAIL,
      //   payload: error.response && error.response.data.message,
      // });
      console.log(error);
    }
  };

export const addAdmin = (conversation) => async (dispatch, getState) => {
  try {
    dispatch({
      type: ADD_ADMIN_REQUEST,
    });

    const { userInfo } = getState();

    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userInfo.user.token}`,
      },
    };

    const { data } = await axios.put(
      `${API_URL}/chat/support/add-admin`,
      {
        conversationId: conversation,
      },
      config
    );

    console.log();
    dispatch({
      type: ADD_ADMIN_SUCCESS,
      payload: data.message,
    });

    setTimeout(() => {
      dispatch({
        type: CLEAR_MESSAGE,
        payload: '',
      });
    }, 3000);
  } catch (error) {
    dispatch({
      type: ADD_ADMIN_FAIL,
      payload: error.response && error.response.data.message,
    });
    setTimeout(() => {
      dispatch({
        type: CLEAR_ALERT,
        payload: '',
      });
    }, 3000);
  }
};

export const sendMessageChat =
  (senderId, conversationId, contentType, message) =>
  async (dispatch, getState) => {
    try {
      dispatch({
        type: SEND_MESSAGE_REQUEST,
      });

      const { userInfo } = getState();

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${userInfo.user.token}`,
        },
      };

      const msg = () => {
        if (contentType === 0) {
          const dataMessage = {
            senderId,
            conversationId,
            contentType,
            message,
          };
          return dataMessage;
        } else {
          const dataMessage = {
            senderId,
            conversationId,
            contentType,
            imageUrl: message,
          };
          return dataMessage;
        }
      };

      const { data } = await axios.post(
        `${API_URL}/chat/message/send`,
        msg(),
        config
      );

      dispatch({
        type: SEND_MESSAGE_SUCCESS,
        payload: data.data,
      });
    } catch (error) {
      dispatch({
        type: SEND_MESSAGE_FAIL,
        payload: error.response && error.response.data.message,
      });
    }
  };

export const sendImageChat =
  (extension, dataUrl, user, conversationId) => async (dispatch, getState) => {
    try {
      dispatch({
        type: SEND_IMAGE_REQUEST,
      });

      const { userInfo } = getState();

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${userInfo.user.token}`,
        },
      };

      const dataImg = {
        extension,
        data: dataUrl,
        userId: user,
      };

      const { data } = await axios.post(
        `${API_URL}/chat/image`,
        dataImg,
        config
      );

      dispatch(sendMessageChat(user, conversationId, 1, data?.data));

      dispatch({
        type: SEND_IMAGE_SUCCESS,
        payload: data.data,
      });
    } catch (error) {
      dispatch({
        type: SEND_IMAGE_FAIL,
        payload: error.response && error.response.data.message,
      });
    }
  };
