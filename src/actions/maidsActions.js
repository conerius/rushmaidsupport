import axios from 'axios';
import { auth } from '../config/firebase';

import {
  GET_MAIDS_REQUEST,
  GET_MAIDS_SUCCESS,
  GET_MAIDS_FAIL,
} from '../constants/maidsConstants';

const API_URL = process.env.REACT_APP_API_URL;

export const getMaids = () => async (dispatch, getState) => {
  try {
    dispatch({
      type: GET_MAIDS_REQUEST,
    });

    const { userInfo } = getState();

    // console.log(userInfo.user.idToken);

    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userInfo.user.idToken}`,
      },
    };
    const { data } = await axios.get(`${API_URL}/user/maids/get/all`, config);

    // console.log(data);
    // console.log(config);

    dispatch({
      type: GET_MAIDS_SUCCESS,
      payload: data.data,
    });

    // const token = await auth?.currentUser
    //   ?.getIdToken(/* forceRefresh */ true)
    //   .then((idToken) => {
    //     const config = {
    //       headers: {
    //         'Content-Type': 'application/json',
    //         Authorization: `Bearer ${idToken && idToken}`,
    //       },
    //     };

    //     const { data } = axios.get(`${API_URL}/user/maids/get/all`, config);

    //     console.log(idToken);
    //     console.log('THIS IS DATA' + data);

    //     // dispatch({
    //     //   type: GET_MAIDS_SUCCESS,
    //     //   payload: data.data,
    //     // });
    //   })
    //   .catch(function (error) {
    //
    //     console.log(error);
    //   });

    // console.log(token);
  } catch (error) {
    dispatch({
      type: GET_MAIDS_FAIL,
      payload: error.response && error.response.data.message,
    });
    // console.log(error);
  }
};
