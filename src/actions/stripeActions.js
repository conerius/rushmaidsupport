import axios from 'axios';
import {
  GET_CUSTOMER_REQUEST,
  GET_CUSTOMER_SUCCESS,
  GET_CUSTOMER_FAIL,
  CREATE_CUSTOMER_REQUEST,
  CREATE_CUSTOMER_SUCCESS,
  CREATE_CUSTOMER_FAIL,
  CREATE_CREDIT_CARD_REQUEST,
  CREATE_CREDIT_CARD_SUCCESS,
  CREATE_CREDIT_CARD_FAIL,
} from '../constants/stripeConstants';

const API_KEY = process.env.REACT_APP_API_URL;

export const getCustomer = (customerId) => async (dispatch, getState) => {
  try {
    dispatch({
      type: GET_CUSTOMER_REQUEST,
    });

    const { userInfo } = getState();

    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userInfo.user.token}`,
      },
    };

    const { data } = await axios.get(
      `${API_KEY}/stripe/customer/${customerId}`,
      config
    );

    dispatch({
      type: GET_CUSTOMER_SUCCESS,
      payload: data.data,
    });
  } catch (error) {
    dispatch({
      type: GET_CUSTOMER_FAIL,
      payload: error.response && error.response.data,
    });
  }
};

export const getCardToken = async (
  user,
  cardNumber,
  cardExpireDate,
  cardCvc
) => {
  try {
    const cardInfo = {
      cardNumber,
      expireMonth: cardExpireDate.split('/')[0],
      expireYear: cardExpireDate.split('/')[1],
      cvc: cardCvc,
    };

    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${user.token}`,
      },
    };

    const { data } = await axios.post(
      `${API_KEY}/stripe/get-token`,
      cardInfo,
      config
    );

    return data.data.id;
  } catch (error) {
    return error.response.data;
  }
};

export const createCustomer = async (user, source, dispatch) => {
  try {
    dispatch({
      type: CREATE_CUSTOMER_REQUEST,
    });

    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${user.token}`,
      },
    };

    const customerInfo = {
      name: user.firstName === null ? '' : user.firstName,
      email: user.email,
      source,
      description: 'iOS Customer',
    };

    const { data } = await axios.post(
      `${API_KEY}/stripe/customer/create`,
      customerInfo,
      config
    );

    dispatch({
      type: CREATE_CUSTOMER_SUCCESS,
      payload: data.data.id,
    });

    localStorage.setItem('customer', JSON.stringify(data.data.id));

    return data.data;
  } catch (error) {
    dispatch({
      type: CREATE_CUSTOMER_FAIL,
      payload: error.response && error.response.data,
    });
  }
};

export const createCreditCard =
  (user, customer, cardTokenId) => async (dispatch) => {
    try {
      dispatch({
        type: CREATE_CREDIT_CARD_REQUEST,
      });

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.token}`,
        },
      };

      const cardInfo = {
        customerId: customer.id,
        source: cardTokenId,
      };

      const { data } = await axios.post(
        `${API_KEY}/stripe/customer/create-credit-card`,
        cardInfo,
        config
      );

      dispatch({
        type: CREATE_CREDIT_CARD_SUCCESS,
      });

      // localStorage.setItem('customer', JSON.stringify(data.data));
    } catch (error) {
      dispatch({
        type: CREATE_CREDIT_CARD_FAIL,
        payload: error.response && error.response.data,
      });
    }
  };
