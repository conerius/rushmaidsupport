import axios from 'axios';
import {
  CREATE_DISINFECTION_REQUEST,
  CREATE_DISINFECTION_SUCCESS,
  CREATE_DISINFECTION_FAIL,
  SET_DISINFECTION_DATE,
  SET_DISINFECTION_TIME,
  SET_DISINFECTION_ADDRESS,
  SET_DISINFECTION_TOTAL_PRICE,
  SET_DISINFECTION_ZIP_CODE,
  SET_DISINFECTION_PROMO_CODE,
  SET_DISINFECTION_FEETS,
  SET_DISINFECTION_DISCOUNT,
  SET_DISINFECTION_PROMO_CODE_ID,
  CREATE_DISINFECTION_PAYLESS_REQUEST,
  CREATE_DISINFECTION_PAYLESS_SUCCESS,
  CREATE_DISINFECTION_PAYLESS_FAIL,
} from '../constants/disnifectionConstants';

import { geocodeByAddress, getLatLng } from 'react-places-autocomplete';

const API_URL = process.env.REACT_APP_API_URL;

const setMessage = (message, history) => {
  const confirm = window.confirm(message);
  if (confirm) {
    // window.open(data.data, "_blank");
    history.push('/AllAppointments');
  } else {
    return false;
  }
};

export const createDisinfection =
  (
    date,
    feets,
    address,
    longitude,
    latitude,
    user,
    customer,
    promoCodeId,
    totalPrice,
    zipCode,
    history
  ) =>
  async (dispatch, getState) => {
    try {
      dispatch({
        type: CREATE_DISINFECTION_REQUEST,
      });

      const { userInfo } = getState();

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${userInfo.user.token}`,
        },
      };

      const disinfectionInfo = {
        date,
        feets,
        address,
        longitude,
        latitude,
        userUID: user.uid,
        credit: user.credit ? user.credit : 0,
        receipt_email: user.email,
        description: 'Web customer disinfection booking',
        customer_id: customer.id,
        source_id: customer.default_source,
        promo_code_id: promoCodeId ? promoCodeId : 'promoID123',
        price: totalPrice,
        zip_code: zipCode.toString(),
      };

      const { data } = await axios.post(
        `${API_URL}/disinfection/create`,
        disinfectionInfo,
        config
      );

      dispatch({
        type: CREATE_DISINFECTION_SUCCESS,
        payload: {
          data: data.data,
          message: data.message,
        },
      });

      setMessage(data.message, history);

      dispatch(setDisinfectionPromoCode(''));
      dispatch(setDisinfectionZipCode(''));
      dispatch(setDisinfectionFeets(0));
      dispatch(setDisinfectionTotalPrice(0));

      dispatch({
        type: SET_DISINFECTION_ADDRESS,
        payload: {
          address: '',
          coordinates: '',
        },
      });
    } catch (error) {
      dispatch({
        type: CREATE_DISINFECTION_FAIL,
        payload: error.response && error.response.data.message,
      });
    }
  };

export const createDisinfectionPayless =
  (
    date,
    feets,
    address,
    longitude,
    latitude,
    user,
    promoCodeId,
    zipCode,
    history
  ) =>
  async (dispatch, getState) => {
    try {
      dispatch({
        type: CREATE_DISINFECTION_PAYLESS_REQUEST,
      });

      const { userInfo } = getState();

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${userInfo.user.token}`,
        },
      };

      const disinfectionInfo = {
        date,
        feets,
        address,
        longitude,
        latitude,
        userUID: user.uid,
        promo_code_id: promoCodeId,
        zip_code: zipCode.toString(),
      };

      const { data } = await axios.post(
        `${API_URL}/disinfection/create-payless`,
        disinfectionInfo,
        config
      );

      dispatch({
        type: CREATE_DISINFECTION_PAYLESS_SUCCESS,
        payload: {
          data: data.data,
          message: data.message,
        },
      });

      setMessage(data.message, history);

      dispatch(setDisinfectionPromoCode(''));
      dispatch(setDisinfectionZipCode(''));
      dispatch(setDisinfectionFeets(0));
      dispatch(setDisinfectionTotalPrice(0));

      dispatch({
        type: SET_DISINFECTION_ADDRESS,
        payload: {
          address: '',
          coordinates: '',
        },
      });
    } catch (error) {
      dispatch({
        type: CREATE_DISINFECTION_PAYLESS_FAIL,
        payload: error.response && error.response.data.message,
      });
    }
  };

export const setDisinfectionFeets = (feets) => (dispatch) => {
  dispatch({
    type: SET_DISINFECTION_FEETS,
    payload: feets,
  });
};

export const setDisinfectionDate = (date) => (dispatch) => {
  dispatch({
    type: SET_DISINFECTION_DATE,
    payload: date,
  });
};

export const setDisinfectionTime = (time) => (dispatch) => {
  dispatch({
    type: SET_DISINFECTION_TIME,
    payload: time,
  });
};

export const handleSelectComplete = (value) => async (dispatch) => {
  const result = await geocodeByAddress(value);
  const latlng = await getLatLng(result[0]);

  dispatch({
    type: SET_DISINFECTION_ADDRESS,
    payload: {
      address: value,
      coordinates: latlng,
    },
  });
};

export const setDisinfectionZipCode = (zipCode) => async (dispatch) => {
  dispatch({
    type: SET_DISINFECTION_ZIP_CODE,
    payload: zipCode,
  });
};

export const setDisinfectionPromoCode = (promoCode) => async (dispatch) => {
  dispatch({
    type: SET_DISINFECTION_PROMO_CODE,
    payload: promoCode,
  });
};

export const setDisinfectionPromoCodeId = (id) => async (dispatch) => {
  dispatch({
    type: SET_DISINFECTION_PROMO_CODE_ID,
    payload: id,
  });
};

export const setDisinfectionDiscount = (dicount) => async (dispatch) => {
  dispatch({
    type: SET_DISINFECTION_DISCOUNT,
    payload: dicount,
  });
};

export const setDisinfectionTotalPrice = (price) => async (dispatch) => {
  dispatch({
    type: SET_DISINFECTION_TOTAL_PRICE,
    payload: price,
  });
};
