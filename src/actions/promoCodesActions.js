import axios from 'axios';
import {
  GET_PROMO_CODE_REQUEST,
  GET_PROMO_CODE_SUCCESS,
  GET_PROMO_CODE_FAIL,
} from '../constants/promoCodesConstants';

const API_URL = process.env.REACT_APP_API_URL;

export const getPromoCode = () => async (dispatch, getState) => {
  try {
    dispatch({
      type: GET_PROMO_CODE_REQUEST,
    });

    const { userInfo } = getState();

    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userInfo.user.token}`,
      },
    };

    const { data } = await axios.get(
      `${API_URL}/booking/promo-codes/${userInfo.user.uid}`,
      config
    );

    dispatch({
      type: GET_PROMO_CODE_SUCCESS,
      payload: data.data,
    });
  } catch (error) {
    dispatch({
      type: GET_PROMO_CODE_FAIL,
      payload: error.response && error.response.data,
    });
  }
};
