import axios from 'axios';
import { auth } from '../config/firebase';

import {
  GET_CUSTOMERS_REQUEST,
  GET_CUSTOMERS_SUCCESS,
  GET_CUSTOMERS_FAIL,
} from '../constants/customersConstants';
import { refreshToken } from './authActions';

const API_URL = process.env.REACT_APP_API_URL;

export const getCustomers = () => async (dispatch, getState) => {
  try {
    dispatch({
      type: GET_CUSTOMERS_REQUEST,
    });
    const { userInfo } = getState();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userInfo.user.idToken}`,
      },
    };

    // refreshToken();

    const { data } = await axios.post(
      `${API_URL}/user/admins/get-customers`
      // config
    );
    console.log(data);
    dispatch({
      type: GET_CUSTOMERS_SUCCESS,
      payload: data.data,
    });
  } catch (error) {
    dispatch({
      type: GET_CUSTOMERS_FAIL,
      payload: error.response && error.response.data.message,
    });
  }
};
