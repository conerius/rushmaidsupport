import axios from 'axios';
import {
  GET_CATEGORIES_REQUEST,
  GET_CATEGORIES_SUCCESS,
  GET_CATEGORIES_FAIL,
  CHECK_CATEGORY,
  GET_CATEGORIES_TOTAL,
  GET_SELECTED_INDEX,
} from '../constants/categoriesConstants';
import NProgress from 'nprogress';

const API_URL = process.env.REACT_APP_API_URL;

export const getCategories = (id) => async (dispatch) => {
  try {
    dispatch({
      type: GET_CATEGORIES_REQUEST,
    });

    NProgress.start();

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const { data } = await axios.get(`${API_URL}/category/${id}`, config);

    dispatch({
      type: GET_CATEGORIES_SUCCESS,
      payload: data.data.map((category) => ({ ...category, checked: false })),
    });

    NProgress.done();
  } catch (error) {
    NProgress.done();
    dispatch({
      type: GET_CATEGORIES_FAIL,
      payload: error.response && error.response.data,
    });
  }
};

export const checkCategory = (id) => (dispatch) => {
  dispatch({
    type: CHECK_CATEGORY,
    payload: id,
  });

  dispatch(getSelectedIndex());
};

export const getCategoriesTotal = (categories) => (dispatch, getState) => {
  let sum = 0;

  categories.forEach((category) => {
    if (category.checked) {
      sum += category.price;
    }
  });

  dispatch({
    type: GET_CATEGORIES_TOTAL,
    payload: sum,
  });
};

const getSelectedIndex = () => (dispatch, getState) => {
  const { categories } = getState();

  let categoryIndex = '';

  categories.categories.forEach((category) => {
    if (category.checked) {
      categoryIndex += category.selfID + ', ';
    }
  });

  const editedId = categoryIndex.slice(0, -2);

  dispatch({
    type: GET_SELECTED_INDEX,
    payload: editedId,
  });
};
