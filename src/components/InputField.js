import React from "react";
import styled from "styled-components";

const Input = styled.input`
width: 100%;
border: none;
background: none;
outline: none;
font-size: 14px;
`;

const InputContainer = styled.div`
background: linear-gradient(147.42deg, rgba(64, 72, 93, 0.4) 6.32%, rgba(96, 106, 130, 0.4) 92.25%), linear-gradient(128deg, #E6E7ED -79.65%, #F7F8FA 151.25%);
background-blend-mode: soft-light, normal;
box-shadow: -7px -7px 16px #FAFBFC, 4px 3px 19px #BDC1D1, inset -1px -1px 16px #F5F6FA, inset 1px 1px 16px #E9EAF2;
border-radius: 8px;
width: 100%;
height: 50px;
display: flex;
align-items: center;
padding: 0 19px;
margin: 0 0 20px 0;
`;

const InputField = (props) => {
  return (
    <InputContainer>
    <Input
        id={props.id}
        type={props.type}
        name={props.name}
        value={props.value}
        onChange={(e) => props.onChange(e.target.value)}
        label={props.label}
        placeholder={props.placeholder}
        required
      />
    </InputContainer>
  );
};

export default InputField;
