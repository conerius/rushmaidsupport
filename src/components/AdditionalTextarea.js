import React from 'react';
import styled from 'styled-components';

const Input = styled.textarea`
  width: 100%;
  border: none;
  background: none;
  outline: none;
  font-size: 14px;
  height: 100%;
  min-height: ${(props) => props.height};
`;

const InputContainer = styled.div`
  background: #ebecf0;
  box-shadow: 1px 1px 7px #ebecf0, inset -5px -4px 10px #fafbfc,
    inset 3px 3px 8px #bdc1d1;
  border-radius: 16px;
  width: 100%;
  height: 100%;
  padding: 17px 19px;
  margin: 0 0 20px 0;
`;

const TextArea = (props) => {
  return (
    <InputContainer type={props.type}>
      <Input
        id={props.id}
        type={props.type}
        name={props.name}
        value={props.value}
        onChange={props.onChange}
        label={props.label}
        placeholder={props.placeholder}
        height={props.height ? props.height : '150px'}
        disabled={props.disabled ? props.disabled : null}
      />
    </InputContainer>
  );
};

export default TextArea;
