import React, { useState, useEffect, useRef } from 'react';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';
import { CLEAR_ALERT } from '../constants/alertConstants';

// import danger from '../../assets/icons/danger.svg';
// import success from '../../assets/icons/success.svg';
// import x from '../../assets/icons/XSmall.svg';

const AlertContainer = styled.div`
  min-height: 55px;
  width: calc(100% - 32px);
  max-width: 500px;
  position: fixed;
  top: 30px;
  left: 0;
  right: 0;
  background: linear-gradient(
      147.42deg,
      rgba(64, 72, 93, 0.4) 6.32%,
      rgba(96, 106, 130, 0.4) 92.25%
    ),
    linear-gradient(128deg, #e6e7ed -79.65%, #f7f8fa 151.25%);
  background-blend-mode: soft-light, normal;
  box-shadow: -7px -7px 16px #fafbfc, 4px 3px 19px #bdc1d1,
    inset -1px -1px 16px #f5f6fa, inset 1px 1px 16px #e9eaf2;
  border-radius: 8px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-weight: 500;
  font-size: 15px;
  line-height: 18px;
  color: #212529;
  padding: 10px 15px;
  margin: 0 auto;
  z-index: 10000;
  transition: all 0.1s ease;
`;

const IconAndMessage = styled.div`
  display: flex;
  align-items: center;
  img {
    margin: 0 15px 0 0;
  }
`;

const Alert = (props) => {
  const [isVisible, setIsVisible] = useState(true);

  const dispatch = useDispatch();

  useEffect(() => {
    if (props.message) {
      setIsVisible(true);
    }
  }, [props.message]);

  function useOutsideAlerter(ref) {
    useEffect(() => {
      /**
       * Alert if clicked on outside of element
       */
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setIsVisible(false);
          props.setError && props.setError('');
          dispatch({
            type: CLEAR_ALERT,
            payload: '',
          });
        }
      }
      // Bind the event listener
      document.addEventListener('mousedown', handleClickOutside);
      return () => {
        // Unbind the event listener on clean up
        document.removeEventListener('mousedown', handleClickOutside);
      };
    }, [ref]);
  }

  const wrapperRef = useRef(null);
  useOutsideAlerter(wrapperRef);

  return (
    <AlertContainer
      ref={wrapperRef}
      style={{ display: isVisible ? 'flex' : 'none' }}
      onClick={() => {
        setIsVisible(false);
        props.setError && props.setError('');
        dispatch({
          type: CLEAR_ALERT,
          payload: '',
        });
      }}
    >
      <IconAndMessage>
        {/* <img
          src={props.icon === 'success' ? success : danger}
          alt='AlertIcon'
        /> */}
        {props.message}
      </IconAndMessage>
    </AlertContainer>
  );
};

export default Alert;
