import React from 'react';
import styled from 'styled-components';

const Input = styled.input`
  width: 100%;
  border: none;
  background: none;
  outline: none;
  font-size: 14px;
`;

const InputContainer = styled.div`
  background: #ebecf0;
  box-shadow: 1px 1px 7px #ebecf0, inset -5px -4px 10px #fafbfc,
    inset 3px 3px 8px #bdc1d1;
  border-radius: 16px;
  width: 100%;
  height: ${(props) => (props.type === 'textarea' ? '200px' : '50px')};
  display: flex;
  align-items: ${(props) =>
    props.type === 'textarea' ? 'flex-start' : 'center'};
  padding: 17px 19px;
  /* margin: 0 0 20px 0; */
`;

const Label = styled.p`
  font-weight: normal;
  font-size: 16px;
  margin: 0 0 15px 0 !important;
  color: #000;
`;

const InputFieldChat = (props) => {
  return (
    <>
      {props.label ? <Label>{props.label}</Label> : null}
      <InputContainer type={props.type}>
        <Input
          id={props.id}
          type={props.type}
          name={props.name}
          value={props.value}
          onChange={props.onChange}
          placeholder={props.placeholder}
        />
        {props.icon && <img src={props.icon} alt='search' />}
      </InputContainer>
    </>
  );
};

export default InputFieldChat;
