import React from 'react';
import { useSelector } from 'react-redux';
import {
  Sidebar,
  ProfilePic,
  ProfilePicContainer,
  ProfilePicture,
  FullName,
  TabContainer,
  Tab,
  SidebarLogo,
  SidebarLogoContainer,
} from './SidebarStyled';
import logo1 from '../../assets/images/Logo1.svg';
import brush from '../../assets/icons/brush.svg';
import coronavirus from '../../assets/icons/coronavirus.svg';
import calendar from '../../assets/icons/calendar.svg';
import account from '../../assets/icons/account.svg';
import settings from '../../assets/icons/settings.svg';

import { NavLink } from 'react-router-dom';

const SideNav = () => {
  const { user } = useSelector((state) => state.userInfo);

  return (
    <Sidebar>
      <ProfilePicContainer>
        <ProfilePic>
          <ProfilePicture src={user.imageURL ? user.imageURL : logo1} alt='' />
        </ProfilePic>
      </ProfilePicContainer>
      <FullName>{`${user.firstName || ''} ${user.lastName || ''}`}</FullName>
      <TabContainer>
        <NavLink to='/list-of-all-jobs' activeClassName='navActiveClass' exact>
          <Tab>
            <img src={brush} alt='icon' />
            <p>List of all jobs</p>
          </Tab>
        </NavLink>
        <NavLink to='/all-maids' activeClassName='navActiveClass'>
          <Tab>
            <img src={account} alt='icon' />
            <p>All maids</p>
          </Tab>
        </NavLink>
        <NavLink to='/get-calendar-for-maid' activeClassName='navActiveClass'>
          <Tab>
            <img src={calendar} alt='icon' />
            <p>Get calendar for maid</p>
          </Tab>
        </NavLink>

        <NavLink to='/all-customers' activeClassName='navActiveClass'>
          <Tab>
            <img src={account} alt='icon' />
            <p>All customers</p>
          </Tab>
        </NavLink>

        <NavLink to='/chat-support' activeClassName='navActiveClass'>
          <Tab>
            <img src={settings} alt='icon' />
            <p>Chat support</p>
          </Tab>
        </NavLink>
      </TabContainer>
      <SidebarLogoContainer>
        <SidebarLogo>
          <img src={logo1} alt='logo' />
        </SidebarLogo>
      </SidebarLogoContainer>
    </Sidebar>
  );
};

export default SideNav;
