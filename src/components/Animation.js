import React from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

const Animation = ({ children }) => {
  return (
    <CSSTransition in={true} appear={true} timeout={2000} classNames={'fade'}>
      {children}
    </CSSTransition>
  );
};

export const AnimationGroup = ({ children }) => {
  return <TransitionGroup>{children}</TransitionGroup>;
};

export const AnimationMulti = ({ children }, id) => {
  return (
    <CSSTransition
      in={true}
      // appear={true}
      timeout={2000}
      classNames={'fade'}
      id={id}
    >
      {children}
    </CSSTransition>
  );
};

export default Animation;
