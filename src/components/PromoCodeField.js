import React from 'react';
import styled from 'styled-components';

const Input = styled.input`
  width: 100%;
  border: none;
  background: none;
  outline: none;
  font-size: 14px;
  box-shadow: inset -5px -4px 10px #fafbfc, inset 3px 3px 8px #bdc1d1;
`;

const InputContainer = styled.div`
  min-width: 120px;
  width: 100%;
  margin: 0 20px 0 0;
`;

const Label = styled.p`
  font-weight: bold;
  font-size: 13px;
  text-align: center;
  margin-bottom: 20px;
  font-weight: bold;
`;

const PromoCodeField = (props) => {
  return (
    <InputContainer props={props}>
      <Label>{props.label}</Label>
      <Input
        id={props.id}
        type={props.type}
        name={props.name}
        value={props.value}
        onChange={(e) => props.onChange(e.target.value)}
        placeholder={props.placeholder}
        className={props.discount > 0 ? 'discount-success' : 'discount-fail'}
        style={{ textAlign: 'center' }}
      />
    </InputContainer>
  );
};

export default PromoCodeField;
