import React from 'react';
import { Error } from '../views/Login/loginStyled';

const ErrorMessage = ({ error }) => {
  return (
    <>
      {error && (
        <div style={{ textAlign: 'center', marginTop: '20px' }}>
          <Error>
            <p>{error}</p>
          </Error>
        </div>
      )}
    </>
  );
};

export default ErrorMessage;
