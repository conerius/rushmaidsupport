import React from 'react';
import styled from 'styled-components';

const Input = styled.input`
  width: 100%;
  border: none;
  background: none;
  outline: none;
  font-size: 15px;
  /* box-shadow: inset -5px -4px 10px #fafbfc, inset 3px 3px 8px #bdc1d1; */
  font-weight: bold;
`;

const InputContainer = styled.div`
  min-width: 120px;
  width: 100%;
  margin: 0 40px;
  @media (max-width: 1100px) {
    margin: 0;
  }
`;

const Label = styled.p`
  font-weight: bold;
  font-size: 13px;
  text-align: center;
  margin-bottom: 20px;
  font-weight: bold;
  @media (max-width: 1100px) {
    /* margin-top: 30px; */
    text-align: left;
  }
`;

const ZipCodeFieldMobile = (props) => {
  return (
    <InputContainer props={props}>
      <Label>{props.label}</Label>
      <Input
        id={props.id}
        type='text'
        name={props.name}
        value={props.value}
        onChange={(e) => props.onChange(e.target.value)}
        placeholder={props.placeholder}
      />
    </InputContainer>
  );
};

export default ZipCodeFieldMobile;
