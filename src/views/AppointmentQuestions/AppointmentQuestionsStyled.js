import styled from 'styled-components';

const BookNow = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  padding: 100px 0;
`;

const BookContent = styled.div`
  max-width: 800px;
  width: 100%;
`;

const Questions = styled.div`
  // display: grid;
  // grid-template-columns: repeat(2, 1fr);
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  @media (max-width: 1100px) {
    flex-direction: column;
  }
`;

const Question = styled.div`
  border-bottom: 1px solid rgb(222, 222, 222);
  width: calc(50% - 25px);
  @media (max-width: 1100px) {
    width: 100%;
  }
  margin-bottom: 30px !important;

  h5 {
    font-weight: bold;
    font-size: 16px;
    line-height: 22px;
    color: #000000;
    margin-bottom: 20px !important;
  }
  p {
    font-weight: 300;
    font-size: 16px;
    line-height: 22px;
    margin-bottom: 30px !important;
  }
`;

export { BookNow, BookContent, Questions, Question };
