/* eslint-disable react/no-unescaped-entities */
import React, { useState, useEffect } from 'react';
import { BookContent, Question, Questions } from './AppointmentQuestionsStyled';

const QuestionComp = (props) => {
  return (
    <Question>
      <h5>{props.q}</h5>
      <p>{props.a}</p>
    </Question>
  );
};

const AppointmentQuestions = ({ location }) => {
  const { questions } = location.state;
  const [getQuestions, setGetQuestions] = useState([]);

  useEffect(() => {
    const data = [];

    for (const [key, value] of Object.entries(questions)) {
      data.push(value);
    }

    const sortByIndex = data.slice(0);
    sortByIndex.sort(function (a, b) {
      return a.index - b.index;
    });

    setGetQuestions(sortByIndex);
  }, []);

  return (
    <BookContent>
      <Questions>
        <QuestionComp
          q={'Do you have any pets?'}
          a={getQuestions.length > 0 && getQuestions[0].answer}
        />
        <QuestionComp
          q={'Are you Airbnb host?'}
          a={getQuestions.length > 0 && getQuestions[6].answer}
        />
        <QuestionComp
          q={'Do you have proper cleaning supplies necessary for a clean home?'}
          a={getQuestions.length > 0 && getQuestions[4].answer}
        />
        <QuestionComp
          q={'Do you have urgent matters needed to be attended to?'}
          a={getQuestions.length > 0 && getQuestions[2].answer}
        />
        <QuestionComp
          q={
            'Will you be home or away during the cleaning? How will made receive the key?'
          }
          a={getQuestions.length > 0 && getQuestions[1].answer}
        />
        <QuestionComp
          q={'Additional notes?'}
          a={getQuestions.length > 0 && getQuestions[5].answer}
        />
        <QuestionComp
          q={'What do we need to ask your front desk/concierge to gain entry?'}
          a={getQuestions.length > 0 && getQuestions[3].answer}
        />
      </Questions>
    </BookContent>
  );
};

export default AppointmentQuestions;
