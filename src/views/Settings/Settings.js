/* eslint-disable react/no-unescaped-entities */
import React from 'react';
import { logout } from '../../actions/authActions';

import { BookContent, BookTitle, ProfileTab, Logout } from './SettingsStyled';
import { Link } from 'react-router-dom';

import { useDispatch } from 'react-redux';

import 'react-toggle/style.css'; // for ES6 modules

const Settings = () => {
  const dispatch = useDispatch();

  return (
    <>
      <BookContent>
        <BookTitle>Settings</BookTitle>
        <ProfileTab>
          <Link to='/RedeemGift'>
            <h3>Redeem Gift</h3>
          </Link>
        </ProfileTab>
        <ProfileTab>
          <Link to='/TermsAndCondition'>
            <h3>Terms and Condition</h3>
          </Link>
        </ProfileTab>
        <ProfileTab>
          <Link to='/PrivacyPolicy'>
            <h3>Privacy Policy</h3>
          </Link>
        </ProfileTab>
        <ProfileTab>
          <Link to='/BillingInfo'>
            <h3>Billing Info</h3>
          </Link>
        </ProfileTab>
        <Logout style={{ marginTop: 100 }} onClick={() => dispatch(logout())}>
          Logout
        </Logout>
      </BookContent>
    </>
  );
};

export default Settings;
