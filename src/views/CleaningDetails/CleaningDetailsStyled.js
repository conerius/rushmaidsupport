import styled from 'styled-components';

const AccIcon = styled.div`
  background: linear-gradient(
      147.42deg,
      rgba(64, 72, 93, 0.4) 6.32%,
      rgba(96, 106, 130, 0.4) 92.25%
    ),
    linear-gradient(128deg, #e6e7ed -79.65%, #f7f8fa 151.25%);
  background-blend-mode: soft-light, normal;
  box-shadow: -7px -7px 16px #fafbfc, 4px 3px 19px #bdc1d1,
    inset -1px -1px 16px #f5f6fa, inset 1px 1px 16px #e9eaf2;
  border-radius: 50%;
  height: 48px;
  width: 48px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 30px;
`;

const BookNow = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  padding: 100px 0;
`;
const BookContent = styled.div`
  max-width: 800px;
  width: 100%;
`;
const BookTitle = styled.div`
  font-size: 20px;
  margin-bottom: 30px;
`;

const Tabs = styled.div`
  background: linear-gradient(
      147.42deg,
      rgba(64, 72, 93, 0.4) 6.32%,
      rgba(96, 106, 130, 0.4) 92.25%
    ),
    linear-gradient(128deg, #e6e7ed -79.65%, #f7f8fa 151.25%);
  background-blend-mode: soft-light, normal;
  box-shadow: -7px -7px 16px #fafbfc, 4px 3px 19px #bdc1d1,
    inset -1px -1px 16px #f5f6fa, inset 1px 1px 16px #e9eaf2;
  border-radius: 20px;
  height: 80px;
  width: 100%;
  padding: 15px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 30px;
`;

const Tab = styled.div`
  transition: all 0.2s;

  width: calc(50% - 10px);
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
  ${(props) =>
    props.selected
      ? `background: #EBECF0;
box-shadow: 1px 1px 7px #EBECF0, inset -5px -4px 10px #FAFBFC, inset 3px 3px 8px #BDC1D1;
border-radius: 16px;
color: #7838FF;
`
      : ''}
  background-blend-mode: soft-light, normal;
  color: #000;
  cursor: pointer;
  p {
    font-weight: 300;
    font-size: 16px;
    ${(props) =>
      props.selected
        ? `
color: #7838FF;
`
        : ''}
  }
`;

const Appointment = styled.div`
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    linear-gradient(133.76deg, #e6e7ed -18.17%, #f7f8fa 140.27%);
  background-blend-mode: soft-light, normal;
  box-shadow: -2px -3px 8px #fafafc, 1px 1px 5px #b1b5c6,
    inset 1px 1px 3px #ebecf0;
  border-radius: 8px;
  margin-bottom: 15px;
  position: relative;
  p {
    font-size: 14px;
    text-transform: uppercase;
    color: #9a7df8;
  }
  h3 {
    font-weight: 600;
    font-size: 16px;
    color: #000;
  }
`;

const FlexBetween = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`;

const FlexBetweenResponsive = styled.div`
  display: flex;
  justify-content: space-between;
  @media (max-width: 1100px) {
    flex-direction: column;
  }
`;

const BetweenIn = styled.div`
  width: calc(50% - 25px);
  @media (max-width: 1100px) {
    width: 100%;
  }
`;

const TimeDateTab = styled.div`
  display: flex;
  align-items: center;
  // justify-content: space-between;
  min-height: 66px;
  padding: 15px;
  p {
    font-size: 14px;
    text-transform: uppercase;
    color: #9a7df8;
  }
  h3 {
    font-weight: 600;
    font-size: 16px;
    color: #000;
  }
  img {
    margin-right: 20px;
  }
`;
const DateTab = styled(TimeDateTab)`
  width: 60%;
`;
const TimeTab = styled(TimeDateTab)`
  width: 40%;
`;
const LocationTab = styled(TimeDateTab)`
  width: 100%;
`;

const DisinfectionAppointment = styled(Appointment)`
  background: linear-gradient(
      147.42deg,
      rgba(64, 72, 93, 0.4) 6.32%,
      rgba(96, 106, 130, 0.4) 92.25%
    ),
    linear-gradient(128deg, #f7f8fa -26.74%, #e8ddff 151.25%);
`;

const Disinfection = styled(TimeDateTab)`
  width: 100%;
  img {
    margin-right: 15px;
    height: 30px;
    width: 30px;
  }
  h4 {
    font-weight: bold;
    font-size: 18px;
  }
`;

const AppointmentRestart = styled.div`
  position: absolute;
  right: 30px;
  top: calc(50% - 46px);
  background: linear-gradient(
      147.42deg,
      rgba(64, 72, 93, 0.4) 6.32%,
      rgba(96, 106, 130, 0.4) 92.25%
    ),
    linear-gradient(128deg, #e6e7ed -79.65%, #f7f8fa 151.25%);
  background-blend-mode: soft-light, normal;
  box-shadow: -7px -7px 16px #fafbfc, 4px 3px 19px #bdc1d1,
    inset -1px -1px 16px #f5f6fa, inset 1px 1px 16px #e9eaf2;
  width: 92px;
  height: 92px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
  cursor: pointer;
  transition: all 0.2s;
  :hover {
    opacity: 0.8;
  }
  @media (max-width: 900px) {
    top: calc(50% - 29px);
    right: 20px;
    width: 58px;
    height: 58px;

    img {
      width: 40px;
    }
  }
`;

const NextButton = styled.button`
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    linear-gradient(135deg, #b4b2f1 0%, #7838ff 100%);
  background-blend-mode: soft-light, normal;
  box-shadow: -2px -3px 8px #fafafc, 1px 1px 5px #b1b5c6,
    inset 1px 1px 3px #ebecf0;
  text-transform: uppercase;
  border-radius: 8px;
  width: 100%;
  height: 50px;
  padding: 0 19px;
  margin: 0 0 20px 0;
  outline: none;
  font-size: 16px;
  border: none;
  color: #ffffff;
  font-weight: bold;
  transition: all 0.2s;
  :hover {
    opacity: 0.8;
  }
`;

const NegativeButton = styled(NextButton)`
  background: linear-gradient(
      147.42deg,
      rgba(64, 72, 93, 0.4) 6.32%,
      rgba(96, 106, 130, 0.4) 92.25%
    ),
    linear-gradient(128deg, #e6e7ed -79.65%, #f7f8fa 151.25%);
  background-blend-mode: soft-light, normal;
  box-shadow: -7px -7px 16px #fafbfc, 4px 3px 19px #bdc1d1,
    inset -1px -1px 16px #f5f6fa, inset 1px 1px 16px #e9eaf2;
  color: #9a7df8;
  text-transform: capitalize;
`;

const Flex = styled.div`
  display: flex;
  align-items: center;
`;

export {
  BookNow,
  BookContent,
  BookTitle,
  Tab,
  Tabs,
  Appointment,
  DateTab,
  TimeTab,
  LocationTab,
  FlexBetween,
  Flex,
  AppointmentRestart,
  Disinfection,
  DisinfectionAppointment,
  AccIcon,
  NextButton,
  NegativeButton,
  FlexBetweenResponsive,
  BetweenIn,
};
