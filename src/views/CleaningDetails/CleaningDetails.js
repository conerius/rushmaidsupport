/* eslint-disable react/no-unescaped-entities */
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import NProgress from 'nprogress';

import {
  BookContent,
  BookTitle,
  Appointment,
  DateTab,
  TimeTab,
  LocationTab,
  FlexBetween,
  Flex,
  AccIcon,
  NextButton,
  NegativeButton,
  BetweenIn,
  FlexBetweenResponsive,
} from './CleaningDetailsStyled';

import acc from '../../assets/icons/accountC.svg';
import TextArea from '../../components/AdditionalTextarea';
import { NavLink } from 'react-router-dom';

import { getAppointments } from '../../actions/appointmentsActions';
import { createBooking } from '../../actions/bookingActions';
import { getCustomer } from '../../actions/stripeActions';

import moment from 'moment';
import { momentDate, momentTime } from '../../helperFunction';
import { getMaids } from '../../actions/maidsActions';

const Appointments = ({ appointment, maids }) => {
  const [additionalServices, setAditionalServices] = useState([]);
  const {
    date,
    address,
    numberOfBalconies,
    numberOfBathrooms,
    numberOfKitchens,
    numberOfLivingAreas,
    numberOfBedrooms,
    status,
    mainMaidUID,
    additionalServiceIDs,
    type,
  } = appointment;

  const Status = (status) => {
    if (status === 'cleaning') {
      return 'Cleaning';
    } else if (status === 'notCleaning') {
      return 'Not Cleaning';
    } else {
      return 'Completed';
    }
  };

  const services = () => {
    const data = additionalServiceIDs?.split(', ');

    const categories = [];

    data?.forEach((item) => {
      if (item === 'autoID1') {
        categories.push('Move in-move out cleaning');
      } else if (item === 'autoID2') {
        categories.push('Deep Cleaning');
      } else if (item === 'autoID3') {
        categories.push('Inside Refrigerator');
      } else if (item === 'autoID4') {
        categories.push('Inside Oven');
      }
    });

    setAditionalServices(categories);
  };

  useEffect(() => {
    services();
  }, []);

  const maid = maids?.filter((id) => {
    return id?.uid === appointment?.mainMaidUID;
  });

  console.log(maid);

  return (
    <Appointment>
      <FlexBetween style={{ borderBottom: '1px solid #DEDEDE' }}>
        <DateTab>
          <div>
            <p>date</p>
            <h3>{momentDate(date)}</h3>
          </div>
        </DateTab>
        <TimeTab>
          <div>
            <p>time</p>
            <h3>{momentTime(date)}</h3>
          </div>
        </TimeTab>
      </FlexBetween>
      <Flex style={{ borderBottom: '1px solid #DEDEDE' }}>
        <LocationTab>
          <div>
            <p>CLEANING ADDRESS</p>
            <h3>{address}</h3>
          </div>
        </LocationTab>
      </Flex>
      <Flex style={{ borderBottom: '1px solid #DEDEDE' }}>
        <LocationTab>
          <div>
            <p>CLEANING ITEMS</p>
            <h3>{numberOfBedrooms} Bedroom</h3>
            <h3>{numberOfBathrooms} Bathroom</h3>
            <h3>{numberOfKitchens} Kitchen</h3>
            <h3>{numberOfBalconies} Balcony</h3>
            <h3>{numberOfLivingAreas} Living Room</h3>
          </div>
        </LocationTab>
      </Flex>
      <Flex style={{ borderBottom: '1px solid #DEDEDE' }}>
        <LocationTab>
          <div>
            <p>EXTRA SERVICES</p>
            {additionalServices &&
              additionalServices.map((service, i) => (
                <h3 key={i}>{service}</h3>
              ))}
          </div>
        </LocationTab>
      </Flex>
      <FlexBetween
        style={{ borderBottom: '1px solid #DEDEDE', alignItems: 'center' }}
      >
        <DateTab>
          <div>
            <p>MAID NAME</p>
            <h3>
              {mainMaidUID ? (
                <>
                  {maids && maid[0]?.firstName} {maids && maid[0]?.lastName}
                </>
              ) : (
                'Maid not assigned yet'
              )}
            </h3>
          </div>
        </DateTab>

        <AccIcon>
          <img src={acc} alt='Account' />
        </AccIcon>
      </FlexBetween>
      <Flex>
        <LocationTab>
          <div>
            <p>Type</p>
            <h3>{type}</h3>
          </div>
        </LocationTab>
      </Flex>
      <Flex>
        <LocationTab>
          <div>
            <p>STATUS</p>
            <h3>{Status(status)}</h3>
          </div>
        </LocationTab>
      </Flex>
    </Appointment>
  );
};

const Appointments2 = ({ appointment, history }) => {
  const [notes, setNotes] = useState('');

  // const { details } = appointment && appointment;

  const dispatch = useDispatch();

  const { user } = useSelector((state) => state.userInfo);
  // const { customer, customerId } = useSelector((state) => state.stripeInfo);
  // const { loading } = useSelector((state) => state.bookings);

  // const onHandleClick = () => {
  //   dispatch(
  //     createBooking(
  //       customer,
  //       user,
  //       appointment.date,
  //       appointment.address,
  //       appointment.longitude,
  //       appointment.latitude,
  //       appointment.numberOfBedrooms,
  //       appointment.numberOfBathrooms,
  //       appointment.numOfKitchens ? appointment.numOfKitchens : 0,
  //       appointment.numOfLivingAreas ? appointment.numOfLivingAreas : 0,
  //       appointment.numOfBalconies ? appointment.numOfBalconies : 0,
  //       appointment.additionalServiceIDs,
  //       appointment.price,
  //       appointment.promo_code_id,
  //       appointment.questions,
  //       appointment.zipCode,
  //       history
  //     )
  //   );
  // };

  // useEffect(() => {
  //   loading ? NProgress.start() : NProgress.done();
  // }, [loading]);

  return (
    <div>
      <p>Maid notes</p>
      <TextArea
        type={'textarea'}
        placeholder={'Notes...'}
        value={appointment?.details}
        // onChange={(e) => setNotes(e.target.value)}
        height={'296px'}
        // disabled={user.role === 'user'}
        disabled
      />
      {appointment?.type === 'cleaning' && (
        <NavLink
          to={{
            pathname: '/AppointmentQuestions',
            state: {
              questions: appointment?.questions,
            },
          }}
        >
          <NegativeButton>Additional Questions</NegativeButton>
        </NavLink>
      )}

      {/* <NextButton onClick={onHandleClick}>REPEAT CLEANING</NextButton> */}
    </div>
  );
};

const CleaningDetails = ({ match, history }) => {
  const dispatch = useDispatch();
  const { appointments, loading } = useSelector((state) => state.appointments);
  const { maids, loadingMaids } = useSelector((state) => state.maids);

  const appointment = appointments?.find(
    (app) => app.selfUID === match.params.id
  );

  useEffect(() => {
    dispatch(getAppointments());
    dispatch(getMaids());
  }, [dispatch]);

  useEffect(() => {
    loading && loadingMaids ? NProgress.start() : NProgress.done();
  });

  return (
    <BookContent>
      <BookTitle>Cleaning Details</BookTitle>
      {/* {loading && loadingMaids && ( */}
      <FlexBetweenResponsive>
        <BetweenIn>
          <Appointments appointment={appointment} maids={maids} />
        </BetweenIn>
        <BetweenIn>
          <Appointments2 appointment={appointment} history={history} />
        </BetweenIn>
      </FlexBetweenResponsive>
      {/* )} */}
    </BookContent>
  );
};

export default CleaningDetails;
