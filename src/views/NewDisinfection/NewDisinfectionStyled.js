import styled from 'styled-components';

const BookNow = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  padding: 100px 0;
`;
const BookContent = styled.div`
  max-width: 800px;
  width: 100%;
`;
const BookTitle = styled.div`
  font-size: 20px;
  margin-bottom: 30px;
`;
const BookTab = styled.div`
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    linear-gradient(133.76deg, #e6e7ed -18.17%, #f7f8fa 140.27%);
  background-blend-mode: soft-light, normal;
  box-shadow: -2px -3px 8px #fafafc, 1px 1px 5px #b1b5c6,
    inset 1px 1px 3px #ebecf0;
  border-radius: 8px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 66px;
  padding: 20px;
  margin-bottom: 15px;

  width: calc(60% - 15px);
  @media (max-width: 1100px) {
    width: 100%;
  }

  p {
    font-size: 14px;
    text-transform: uppercase;
    color: #9a7df8;
  }
  h3 {
    font-weight: 600;
    font-size: 16px;
    color: #000;
  }
  img {
    margin-right: 20px;
  }
`;
const BookTabWrap = styled.div`
  width: calc(50% - 5px);
`;
const LocationArrow = styled.button`
  background: none;
  box-shadow: -7px -7px 16px #fafbfc, 4px 3px 19px #bdc1d1,
    inset -1px -1px 16px #f5f6fa, inset 1px 1px 16px #e9eaf2;
  border-radius: 50%;
  width: 48px;
  height: 48px;
  display: flex;
  align-items: center;
  justify-content: center;
  outline: none;
  border: none;
  img {
    margin: 0;
  }
`;
const BookTabNum = styled.h3`
  color: #b09aff;
  font-size: 24px !important;
  font-weight: normal;
  margin: 0 20px;
`;

const Flex = styled.div`
  display: flex;
  align-items: center;
`;

const FlexBetween = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  @media (max-width: 1100px) {
    flex-direction: column;
  }
`;
const FlexBetweenStatic = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`;

const TimeDateTab = styled.div`
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    linear-gradient(133.76deg, #e6e7ed -18.17%, #f7f8fa 140.27%);
  background-blend-mode: soft-light, normal;
  box-shadow: -2px -3px 8px #fafafc, 1px 1px 5px #b1b5c6,
    inset 1px 1px 3px #ebecf0;
  border-radius: 8px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 66px;
  padding: 15px;
  margin-bottom: 15px;
  p {
    font-size: 14px;
    text-transform: uppercase;
    color: #9a7df8;
  }
  h3 {
    font-weight: 600;
    font-size: 16px;
    color: #000;
  }
`;
const DateTab = styled(TimeDateTab)`
  width: calc(60% - 15px);
  @media (max-width: 1100px) {
    width: 100%;
  }
`;
const TimeTab = styled(TimeDateTab)`
  width: calc(40% - 15px);
  @media (max-width: 1100px) {
    width: 100%;
  }
`;
const NextButton = styled.button`
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    linear-gradient(135deg, #b4b2f1 0%, #7838ff 100%);
  background-blend-mode: soft-light, normal;
  box-shadow: -2px -3px 8px #fafafc, 1px 1px 5px #b1b5c6,
    inset 1px 1px 3px #ebecf0;
  text-transform: uppercase;
  border-radius: 8px;
  width: 100%;
  height: 50px;
  padding: 0 19px;

  margin: 0 0 20px 0;
  outline: none;
  font-size: 16px;
  border: none;
  color: #ffffff;
  font-weight: bold;
  transition: all 0.2s;
  :hover {
    opacity: 0.8;
  }
`;
const TotalPrice = styled.div`
  text-align: right;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding: 15px;
  margin-bottom: 30px;
  p {
    font-size: 14px;
    text-transform: uppercase;
    color: #9a7df8;
  }
  h3 {
    font-weight: 600;
    font-size: 30px;
    color: #000;
  }
  img {
    margin-right: 20px;
  }

  @media (max-width: 1100px) {
    text-align: center;

    justify-content: center;
  }
`;

const PromoCode = styled.div`
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    linear-gradient(133.76deg, #e6e7ed -18.17%, #f7f8fa 140.27%);
  background-blend-mode: soft-light, normal;
  box-shadow: -2px -3px 8px #fafafc, 1px 1px 5px #b1b5c6,
    inset 1px 1px 3px #ebecf0;
  border-radius: 8px;
  width: 100%;
  // height: 50px;
  display: flex;
  // align-items: center;
  padding: 20px 20px;
  // margin: 0 0 20px 0;
  width: 50%;
  button {
    margin: 0;
    font-size: 11px;
  }
  margin-bottom: 30px;

  @media (max-width: 1100px) {
    width: 100%;
  }
`;

const SliderFlex = styled.div`
  display: flex;
  align-items: center;
  img {
    margin-right: 20px;
  }
  @media (max-width: 1100px) {
    flex-direction: column;
    img {
      margin: 0;
    }
  }
`;

const BookTabMobile = styled.div`
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    linear-gradient(133.76deg, #e6e7ed -18.17%, #f7f8fa 140.27%);
  background-blend-mode: soft-light, normal;
  box-shadow: -2px -3px 8px #fafafc, 1px 1px 5px #b1b5c6,
    inset 1px 1px 3px #ebecf0;
  border-radius: 8px;
  /* display: flex;
  align-items: center;
  justify-content: space-between; */
  /* height: 66px; */
  padding: 20px;
  margin-bottom: 15px;

  width: 100%;
  @media (max-width: 1100px) {
    width: 100%;
  }

  p {
    font-size: 14px;
    text-transform: uppercase;
    color: #9a7df8;
  }
  h3 {
    font-weight: 600;
    font-size: 16px;
    color: #000;
  }
  img {
    margin-right: 20px;
  }
`;

export {
  BookNow,
  BookContent,
  BookTitle,
  BookTab,
  LocationArrow,
  BookTabNum,
  Flex,
  TimeTab,
  DateTab,
  FlexBetween,
  FlexBetweenStatic,
  NextButton,
  BookTabWrap,
  TotalPrice,
  PromoCode,
  SliderFlex,
  BookTabMobile,
};
