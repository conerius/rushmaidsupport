/* eslint-disable react/no-unescaped-entities */
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';

import ErrorMessage from '../../components/ErrorMessage';
import ZipCodeFieldMobile from '../../components/ZipCodeFieldMobile';

import {
  createDisinfection,
  setDisinfectionDate,
  setDisinfectionTime,
  handleSelectComplete,
  setDisinfectionZipCode,
  setDisinfectionTotalPrice,
  setDisinfectionFeets,
  setDisinfectionPromoCode,
  setDisinfectionDiscount,
  createDisinfectionPayless,
  setDisinfectionPromoCodeId,
} from '../../actions/disinfectionActions';

import { getCustomer } from '../../actions/stripeActions';
import { getPromoCode } from '../../actions/promoCodesActions';

import { SingleDatePicker } from 'react-dates';
import { TimePicker } from 'antd';
import NProgress from 'nprogress';

import locationIcon from '../../assets/icons/location.svg';

import PlacesAutocomplete from 'react-places-autocomplete';

import {
  BookContent,
  BookTab,
  BookTitle,
  Flex,
  FlexBetween,
  TimeTab,
  DateTab,
  NextButton,
  TotalPrice,
  PromoCode,
  SliderFlex,
  BookTabMobile,
} from './NewDisinfectionStyled';

import '@shwilliam/react-rubber-slider/dist/styles.css';

import arrow from '../../assets/icons/arrow.svg';
import corona from '../../assets/icons/coronavirus24px.svg';

import PromoCodeField from '../../components/PromoCodeField';
import ZipCodeField from '../../components/ZipCodeField';

import Slider from 'react-rangeslider';

import 'react-rangeslider/lib/index.css';
import moment from 'moment';

const API_URL = process.env.REACT_APP_API_URL;

const NewDisinfection = ({ history, location }) => {
  const [focused, setFocused] = useState(false);
  const [address, setAddress] = useState('');
  const [timePickerOpen, setTimePickerOpen] = useState(false);
  const [errorMsg, setErrorMsg] = useState('');
  const format = 'HH:mm A';

  const dispatch = useDispatch();

  const { user } = useSelector((state) => state.userInfo);
  const { customer, customerId } = useSelector((state) => state.stripeInfo);
  const { promoCodes } = useSelector((state) => state.promoCodes);
  const {
    disinfectionTime,
    disinfectionDate,
    value,
    disinfectionTotalPrice,
    disinfectionAddress,
    disinfectionCoordinates,
    disinfectionZipCode,
    disinfectionPromoCode,
    disinfectionDiscount,
    disinfectionPromoCodeId,
    loading,
    error,
  } = useSelector((state) => state.disinfections);

  const getZipCodeStatus = async (zipCode) => {
    try {
      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.token}`,
        },
      };

      const { data } = await axios.get(
        `${API_URL}/booking/zip-code-availability/${zipCode}`,
        config
      );

      return data;
    } catch (error) {
      return error.response.data;
    }
  };

  const onHandleTimeChange = (newTime) => {
    dispatch(setDisinfectionTime(newTime));
  };

  const getDiscount = () => {
    if (disinfectionTotalPrice === (0).toFixed(2)) {
      setErrorMsg('Please choose disinfection area feets');
    } else {
      setErrorMsg('');
      promoCodes.forEach((code) => {
        if (code.code === disinfectionPromoCode) {
          dispatch(setDisinfectionDiscount(code.discount));
          dispatch(setDisinfectionPromoCode(`${code.discount} %`));
          dispatch(setDisinfectionPromoCodeId(code.selfID));

          let totalValue = (code.discount / 100) * disinfectionTotalPrice;

          dispatch(
            setDisinfectionTotalPrice(disinfectionTotalPrice - totalValue)
          );
        }
      });
    }
  };

  const setFeets = (feets) => {
    let price;
    if (feets === 0) {
      price = 0;
    } else if (feets < 800) {
      price = 140;
    } else if (feets > 800 && feets < 1700) {
      price = 210;
    } else if (feets > 1700 && feets < 2700) {
      price = 280;
    } else if (feets > 2700 && feets <= 4000) {
      price = 330;
    } else {
      price = 0;
    }
    dispatch(setDisinfectionTotalPrice(price.toFixed(2)));
  };

  const checkout = async () => {
    if (!disinfectionAddress) {
      setErrorMsg('Please enter disinfection address');
    } else {
      if (!disinfectionZipCode) {
        setErrorMsg('Please enter zip code');
      } else if (disinfectionTotalPrice === (0).toFixed(2)) {
        setErrorMsg('Please choose disinfection area feets');
      } else {
        setErrorMsg('');

        const zipCodeResponse = await getZipCodeStatus(disinfectionZipCode);

        if (zipCodeResponse.error) {
          setErrorMsg(zipCodeResponse.message);
        } else {
          if (!customer) {
            history.push({
              pathname: `/BillingInfo/${disinfectionZipCode}`,
              state: {
                from: location.pathname,
              },
            });
          } else {
            setErrorMsg('');

            disinfectionDiscount === 100
              ? dispatch(
                  createDisinfectionPayless(
                    moment(disinfectionDate).unix(),
                    value,
                    disinfectionAddress,
                    disinfectionCoordinates.lng,
                    disinfectionCoordinates.lat,
                    user,
                    disinfectionPromoCodeId,
                    disinfectionZipCode,
                    history
                  )
                )
              : dispatch(
                  createDisinfection(
                    moment(disinfectionDate).unix(),
                    value,
                    disinfectionAddress,
                    disinfectionCoordinates.lng,
                    disinfectionCoordinates.lat,
                    user,
                    customer,
                    disinfectionPromoCode,
                    disinfectionTotalPrice,
                    disinfectionZipCode,
                    history
                  )
                );
          }
        }
      }
    }
  };

  const handleSelect = async (value) => {
    setAddress(value);
    dispatch(handleSelectComplete(value));
  };

  useEffect(() => {
    customerId && dispatch(getCustomer(customerId));
    loading ? NProgress.start() : NProgress.done();

    dispatch(getPromoCode());
  }, [dispatch, loading]);

  return (
    <BookContent>
      <BookTitle>How big is the area that needs disinfection?</BookTitle>

      <SliderFlex>
        <img src={corona} alt='corona' />

        <div className='slider'>
          <Slider
            min={0}
            max={4000}
            value={value}
            //  labels={horizontalLabels}
            //  format={formatkg}
            handleLabel={value + 'ft²'}
            onChange={(e) => {
              dispatch(setDisinfectionFeets(e));
              setFeets(value);
            }}
            tooltip={false}
          />
        </div>
      </SliderFlex>
      <FlexBetween>
        <DateTab>
          <div className='dateInp'>
            <p>date</p>
            <SingleDatePicker
              date={disinfectionDate}
              onDateChange={(date) => dispatch(setDisinfectionDate(date))}
              focused={focused}
              onFocusChange={({ focused }) => setFocused(focused)}
              numberOfMonths={1}
              displayFormat={() => 'MMM D, YYYY'}
            />
          </div>
          <img
            src={arrow}
            alt='arrow'
            className='arrow-picker'
            onClick={({ focused }) => setFocused(!focused)}
          />
        </DateTab>
        <TimeTab>
          <div>
            <p>time</p>
            <TimePicker
              bordered={false}
              showNow={false}
              defaultValue={disinfectionTime}
              format={format}
              suffixIcon={false}
              preffixIcon={false}
              open={timePickerOpen}
              minuteStep={30}
              onSelect={(value) => onHandleTimeChange(value)}
              onOpenChange={(val) => setTimePickerOpen(val)}
            />
          </div>
          <img
            src={arrow}
            alt='arrow'
            className='arrow-picker'
            onClick={() => setTimePickerOpen(!timePickerOpen)}
          />
        </TimeTab>
      </FlexBetween>
      <div className='desktop'>
        <FlexBetween>
          <BookTab>
            <Flex style={{ width: '100%' }}>
              <img src={locationIcon} alt='' />
              <div className='disinfectionAddress'>
                <p>DISINFECTION ADDRESS</p>

                <PlacesAutocomplete
                  value={address}
                  onChange={setAddress}
                  onSelect={handleSelect}
                >
                  {({
                    getInputProps,
                    suggestions,
                    getSuggestionItemProps,
                    loading,
                  }) => (
                    <div style={{ position: 'relative' }}>
                      <input
                        type='text'
                        className='disinfectionAddress'
                        {...getInputProps({
                          type: 'text',
                          placeholder: 'Enter your address here',
                        })}
                      />
                      <div className='suggestions'>
                        {loading ? <div>...loading</div> : null}
                        {suggestions.map((suggestion) => (
                          <div
                            {...getSuggestionItemProps(suggestion)}
                            key={suggestion.placeId}
                            style={{ cursor: 'pointer' }}
                          >
                            {suggestion.description}
                          </div>
                        ))}
                      </div>
                    </div>
                  )}
                </PlacesAutocomplete>
              </div>
            </Flex>
          </BookTab>
          <TimeTab>
            <ZipCodeField
              label={'Enter Zip Code'}
              value={disinfectionZipCode}
              onChange={(e) => dispatch(setDisinfectionZipCode(e))}
            />
          </TimeTab>
        </FlexBetween>
      </div>

      <div className='mobile'>
        <BookTabMobile>
          <Flex>
            <img src={locationIcon} alt='' />
            <div className='disinfectionAddress'>
              <p>DISINFECTION ADDRESS</p>

              <PlacesAutocomplete
                value={address}
                onChange={setAddress}
                onSelect={handleSelectComplete}
              >
                {({
                  getInputProps,
                  suggestions,
                  getSuggestionItemProps,
                  loading,
                }) => (
                  <div style={{ position: 'relative' }}>
                    <input
                      type='text'
                      className='disinfectionAddress'
                      {...getInputProps({
                        type: 'text',
                        placeholder: 'Enter your address here',
                      })}
                      style={{ padding: '0 0 15px 0' }}
                    />
                    <div className='suggestions'>
                      {loading ? <div>...loading</div> : null}

                      {suggestions.map((suggestion) => (
                        <div
                          {...getSuggestionItemProps(suggestion)}
                          key={suggestion.placeId}
                        >
                          {suggestion.description}
                        </div>
                      ))}
                    </div>
                  </div>
                )}
              </PlacesAutocomplete>
              <ZipCodeFieldMobile
                className={'zip'}
                label={'Enter Zip Code'}
                value={disinfectionZipCode}
                onChange={(e) => dispatch(setDisinfectionZipCode(e))}
                placeholder={'Zip Code'}
              />
            </div>
          </Flex>
        </BookTabMobile>
      </div>

      <FlexBetween>
        <PromoCode>
          <PromoCodeField
            label={'Enter Promo Code'}
            value={disinfectionPromoCode}
            discount={disinfectionDiscount}
            onChange={(e) => {
              dispatch(setDisinfectionPromoCode(e));
              if (disinfectionPromoCode) {
                setFeets(value);
                dispatch(setDisinfectionDiscount(0));
              }
            }}
          />
          <NextButton onClick={getDiscount}>Submit</NextButton>
        </PromoCode>
        <TotalPrice>
          <div>
            <p>Total Price</p>
            <h3> ${disinfectionTotalPrice}</h3>
          </div>
        </TotalPrice>
      </FlexBetween>
      <NextButton onClick={checkout}>checkout</NextButton>
      {error && <ErrorMessage error={error} />}
      {errorMsg && <ErrorMessage error={errorMsg} />}
    </BookContent>
  );
};

export default NewDisinfection;
