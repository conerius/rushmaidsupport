/* eslint-disable react/no-unescaped-entities */
import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  BookContent,
  BookTitle,
  NextButton,
  ProfileTab,
  Flex,
  FlexBetween,
  ProfilePicContainer,
  ProfilePicture,
  ProfilePic,
  PPEdit,
  HalfBox,
  FlexBetweenSmall,
} from './ProfileStyled';
import edit from '../../assets/icons/edit.svg';
import editInCircle from '../../assets/icons/editInCircle.svg';
import wallet from '../../assets/icons/wallet.svg';
import ErrorMessage from '../../components/ErrorMessage';
import logo1 from '../../assets/images/Logo1.svg';

import { getAppointments } from '../../actions/appointmentsActions';
import {
  updateUserProfile,
  updateProfileImage,
} from '../../actions/authActions';

import NProgress from 'nprogress';

const Profile = () => {
  const [fullName, setFullName] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const [zip, setZipCode] = useState('');

  const imageRef = useRef(null);

  const dispatch = useDispatch();

  const { user, loading, error } = useSelector((state) => state.userInfo);
  const { appointments } = useSelector((state) => state.appointments);

  const imageSelect = () => {
    imageRef.current.click();
  };

  const handleChange = (event) => {
    const fileUploaded = event.target.files[0];
    const extension = fileUploaded.name.substr(fileUploaded.name.length - 3); // => "jpg"

    let reader = new FileReader();
    reader.onload = function (e) {
      const base64result = e.target.result.split(',')[1];

      dispatch(updateProfileImage(extension, base64result, user));
    };
    reader.readAsDataURL(event.target.files[0]);
  };

  const submitHandler = (e) => {
    e.preventDefault();

    dispatch(updateUserProfile(fullName, email, zip, user));
  };

  const nameInput = useRef(null);

  function focusNameInput() {
    nameInput.current.focus();
  }

  const phoneInput = useRef(null);

  function focusPhoneInput() {
    phoneInput.current.focus();
  }

  const zipInput = useRef(null);

  function focusZipInput() {
    zipInput.current.focus();
  }

  useEffect(() => {
    dispatch(getAppointments());
    setFullName(`${user.firstName || ''} ${user.lastName || ''}`);
    setEmail(user.email);
    setPhone(user.phoneNumber);
    setZipCode(user.zipCode);

    loading ? NProgress.start() : NProgress.done();
  }, [dispatch, loading]);

  return (
    <BookContent>
      <BookTitle>Profile</BookTitle>
      <ProfilePicContainer>
        <ProfilePic>
          <ProfilePicture src={user.imageURL ? user.imageURL : logo1} alt='' />
        </ProfilePic>
        <div>
          <input
            ref={imageRef}
            type='file'
            style={{ display: 'none' }}
            onChange={handleChange}
            // placeholder={'Enter here...'}
          />
          <PPEdit
            src={editInCircle}
            alt='Edit'
            className='editCircle'
            onClick={imageSelect}
          />
        </div>
      </ProfilePicContainer>
      <form onSubmit={submitHandler}>
        <FlexBetween>
          <HalfBox>
            <ProfileTab>
              <FlexBetweenSmall>
                <div style={{ width: 'calc(100% - 28px)' }}>
                  <p>Full Name</p>
                  <input
                    type='text'
                    value={fullName || ''}
                    className='editProfile'
                    onChange={(e) => setFullName(e.target.value)}
                    placeholder='Enter here...'
                    ref={nameInput}
                  />
                </div>
                <img
                  src={edit}
                  alt='Edit'
                  className='edit'
                  onClick={focusNameInput}
                />
              </FlexBetweenSmall>
            </ProfileTab>

            <ProfileTab>
              <FlexBetweenSmall>
                <div>
                  <p>Email</p>
                  <h3>{email}</h3>
                </div>
              </FlexBetweenSmall>
            </ProfileTab>

            <ProfileTab>
              <FlexBetweenSmall>
                <div>
                  <p>Number of appointments</p>
                  <h3>{appointments?.length}</h3>
                </div>
              </FlexBetweenSmall>
            </ProfileTab>
          </HalfBox>

          <HalfBox>
            <ProfileTab>
              <FlexBetweenSmall>
                <div style={{ width: 'calc(100% - 28px)' }}>
                  <p>Phone</p>
                  <input
                    type='text'
                    value={phone || ''}
                    className='editProfile'
                    onChange={(e) => setPhone(e.target.value)}
                    placeholder={'Enter here...'}
                    ref={phoneInput}
                  />
                </div>
                <img
                  src={edit}
                  alt='Edit'
                  className='edit'
                  onClick={focusPhoneInput}
                />
              </FlexBetweenSmall>
            </ProfileTab>

            <ProfileTab>
              <FlexBetweenSmall>
                <div style={{ width: 'calc(100% - 28px)' }}>
                  <p>Zip</p>
                  <input
                    type='text'
                    value={zip || ''}
                    className='editProfile'
                    onChange={(e) => setZipCode(e.target.value)}
                    ref={zipInput}
                  />
                </div>
                <img
                  src={edit}
                  alt='Edit'
                  className='edit'
                  onClick={focusZipInput}
                />
              </FlexBetweenSmall>
            </ProfileTab>

            <ProfileTab>
              <FlexBetweenSmall>
                <Flex>
                  <img src={wallet} alt='Edit' className='mr-2' />
                  <p>Credit</p>
                </Flex>
                <h2 style={{ fontWeight: 'bold', padding: '1px 0' }}>
                  ${user.credit ? user.credit.toFixed(2) : 0}
                </h2>
              </FlexBetweenSmall>
            </ProfileTab>
          </HalfBox>
        </FlexBetween>
        <NextButton type='submit'>Save</NextButton>
        <ErrorMessage error={error} />
      </form>
    </BookContent>
    // <BookContent>
    //   <BookTitle>Profile</BookTitle>
    //   <ProfilePicContainer>
    //     <ProfilePic>
    //       <ProfilePicture src={user.imageURL ? user.imageURL : logo1} alt='' />
    //     </ProfilePic>
    //     <div>
    //       <input
    //         ref={imageRef}
    //         type='file'
    //         style={{ display: 'none' }}
    //         onChange={handleChange}
    //       />
    //       <PPEdit
    //         src={editInCircle}
    //         alt='Edit'
    //         className='editCircle'
    //         onClick={imageSelect}
    //       />
    //     </div>
    //   </ProfilePicContainer>
    //   <form onSubmit={submitHandler}>
    //     <FlexBetween>
    //       <HalfBox>
    //         <ProfileTab>
    //           <FlexBetweenSmall>
    //             <div style={{ width: 'calc(100% - 27px)' }}>
    //               <p>Full Name</p>
    //               <input
    //                 type='text'
    //                 value={fullName || ''}
    //                 className='editProfile'
    //                 onChange={(e) => setFullName(e.target.value)}
    //               />
    //             </div>
    //             <img src={edit} alt='Edit' className='edit' />
    //           </FlexBetweenSmall>
    //         </ProfileTab>

    //         <ProfileTab>
    //           <FlexBetweenSmall>
    //             <div>
    //               <p>Email</p>
    //               <h3>{email}</h3>
    //             </div>
    //           </FlexBetweenSmall>
    //         </ProfileTab>

    //         <ProfileTab>
    //           <FlexBetweenSmall>
    //             <div>
    //               <p>Number of appointments</p>
    //               <h3>{appointments && appointments.length}</h3>
    //             </div>
    //           </FlexBetweenSmall>
    //         </ProfileTab>
    //       </HalfBox>

    //       <HalfBox>
    //         <ProfileTab>
    //           <FlexBetweenSmall>
    //             <div style={{ width: 'calc(100% - 27px)' }}>
    //               <p>Phone</p>
    //               <input
    //                 type='text'
    //                 value={phone || ''}
    //                 className='editProfile'
    //                 onChange={(e) => setPhone(e.target.value)}
    //               />
    //             </div>
    //             <img src={edit} alt='Edit' className='edit' />
    //           </FlexBetweenSmall>
    //         </ProfileTab>

    //         <ProfileTab>
    //           <FlexBetweenSmall>
    //             <div>
    //               <p>Address</p>
    //               <PlacesAutocomplete
    //                 value={address}
    //                 onChange={setAddress}
    //                 onSelect={handleSelectComplete}
    //               >
    //                 {({
    //                   getInputProps,
    //                   suggestions,
    //                   getSuggestionItemProps,
    //                   loading,
    //                 }) => (
    //                   <div className='suggestions'>
    //                     <input
    //                       type='text'
    //                       className='disinfectionAddress'
    //                       {...getInputProps({
    //                         type: 'text',
    //                         placeholder: 'Enter your address here',
    //                       })}
    //                     />
    //                     {loading ? <div>...loading</div> : null}
    //                     {suggestions.map((suggestion) => (
    //                       <div
    //                         {...getSuggestionItemProps(suggestion)}
    //                         key={suggestion.placeId}
    //                       >
    //                         {suggestion.description}
    //                       </div>
    //                     ))}
    //                   </div>
    //                 )}
    //               </PlacesAutocomplete>
    //             </div>
    //             <img src={edit} alt='Edit' className='edit' />
    //           </FlexBetweenSmall>
    //         </ProfileTab>

    //         <ProfileTab>
    //           <FlexBetweenSmall>
    //             <Flex>
    //               <img src={wallet} alt='Edit' className='mr-2' />
    //               <p>Credit</p>
    //             </Flex>
    //             <h2 style={{ fontWeight: 'bold', padding: '1px 0' }}>
    //               ${user.credit ? user.credit.toFixed(2) : 0}
    //             </h2>
    //           </FlexBetweenSmall>
    //         </ProfileTab>
    //       </HalfBox>
    //     </FlexBetween>
    //     <NextButton type='submit'>Save</NextButton>
    //     <ErrorMessage error={error} />
    //   </form>
    // </BookContent>
  );
};

export default Profile;
