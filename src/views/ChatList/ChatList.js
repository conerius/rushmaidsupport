/* eslint-disable react/no-unescaped-entities */
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { getMaids } from '../../actions/maidsActions';

import NProgress from 'nprogress';
import { Link } from 'react-router-dom';
import { BookContent, BookTitle, FlexBetween } from '../Maids/MaidsStyled';

import AdditionalInputField from '../../components/AdditionalInputField';

import searchIcon from '../../assets/icons/searchInput.svg';
import { Flex } from '../NewDisinfection/NewDisinfectionStyled';
import {
  ChatImage,
  ChatConversation,
  Date,
  ChatConversationMessage,
} from './ChatListStyled';
import { getConversations } from '../../actions/chatActions';
import { auth, database } from '../../config/firebase';
import Conversation from './Conversation';
import Search from 'antd/lib/transfer/search';

// const Maids = ({ maids }) => {
//   return (
//     <>
//       {maids &&
//         maids.map((maid) => (
//           <div key={maid?.uid}>
//             <Link
//               to={`/UserDetails/${maid?.role}/${maid?.uid}`}
//               key={maid?.uid}
//             >
//               <Maid>
//                 <div style={{ width: '15%' }}>{maid?.firstName}</div>
//                 <div style={{ width: '17%' }}>{maid?.lastName}</div>
//                 <div style={{ width: '30%' }}>{maid?.email}</div>
//                 <div style={{ width: '25%' }}>{maid?.phoneNumber}</div>
//                 <div style={{ width: '13%' }}>{maid?.status}</div>
//               </Maid>
//               <MaidMobile>
//                 <div>
//                   <p>First Name</p>
//                   {maid?.firstName}
//                 </div>
//                 <div>
//                   <p>Last Name</p>
//                   {maid?.lastName}
//                 </div>
//                 <div>
//                   <p>Email</p>
//                   {maid?.email}
//                 </div>
//                 <div>
//                   <p>Phone Number</p>
//                   {maid?.phoneNumber}
//                 </div>
//                 <div>
//                   <p>Cleaning Status</p>
//                   {maid?.status}
//                 </div>
//               </MaidMobile>
//             </Link>
//           </div>
//         ))}
//     </>
//   );
// };

const ChatList = () => {
  const dispatch = useDispatch();
  const [search, setSearch] = useState('');
  const [chats, setChats] = useState([]);
  const [loading, setLoading] = useState([]);

  const { user } = useSelector((state) => state.userInfo);

  // const wordMatch = (name, searchVal) => {
  //   const splitPattern = /[\s,\,\!]+/;
  //   return name
  //     .split(splitPattern)
  //     .join('')
  //     .includes(searchVal.split(splitPattern).join(''));
  // };

  useEffect(() => {
    setLoading(true);
    const ref = database.ref('/conversations');
    ref.once('value', function (snapshot) {
      const conversations = Object.values(snapshot.val());

      const filteredConversations = conversations?.filter((chat) => {
        return chat?.userIDs?.includes(user?.uid);
      });

      setChats(filteredConversations);
      setLoading(false);
    });
  }, []);
  // console.log(chats);

  useEffect(() => {
    loading ? NProgress.start() : NProgress.done();
  });

  console.log(chats);

  const wordMatch = (name, searchVal) => {
    const splitPattern = /[\s,\,\!]+/;
    return name
      .split(splitPattern)
      .join('')
      .includes(searchVal.split(splitPattern).join(''));
  };

  const renderFilteredChats = () =>
    chats.filter((chat) => {
      if (!search || search === '') return true;
      return wordMatch(
        `${chat.firstName},${chat.lastName},`.toLowerCase(),
        search.toLowerCase().trim()
      );
    });

  return (
    <>
      <BookContent>
        <BookTitle>Chat support</BookTitle>
        <AdditionalInputField
          type='text'
          placeholder='Search'
          icon={searchIcon}
          value={search}
          onChange={(e) => setSearch(e.target.value)}
        />
        {chats
          ?.sort((a, b) => {
            return b?.timestamp - a?.timestamp;
          })
          .map((chat, i) => (
            <Conversation key={i} chat={chat} uid={user?.uid} />
          ))}
      </BookContent>
    </>
  );
};

export default ChatList;
