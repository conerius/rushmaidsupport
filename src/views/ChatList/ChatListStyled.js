import styled from 'styled-components';

const ChatConversation = styled.div`
  width: 100%;
  padding: 17px;
  background: ${(props) =>
    props.read === true || props.read === undefined
      ? 'linear-gradient(147.42deg, rgba(64, 72, 93, 0.4) 6.32%, rgba(96, 106, 130, 0.4) 92.25%), linear-gradient(128deg, #E6E7ED -79.65%, #F7F8FA 151.25%)'
      : 'linear-gradient(147.42deg, rgba(64, 72, 93, 0.4) 6.32%, rgba(96, 106, 130, 0.4) 92.25%), linear-gradient(128deg, #F7F8FA -26.74%, #E8DDFF 151.25%)'};
  background-blend-mode: soft-light, normal;
  box-shadow: -7px -7px 16px #fafbfc, 4px 3px 19px #bdc1d1,
    inset -1px -1px 16px #f5f6fa, inset 1px 1px 16px #e9eaf2;
  border-radius: 8px;
  cursor: pointer;
  display: flex;
  align-items: center;
  margin-bottom: 20px;
  h6 {
    font-size: 14px;
    text-transform: uppercase;
    color: #9a7df8;
    margin-bottom: 10px !important;
  }
  h5 {
    font-weight: bold;
    font-size: 16px;
    line-height: 22px;
    color: #000000;
  }
  p {
    font-weight: 300;
    font-size: 16px;
    line-height: 22px;
    color: #000000;
    display: block;
  }
`;
const ChatImage = styled.div`
  width: 75px;
  height: 75px;
  margin-right: 20px;
  flex: none;
  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
    border-radius: 50%;
  }
`;

const Date = styled.div`
  font-weight: 300;
  font-size: 14px;
  line-height: 19px;
  color: #979797;
  display: flex;
  align-items: flex-end;
`;

const ChatConversationMessage = styled.div`
  justify-content: space-between;
  display: flex;
  align-items: flex-end;
`;

export { ChatConversation, ChatImage, Date, ChatConversationMessage };
