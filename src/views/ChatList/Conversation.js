/* eslint-disable react/no-unescaped-entities */
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { getMaids } from '../../actions/maidsActions';

import NProgress from 'nprogress';
import { Link } from 'react-router-dom';
import { BookContent, BookTitle, FlexBetween } from '../Maids/MaidsStyled';

import AdditionalInputField from '../../components/AdditionalInputField';

import searchIcon from '../../assets/icons/searchInput.svg';
import { Flex } from '../NewDisinfection/NewDisinfectionStyled';
import {
  ChatImage,
  ChatConversation,
  Date,
  ChatConversationMessage,
} from './ChatListStyled';
import { getConversations } from '../../actions/chatActions';
import { auth, database } from '../../config/firebase';
import { momentDate } from '../../helperFunction';

const Conversation = ({ chat, uid }) => {
  const filteredUid = chat?.userIDs?.filter((chat, i) => {
    return chat !== uid && i < 2;
  });

  const [chatUser, setChatUser] = useState({});
  const [loading, setLoading] = useState(false);

  // console.log(filteredUid);

  useEffect(() => {
    setLoading(true);
    const ref = database.ref(`/users`);
    ref
      .orderByChild('uid')
      .equalTo(filteredUid[0])
      .once('value', function (snapshot) {
        const conversationUser = Object.values(snapshot.val());
        setChatUser(conversationUser[0]);
        setLoading(false);
        // console.log(conversationUser[0]);
      });
  }, []);

  // console.log(chatUser);
  // console.log(chat);

  // const IsRead = () => {
  //   if (chat?.isRead) {
  //     const read = Object.keys(chat?.isRead).filter((read, i) => {
  //       return read[i] == filteredUid;
  //     });
  //     return read;
  //   } else return null;
  // };

  // console.log(chat?.isRead && chat?.isRead[uid]);

  return (
    <Link to={`/chat/${chat?.id}`}>
      {!loading && (
        <ChatConversation read={chat?.isRead && chat?.isRead[uid]}>
          <ChatImage>
            <img src={chatUser?.imageURL} alt='user' />
          </ChatImage>
          <div style={{ width: '100%' }}>
            <h5>{chatUser?.firstName + ' ' + chatUser?.lastName}</h5>
            <h6>MAID</h6>
            <ChatConversationMessage>
              {chat?.lastMessage ? (
                <p>{chat?.lastMessage}</p>
              ) : (
                <Date>no messages yet</Date>
              )}
              <Date>{momentDate(chat?.timestamp)}</Date>
            </ChatConversationMessage>
          </div>
        </ChatConversation>
      )}
    </Link>
  );
};

export default Conversation;
