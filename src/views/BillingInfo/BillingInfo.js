/* eslint-disable react/no-unescaped-entities */
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import NProgress from 'nprogress';
import moment from 'moment';

import {
  BookContent,
  BookTitle,
  NextButton,
  ProfileTab,
  FlexBetween,
  Subtitle,
  HalfBox,
  FlexBetweenSmall,
} from './BillingInfoStyled';
import AdditionalInputField from '../../components/AdditionalInputField';

import Card1 from '../../assets/images/card1.svg';
import Card2 from '../../assets/images/card2.svg';

import Cleave from 'cleave.js/react';

import ErrorMessage from '../../components/ErrorMessage';

import { createBooking, createFreeBooking } from '../../actions/bookingActions';

import {
  createDisinfection,
  createDisinfectionPayless,
} from '../../actions/disinfectionActions';

import {
  getCustomer,
  getCardToken,
  createCustomer,
  createCreditCard,
} from '../../actions/stripeActions';

const BillingInfo = ({ history, location, match }) => {
  const [cardNumber, setCardNumber] = useState('');
  const [cardExpireDate, setCardExpireDate] = useState('');
  const [cardCvc, setCardCvc] = useState('');
  const [errorMsg, setErrorMsg] = useState('');

  const dispatch = useDispatch();

  const { user } = useSelector((state) => state.userInfo);
  const { customer, customerId, loading, error } = useSelector(
    (state) => state.stripeInfo
  );
  const {
    tabs,
    bookingTotalPrice,
    bookingDate,
    bookingDiscount,
    bookingPromoCodeId,
  } = useSelector((state) => state.bookings);

  const {
    disinfectionDate,
    value,
    disinfectionTotalPrice,
    disinfectionAddress,
    disinfectionCoordinates,
    disinfectionZipCode,
    disinfectionPromoCode,
    disinfectionDiscount,
    disinfectionPromoCodeId,
  } = useSelector((state) => state.disinfections);

  const { selectedIndex } = useSelector((state) => state.categories);

  const { bookingAddress, bookingCoordinates, questions, zipCode } =
    useSelector((state) => state.additionalQuestions);

  const addCard = async (e) => {
    e.preventDefault();

    const tokenResponse = await getCardToken(
      user,
      cardNumber,
      cardExpireDate,
      cardCvc
    );

    if (!cardNumber && !cardExpireDate && !cardCvc) {
      setErrorMsg('Please fill all fields');
    } else {
      setErrorMsg('');

      if (tokenResponse.error) {
        setErrorMsg(tokenResponse.message);
      } else {
        setErrorMsg('');

        if (location.state) {
          if (location.state.from === '/CompleteBooking') {
            const customerResponse = await createCustomer(
              user,
              tokenResponse,
              dispatch
            );

            bookingDiscount === 100
              ? dispatch(
                  createFreeBooking(
                    user,
                    bookingDate,
                    bookingAddress,
                    bookingCoordinates.lng,
                    bookingCoordinates.lat,
                    tabs[0].num,
                    tabs[1].num,
                    tabs[2].num,
                    tabs[3].num,
                    tabs[4].num,
                    selectedIndex,
                    bookingTotalPrice,
                    bookingPromoCodeId,
                    questions,
                    zipCode,
                    history
                  )
                )
              : dispatch(
                  createBooking(
                    customerResponse,
                    user,
                    bookingDate,
                    bookingAddress,
                    bookingCoordinates.lng,
                    bookingCoordinates.lat,
                    tabs[0].num,
                    tabs[1].num,
                    tabs[2].num,
                    tabs[3].num,
                    tabs[4].num,
                    selectedIndex,
                    bookingTotalPrice,
                    bookingPromoCodeId,
                    questions,
                    zipCode,
                    history
                  )
                );
          } else if (location.state.from === '/NewDisinfection') {
            const customerResponse = await createCustomer(
              user,
              tokenResponse,
              dispatch
            );

            disinfectionDiscount === 100
              ? dispatch(
                  createDisinfectionPayless(
                    moment(disinfectionDate).unix(),
                    value,
                    disinfectionAddress,
                    disinfectionCoordinates.lng,
                    disinfectionCoordinates.lat,
                    user,
                    disinfectionPromoCodeId,
                    disinfectionZipCode,
                    history
                  )
                )
              : dispatch(
                  createDisinfection(
                    moment(disinfectionDate).unix(),
                    value,
                    disinfectionAddress,
                    disinfectionCoordinates.lng,
                    disinfectionCoordinates.lat,
                    user,
                    customerResponse,
                    disinfectionPromoCode,
                    disinfectionTotalPrice,
                    disinfectionZipCode,
                    history
                  )
                );
          }
        } else {
          !customerId
            ? createCustomer(user, tokenResponse, dispatch)
            : dispatch(createCreditCard(user, customer, tokenResponse));
        }
      }
    }
  };

  useEffect(() => {
    customerId && dispatch(getCustomer(customerId));
    loading ? NProgress.start() : NProgress.done();
  }, [dispatch, match, loading, customerId]);

  return (
    <BookContent>
      <BookTitle>Billing Info</BookTitle>
      <FlexBetween>
        <HalfBox>
          <Subtitle>Card List</Subtitle>
          {customer &&
            customer.sources.data.map((card) => (
              <ProfileTab key={card.id}>
                <p>**** **** **** {card.last4}</p>
                <img src={card.brand === 'Visa' ? Card1 : Card2} alt='Edit' />
              </ProfileTab>
            ))}
        </HalfBox>

        <HalfBox>
          <Subtitle>Add Payment</Subtitle>
          <form onSubmit={addCard}>
            <div className='billingInput-wrapper'>
              <Cleave
                placeholder='0000 0000 0000 0000'
                options={{ creditCard: true }}
                onChange={(e) => setCardNumber(e.target.value)}
                className='billingInput'
              />
            </div>

            <FlexBetweenSmall>
              <div style={{ width: 'calc(50% - 15px)' }}>
                <p className='billingInput-expiry'>Expiry Date</p>
                <div className='billingInput-wrapper'>
                  <Cleave
                    placeholder='00/00'
                    options={{ date: true, datePattern: ['m', 'y'] }}
                    onChange={(e) => setCardExpireDate(e.target.value)}
                    className='billingInput'
                  />
                </div>
              </div>
              <div style={{ width: 'calc(50% - 15px)' }}>
                <AdditionalInputField
                  type={'text'}
                  placeholder={'000'}
                  label={'CVC'}
                  // id={"address"}
                  // name={""}
                  value={cardCvc}
                  onChange={(e) => setCardCvc(e.target.value)}
                />
              </div>
            </FlexBetweenSmall>
            <NextButton className='mt-3' type='submit'>
              Add Card
            </NextButton>
          </form>
          {error && <ErrorMessage error={error} />}
          {errorMsg && <ErrorMessage error={errorMsg} />}
        </HalfBox>
      </FlexBetween>
    </BookContent>
  );
};

export default BillingInfo;
