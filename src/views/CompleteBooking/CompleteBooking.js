/* eslint-disable react/no-unescaped-entities */
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  getBookingTotalPrice,
  createBooking,
  createFreeBooking,
  setBookingPromoCodeId,
  setBookingDiscount,
  setBookingPromoCode,
} from '../../actions/bookingActions';

import { getCustomer } from '../../actions/stripeActions';

import { getPromoCode } from '../../actions/promoCodesActions';

import {
  BookContent,
  BookTab,
  BookTabNum,
  BookTitle,
  Flex,
  FlexBetween,
  TimeTab,
  DateTab,
  NextButton,
  BookTabNumWrap,
  PricesTextPrice,
  PricesText,
  BookTabPrice,
  BookTabTimeDateLoc,
  LocationTab,
  TotalPrice,
  Between40,
  Between60,
  PromoCode,
  FlexBetweenStatic,
} from './CompleteBookingStyled';
import locationIcon from '../../assets/icons/location.svg';
import PromoCodeField from '../../components/PromoCodeField';
import moment from 'moment';
import ErrorMessage from '../../components/ErrorMessage';

import NProgress from 'nprogress';

const Tabs = ({ title, icon, number }) => {
  return (
    <BookTab>
      <Flex>
        <BookTabNumWrap>
          <BookTabNum>{number}</BookTabNum>
        </BookTabNumWrap>
      </Flex>
      <Flex className='ml-4'>
        <img src={icon} alt='Area' />
        <p>{title}</p>
      </Flex>
    </BookTab>
  );
};

const Prices = (props) => {
  return (
    <BookTabPrice>
      <FlexBetweenStatic>
        <PricesText>{props.name}</PricesText>
        <PricesTextPrice>${props.price}</PricesTextPrice>
      </FlexBetweenStatic>
    </BookTabPrice>
  );
};

const BookNowView = ({ history, location }) => {
  const format = 'HH:mm A';
  const dispatch = useDispatch();

  const { user } = useSelector((state) => state.userInfo);

  const { customer, customerId } = useSelector((state) => state.stripeInfo);

  const {
    tabs,
    bookingTotal,
    bookingTotalPrice,
    bookingDate,
    bookingTime,
    bookingPromoCode,
    bookingDiscount,
    bookingPromoCodeId,
    loading,
    error,
  } = useSelector((state) => state.bookings);

  const { categoriesTotal, selectedIndex } = useSelector(
    (state) => state.categories
  );

  const {
    bookingAddress,
    bookingCoordinates,
    questions,
    pets,
    airBnb,
    zipCode,
  } = useSelector((state) => state.additionalQuestions);

  const { promoCodes } = useSelector((state) => state.promoCodes);

  const completeBooking = () => {
    if (!customerId) {
      history.push({
        pathname: '/BillingInfo',
        state: {
          from: location.pathname,
        },
      });
    } else {
      bookingDiscount === 100
        ? dispatch(
            createFreeBooking(
              user,
              bookingDate,
              bookingAddress,
              bookingCoordinates.lng,
              bookingCoordinates.lat,
              tabs[0].num,
              tabs[1].num,
              tabs[2].num,
              tabs[3].num,
              tabs[4].num,
              selectedIndex,
              bookingTotalPrice,
              bookingPromoCodeId,
              questions,
              zipCode,
              history
            )
          )
        : dispatch(
            createBooking(
              customer,
              user,
              bookingDate,
              bookingAddress,
              bookingCoordinates.lng,
              bookingCoordinates.lat,
              tabs[0].num,
              tabs[1].num,
              tabs[2].num,
              tabs[3].num,
              tabs[4].num,
              selectedIndex,
              bookingTotalPrice,
              bookingPromoCodeId,
              questions,
              zipCode,
              history
            )
          );
    }
  };

  const getDiscount = () => {
    promoCodes.forEach((code) => {
      if (code.code === bookingPromoCode) {
        dispatch(setBookingDiscount(code.discount));
        dispatch(setBookingPromoCode(`${code.discount} %`));
        dispatch(setBookingPromoCodeId(code.selfID));

        let totalValue = (code.discount / 100) * bookingTotalPrice;

        const bookingDiscount = bookingTotalPrice - totalValue;

        dispatch(
          getBookingTotalPrice(
            bookingTotal,
            categoriesTotal,
            pets,
            airBnb,
            bookingDiscount
          )
        );
      }
    });
  };

  useEffect(() => {
    dispatch(
      getBookingTotalPrice(
        bookingTotal,
        categoriesTotal,
        pets,
        airBnb,
        bookingDiscount
      )
    );

    customerId && dispatch(getCustomer(customerId));

    dispatch(getPromoCode());

    loading ? NProgress.start() : NProgress.done();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, bookingDiscount, loading]);

  return (
    <BookContent>
      <BookTitle>Complete Booking</BookTitle>
      <FlexBetween>
        <Between40>
          {tabs.map((tab, index) => (
            <Tabs
              key={index}
              title={tab.title}
              icon={tab.icon}
              number={tab.num}
            />
          ))}
        </Between40>
        <Between60>
          <BookTabTimeDateLoc>
            <FlexBetween style={{ borderBottom: '1px solid #DEDEDE' }}>
              <DateTab>
                <div>
                  <p>date</p>
                  <h3>{moment(bookingDate).format('MMM D, YYYY')}</h3>
                </div>
              </DateTab>
              <TimeTab>
                <div>
                  <p>time</p>
                  <h3>{moment(bookingTime).format(format)}</h3>
                </div>
              </TimeTab>
            </FlexBetween>
            <Flex>
              <LocationTab>
                <img src={locationIcon} alt='' />
                <div>
                  <p>CLEANING ADDRESS</p>
                  <h3>{bookingAddress}</h3>
                </div>
              </LocationTab>
            </Flex>
          </BookTabTimeDateLoc>
          <div style={{ marginBottom: 35 }}>
            <Prices name={'Total Cleaning'} price={bookingTotal} />
            <Prices name={'Extra Services'} price={categoriesTotal} />
            <Prices name={'Pets'} price={pets ? 10 : 0} />
            <Prices name={'Airbnb host'} price={airBnb ? 10 : 0} />
          </div>
          <FlexBetween>
            <PromoCode>
              <PromoCodeField
                label={'Enter Promo Code'}
                value={bookingPromoCode}
                discount={bookingDiscount}
                onChange={(e) => {
                  dispatch(setBookingPromoCode(e));
                  if (bookingPromoCode) {
                    dispatch(setBookingDiscount(''));

                    dispatch(
                      getBookingTotalPrice(
                        bookingTotal,
                        categoriesTotal,
                        pets,
                        airBnb,
                        bookingDiscount
                      )
                    );
                  }
                }}
              />
              <NextButton onClick={getDiscount}>Submit</NextButton>
            </PromoCode>
            <TotalPrice>
              <div>
                <p>Total Price</p>
                <h3>${bookingTotalPrice.toFixed(2)}</h3>
              </div>
            </TotalPrice>
          </FlexBetween>
        </Between60>
      </FlexBetween>
      <FlexBetween>
        <Between40>
          <p style={{ textAlign: 'center', fontSize: '13px' }}>
            A disclaimer is generally any statement intended to specify or
            delimit the scope of rights and obligations that may.
          </p>
        </Between40>
        <Between60>
          <NextButton style={{ marginBottom: '5px' }} onClick={completeBooking}>
            Complete Booking
          </NextButton>
          <ErrorMessage error={error} />
        </Between60>
      </FlexBetween>
    </BookContent>
  );
};

export default BookNowView;
