/* eslint-disable react/no-unescaped-entities */
import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';

import { useDispatch, useSelector } from 'react-redux';
import {
  handleSelectComplete,
  getQuestionsAll,
} from '../../actions/additionalQuestionAction';

import { v4 as uuidv4 } from 'uuid';
import PlacesAutocomplete from 'react-places-autocomplete';
import { geocodeByAddress, getLatLng } from 'react-places-autocomplete';

import {
  PetTab,
  PetTabDropdown,
  BookContent,
  FlexBetween,
  NextButton,
  BookTabWrap,
  BookTitle,
  BookTitleMain,
} from './AdditionalQuestionsStyled';
import AdditionalInputField from '../../components/AdditionalInputField';
import ErrorMessage from '../../components/ErrorMessage';
import TextArea from '../../components/AdditionalTextarea';
import arrow from '../../assets/icons/arrow.svg';

const API_URL = process.env.REACT_APP_API_URL;

const AdditionalQuestions = ({ history }) => {
  const dropdownRef = useRef(null);
  const airDropdownRef = useRef(null);
  const [address, setAddress] = useState('');
  const [openModal, setOpenModal] = useState(false);
  const [airBnbModal, setAirBnbModal] = useState(false);
  const [pets, setPets] = useState(true);
  const [airBnb, setAirbnb] = useState(true);
  const [questionKey, setQuestionKey] = useState('');
  const [questionUrgent, setQuestionUrgent] = useState('');
  const [questionEntry, setQuestionEntry] = useState('');
  const [questionSupplies, setQuestionSupplies] = useState('');
  const [notes, setNotes] = useState('');

  const [coordinates, setCoordinates] = useState({
    lat: null,
    lng: null,
  });

  const [zipCode, setZipCode] = useState('');
  const [errorMsg, setErrorMsg] = useState('');

  const dispatch = useDispatch();

  const { user } = useSelector((state) => state.userInfo);

  const setPetsTrue = () => {
    setPets(true);
  };

  const setPetsFalse = () => {
    setPets(false);
  };

  const setAirBnbTrue = () => {
    setAirbnb(true);
  };

  const setAirBnbFalse = () => {
    setAirbnb(false);
  };

  const onHandleClick = () => {
    setOpenModal(!openModal);
  };

  const onHandleAirBnbClick = () => {
    setAirBnbModal(!airBnbModal);
  };

  const handleClickOutside = (event) => {
    if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
      setOpenModal(false);
    } else if (
      airDropdownRef.current &&
      !airDropdownRef.current.contains(event.target)
    ) {
      setAirBnbModal(false);
    }
  };

  const questionsAll = async () => {
    const q1 = uuidv4();
    const q2 = uuidv4();
    const q3 = uuidv4();
    const q4 = uuidv4();
    const q5 = uuidv4();
    const q6 = uuidv4();
    const q7 = uuidv4();

    const questions = [
      {
        answer: pets ? 'Yes' : 'No',
        index: 0,
      },
      {
        answer: questionKey,
        index: 1,
      },
      {
        answer: questionUrgent,
        index: 2,
      },
      {
        answer: questionEntry,
        index: 3,
      },
      {
        answer: questionSupplies,
        index: 4,
      },
      {
        answer: notes,
        index: 5,
      },
      {
        answer: airBnb ? 'Yes' : 'No',
        index: 6,
      },
    ];

    let questionsMap = new Map();
    questionsMap[q1] = questions[0];
    questionsMap[q2] = questions[1];
    questionsMap[q3] = questions[2];
    questionsMap[q4] = questions[3];
    questionsMap[q5] = questions[4];
    questionsMap[q6] = questions[5];
    questionsMap[q7] = questions[6];

    dispatch(getQuestionsAll(questionsMap, pets, airBnb, zipCode));
  };

  const getZipCodeStatus = async (zipCode) => {
    try {
      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.token}`,
        },
      };

      const { data } = await axios.get(
        `${API_URL}/booking/zip-code-availability/${zipCode}`,
        config
      );

      return data;
    } catch (error) {
      return error.response.data;
    }
  };

  const getQuestions = async () => {
    const zipStatus = await getZipCodeStatus(zipCode);

    if (zipStatus.error) {
      setErrorMsg(zipStatus.message);
    } else {
      questionsAll();

      history.push('/CompleteBooking');
      setErrorMsg('');
    }
  };

  const handleSubmitForm = (e) => {
    e.preventDefault();
    !address
      ? setErrorMsg('Please enter cleaning address')
      : !zipCode
      ? setErrorMsg('Please enter zip code')
      : getQuestions();
  };

  const handleSelect = async (value) => {
    const result = await geocodeByAddress(value);
    const latlng = await getLatLng(result[0]);

    setAddress(value);
    setCoordinates(latlng);

    dispatch(handleSelectComplete(value));
  };

  useEffect(() => {
    window.addEventListener('mousedown', handleClickOutside);

    // returned function will be called on component unmount

    return () => {
      window.removeEventListener('mousedown', handleClickOutside);
    };
  }, [dispatch]);

  return (
    <BookContent>
      <form onSubmit={handleSubmitForm}>
        <BookTitleMain>AdditionalQuestions</BookTitleMain>

        <div style={{ marginBottom: 35 }}>
          <FlexBetween>
            <BookTabWrap>
              <BookTitle>What is your address?</BookTitle>
              <PlacesAutocomplete
                value={address}
                onChange={setAddress}
                onSelect={handleSelect}
              >
                {({
                  getInputProps,
                  suggestions,
                  getSuggestionItemProps,
                  loading,
                }) => (
                  <div style={{ position: 'relative' }}>
                    <AdditionalInputField
                      {...getInputProps({
                        type: 'text',
                        placeholder: 'Enter here...',
                      })}
                    />
                    <div className='suggestions' style={{ marginTop: '22px' }}>
                      {loading ? <div>...loading</div> : null}
                      {suggestions.map((suggestion) => (
                        <div
                          {...getSuggestionItemProps(suggestion)}
                          key={suggestion.placeId}
                          style={{ cursor: 'pointer' }}
                        >
                          {suggestion.description}
                        </div>
                      ))}
                    </div>
                  </div>
                )}
              </PlacesAutocomplete>
              <BookTitle>Enter zip code</BookTitle>
              <AdditionalInputField
                type={'text'}
                placeholder={'Enter here...'}
                value={zipCode}
                onChange={(e) => setZipCode(e.target.value)}
              />

              <BookTitle>
                Will you be home or away during the cleaning? How will made
                receive the key?
              </BookTitle>
              <AdditionalInputField
                type={'text'}
                placeholder={'Enter here...'}
                // id={"address"}
                // name={""}
                value={questionKey}
                onChange={(e) => setQuestionKey(e.target.value)}
              />

              <BookTitle>
                Do you have urgent matters needed to be attended to?
              </BookTitle>
              <AdditionalInputField
                type={'text'}
                placeholder={'Enter here...'}
                // id={"address"}
                // name={""}
                value={questionUrgent}
                onChange={(e) => setQuestionUrgent(e.target.value)}
              />
            </BookTabWrap>
            <BookTabWrap>
              <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <div>
                  <BookTitle>Do you have any pets?</BookTitle>
                  <div ref={dropdownRef}>
                    <PetTab onClick={onHandleClick}>
                      <p>{pets ? 'Yes' : 'No'}</p>
                      {openModal && (
                        <PetTabDropdown>
                          <ul>
                            <li onClick={setPetsTrue}>Yes</li>
                            <li onClick={setPetsFalse}>No</li>
                          </ul>
                        </PetTabDropdown>
                      )}
                      {!openModal && (
                        <img
                          src={arrow}
                          alt='arrow'
                          style={{ cursor: 'pointer' }}
                        />
                      )}
                    </PetTab>
                  </div>
                </div>
                <div>
                  <BookTitle>Are you Airbnb host?</BookTitle>
                  <div ref={airDropdownRef}>
                    <PetTab onClick={onHandleAirBnbClick}>
                      <p>{airBnb ? 'Yes' : 'No'}</p>
                      {airBnbModal && (
                        <PetTabDropdown>
                          <ul>
                            <li onClick={setAirBnbTrue}>Yes</li>
                            <li onClick={setAirBnbFalse}>No</li>
                          </ul>
                        </PetTabDropdown>
                      )}
                      {!airBnbModal && (
                        <img
                          src={arrow}
                          alt='arrow'
                          style={{ cursor: 'pointer' }}
                        />
                      )}
                    </PetTab>
                  </div>
                </div>
              </div>

              <BookTitle>
                What do we need to ask your front desk/concierge to gain entry?
              </BookTitle>
              <AdditionalInputField
                type={'text'}
                placeholder={'Enter here...'}
                // id={"address"}
                // name={""}
                value={questionEntry}
                onChange={(e) => setQuestionEntry(e.target.value)}
              />
              <BookTitle>
                Do you have proper cleaning supplies necessary for a clean home?
              </BookTitle>
              <AdditionalInputField
                type={'text'}
                placeholder={'Enter here...'}
                // id={"address"}
                // name={""}
                value={questionSupplies}
                onChange={(e) => setQuestionSupplies(e.target.value)}
              />
            </BookTabWrap>
          </FlexBetween>
          <BookTitle>Additional notes?</BookTitle>
          <TextArea
            type={'textarea'}
            placeholder={'Enter here...'}
            // id={"address"}
            // name={""}
            value={notes}
            onChange={(e) => setNotes(e.target.value)}
          />
        </div>

        <NextButton type='submit'>next</NextButton>
      </form>
      {errorMsg && <ErrorMessage error={errorMsg} />}
    </BookContent>
  );
};

export default AdditionalQuestions;
