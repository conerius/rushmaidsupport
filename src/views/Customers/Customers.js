/* eslint-disable react/no-unescaped-entities */
import React, { useState, useEffect, useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  PageContent,
  PageTitle,
  User,
  Columns,
  UserMobile,
} from './CustomersStyled';

import repeat from '../../assets/icons/repeat.svg';
import corona from '../../assets/icons/coronavirus.svg';
import { Link } from 'react-router-dom';

import { getAppointments } from '../../actions/appointmentsActions';
import { getCategories } from '../../actions/categoriesActions';

import moment from 'moment';
import { getMaids } from '../../actions/maidsActions';

import NProgress from 'nprogress';
import { getCustomers } from '../../actions/customersActions';

const Customer = ({ customers }) => {
  return (
    <>
      {customers &&
        customers.map((customer, i) => (
          <div key={i}>
            <Link
              to={`/UserDetails/customer/${customer?.user_id}`}
              key={customer?.user_id}
            >
              <User>
                <div style={{ width: '15%' }}>{customer?.first_name}</div>
                <div style={{ width: '17%' }}>{customer?.last_name}</div>
                <div style={{ width: '30%' }}>{customer?.email}</div>
                <div style={{ width: '25%' }}>{customer?.phone_number}</div>
                <div style={{ width: '13%' }}>
                  {customer?.appointments?.length}
                </div>
              </User>
              <UserMobile>
                <div>
                  <p>First Name</p>
                  {customer?.first_name}
                </div>
                <div>
                  <p>Last Name</p>
                  {customer?.last_name}
                </div>
                <div>
                  <p>Email</p>
                  {customer?.email}
                </div>
                <div>
                  <p>Phone Number</p>
                  {customer?.phone_number}
                </div>
                <div>
                  <p>Appointment Num</p>
                  {customer?.appointments?.length}
                </div>
              </UserMobile>
            </Link>
          </div>
        ))}
    </>
  );
};

const Customers = () => {
  const dispatch = useDispatch();

  const { customers, loadingCustomers } = useSelector(
    (state) => state.customers
  );

  useEffect(() => {
    dispatch(getCustomers());
  }, [dispatch]);

  useEffect(() => {
    loadingCustomers ? NProgress.start() : NProgress.done();
  });

  // console.log(customers);

  return (
    <>
      <PageContent>
        <PageTitle>Customers</PageTitle>
        <Columns>
          <div style={{ width: '15%' }}>First Name</div>
          <div style={{ width: '17%' }}>Last Name</div>
          <div style={{ width: '30%' }}>Email</div>
          <div style={{ width: '25%' }}>Phone Number</div>
          <div style={{ width: '13%' }}>Appointment Num</div>
        </Columns>
        <Customer customers={customers} />
      </PageContent>
    </>
  );
};

export default Customers;
