import styled from 'styled-components';

const BookNow = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  padding: 100px 0;
`;
const PageContent = styled.div`
  max-width: 1500px;
  width: 100%;
`;
const PageTitle = styled.div`
  font-size: 20px;
  margin-bottom: 30px;
`;

const User = styled.div`
  display: flex;
  align-items: center;
  // justify-content: space-between;
  height: 66px;
  padding: 15px;
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    linear-gradient(133.76deg, #e6e7ed -18.17%, #f7f8fa 140.27%);
  background-blend-mode: soft-light, normal;
  box-shadow: -2px -3px 8px #fafafc, 1px 1px 5px #b1b5c6,
    inset 1px 1px 3px #ebecf0;
  border-radius: 8px;
  margin-bottom: 15px;
  font-weight: 600;
  font-size: 16px;
  line-height: 20px;
  color: #000000;
  word-break: break-all;
  @media (max-width: 1300px) {
    display: none;
  }
`;
const UserMobile = styled.div`
  display: none;
  /* padding: 15px; */
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    linear-gradient(133.76deg, #e6e7ed -18.17%, #f7f8fa 140.27%);
  background-blend-mode: soft-light, normal;
  box-shadow: -2px -3px 8px #fafafc, 1px 1px 5px #b1b5c6,
    inset 1px 1px 3px #ebecf0;
  border-radius: 8px;
  margin-bottom: 15px;
  font-weight: 600;
  font-size: 16px;
  line-height: 20px;
  color: #000000;
  word-break: break-all;
  @media (max-width: 1300px) {
    display: block;
  }

  div {
    font-weight: 600;
    font-size: 16px;
    line-height: 20px;
    color: #000000;
    padding: 15px;
    border-bottom: 1px solid #d8d8d8;
  }
  p {
    font-weight: normal;
    font-size: 13px;
    line-height: 18px;
    color: #9a7df8;
    margin-bottom: 6px;
  }
`;

const Columns = styled.div`
  font-weight: normal;
  font-size: 13px;
  line-height: 18px;
  color: #9a7df8;
  display: flex;
  align-items: center;
  margin-bottom: 15px;
  height: 33px;
  padding: 15px;
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    linear-gradient(133.76deg, #e6e7ed -18.17%, #f7f8fa 140.27%);
  background-blend-mode: soft-light, normal;
  box-shadow: -2px -3px 8px #fafafc, 1px 1px 5px #b1b5c6,
    inset 1px 1px 3px #ebecf0;
  border-radius: 8px;
  @media (max-width: 1300px) {
    display: none;
  }
`;

export { BookNow, PageContent, PageTitle, User, UserMobile, Columns };
