/* eslint-disable react/no-unescaped-entities */
import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { IsValidMail } from '../../helperFunction';
import { Link } from 'react-router-dom';
import facebook from '../../assets/images/fb.svg';
import logo1 from '../../assets/images/logo2text.svg';
import apple from '../../assets/images/apple.svg';
import {
  Login,
  Logo,
  FormContainer,
  TextBig,
  SubmitButton,
  SignUpButton,
  SocialButton,
} from './RegisterStyled';
import InputField from '../../components/InputField';
import Animation from '../../components/Animation';
import ErrorMessage from '../../components/ErrorMessage';

import NProgress from 'nprogress';

import { signUp, appleLogin, facebookLogin } from '../../actions/authActions';

const StepOneView = (props) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [referralCode, setReferralCode] = useState('');
  const [error, setError] = useState('');
  // const { appleLogin, authErrorAPPLE } = useContext(AuthContext);

  const history = useHistory();

  const dispatch = useDispatch();
  const userInfo = useSelector((state) => state.userInfo);

  const handleClick = (event) => {
    event.preventDefault();
    if (
      firstName.length === 0 ||
      lastName.length === 0 ||
      email.length === 0 ||
      password.length === 0
    ) {
      setError('Please fill all the fields.');
    }
    // else if (!isValidPhoneNumber(phoneNumber)) {
    //   setError('Please provide valid phone number.');
    // }
    else if (password.length < 6) {
      setError(`Password must be at least 6 characters`);
    } else if (!IsValidMail(email)) {
      setError(`Please provide valid email address`);
    } else {
      setError('');
      // props.updateUser(firstName, lastName, email, password, referralCode);
      dispatch(
        signUp(
          email,
          password,
          {
            firstName,
            lastName,
            email,
            referralCode,
          },
          history
        )
      );
    }
  };

  useEffect(() => {
    userInfo.loading ? NProgress.start() : NProgress.done();
  }, [dispatch, userInfo.loading]);

  return (
    <Animation>
      <Login>
        <Logo>
          <img src={logo1} alt='logo' />
        </Logo>
        <FormContainer>
          <TextBig className='mb-4'>
            Don’t stress less Rushmaid clean that mess
          </TextBig>
          {error && <ErrorMessage error={error} />}
          {userInfo.error && <ErrorMessage error={userInfo.error} />}
          <InputField
            id={'firstName'}
            type={'text'}
            placeholder={'First Name'}
            name={'firstName'}
            value={firstName}
            onChange={setFirstName}
          />
          <InputField
            id={'lastName'}
            type={'text'}
            placeholder={'Last Name'}
            name={'lastName'}
            value={lastName}
            onChange={setLastName}
          />
          <InputField
            id={'email'}
            type={'email'}
            placeholder={'Email'}
            name={'email'}
            value={email}
            onChange={setEmail}
          />
          <InputField
            id={'password'}
            type={'password'}
            placeholder={'Password'}
            name={'password'}
            value={password}
            onChange={setPassword}
            label={'Password'}
          />
          <InputField
            id={'code'}
            type={'text'}
            placeholder={'Referal Code'}
            name={'code'}
            value={referralCode}
            onChange={setReferralCode}
          />

          <SubmitButton
            type='button'
            onClick={(e) => handleClick(e)}
            type={props.type}
          >
            sign up
          </SubmitButton>

          <div
            style={{
              width: '100%',
              display: 'flex',
              justifyContent: 'space-between',
            }}
          >
            <div
              onClick={() => dispatch(appleLogin())}
              style={{ width: '50px', cursor: 'pointer' }}
            >
              <SocialButton>
                <img src={apple} alt='apple' />
              </SocialButton>
            </div>
            <div
              onClick={() => dispatch(facebookLogin())}
              style={{ width: '50px', cursor: 'pointer' }}
            >
              <SocialButton>
                <img src={facebook} alt='apple' />
              </SocialButton>
            </div>
            <Link to='/login' style={{ width: '191px' }}>
              <SignUpButton>sign in</SignUpButton>
            </Link>
          </div>
        </FormContainer>
      </Login>
    </Animation>
  );
};

const Register = () => {
  const [currentStep, setCurrentStep] = useState(1);
  const [user, setUser] = useState({});

  const updateUserStepOne = (
    firstName,
    lastName,
    email,
    password,
    referralCode
  ) => {
    setUser({ ...user, firstName, lastName, email, password, referralCode });
  };

  let step = (
    <StepOneView
      updateUser={updateUserStepOne}
      setCurrentStep={setCurrentStep}
      currentStep={currentStep}
    />
  );

  return <>{step}</>;
};

export default Register;
