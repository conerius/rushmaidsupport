import styled from 'styled-components';
// import colors from "./colors";
// import sizes from "./sizes";

// const { base, margin, borderRadiusLarge } = sizes;

const Login = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 100px 0;
  flex-direction: column;
`;

// const Logo = styled.div`
// box-shadow: -10px -10px 42px #FAFCFC, 10px 10px 40px #BDC1D1, inset 1px 1px 9px #E6E7EB;
// background: linear-gradient(147.42deg, rgba(64, 72, 93, 0.4) 6.32%, rgba(96, 106, 130, 0.4) 92.25%), linear-gradient(128deg, #E6E7ED -79.65%, #F7F8FA 151.25%);
// background-blend-mode: soft-light, normal;
// border-radius: 40px;
// width: 150px;
// height: 150px;
// display: flex;
// align-items:center;
// justify-content: center;

// `;

const Logo = styled.div`
  width: 454px;
  img {
    width: 454px;
  }
  @media (max-width: 500px) {
    width: 90%;
    img {
      width: 100%;
    }
  }
`;

const Error = styled.div`
  padding: 20px;
  p {
    color: #d20f0f;
  }
`;

const FormContainer = styled.div`
  width: 361px;
  display: flex;
  flex-direction: column;
  align-items: center;
  @media (max-width: 400px) {
    width: 100%;
    padding: 20px;
  }
`;

const TextBig = styled.h2`
  // font-family: Nunito Sans;
  font-style: normal;
  font-weight: bold;
  font-size: 23px;
  line-height: 31px;
  text-align: center;
  letter-spacing: 0.8125px;
  margin: 58px 0 46px 0;
`;

const SubmitButton = styled.button`
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    linear-gradient(135deg, #b4b2f1 0%, #7838ff 100%);
  background-blend-mode: soft-light, normal;
  box-shadow: -2px -3px 8px #fafafc, 1px 1px 5px #b1b5c6,
    inset 1px 1px 3px #ebecf0;
  text-transform: uppercase;
  border-radius: 8px;
  width: 100%;
  height: 50px;
  padding: 0 19px;
  margin: 0 0 20px 0;
  outline: none;
  font-size: 16px;
  border: none;
  color: #ffffff;
  font-weight: bold;
  transition: all 0.2s;
  :hover {
    opacity: 0.8;
  }
`;

const SignUpButton = styled.button`
  background: linear-gradient(
      147.42deg,
      rgba(64, 72, 93, 0.4) 6.32%,
      rgba(96, 106, 130, 0.4) 92.25%
    ),
    linear-gradient(128deg, #e6e7ed -79.65%, #f7f8fa 151.25%);
  background-blend-mode: soft-light, normal;
  box-shadow: -7px -7px 16px #fafbfc, 4px 3px 19px #bdc1d1,
    inset -1px -1px 16px #f5f6fa, inset 1px 1px 16px #e9eaf2;
  border-radius: 8px;
  width: 100%;
  height: 50px;
  margin: 0 0 30px 0;
  text-transform: uppercase;
  outline: none;
  font-size: 16px;
  border: none;
  color: #9a7df8;
  font-weight: bold;
  transition: all 0.2s;
  :hover {
    opacity: 0.8;
  }
`;

const SocialButton = styled.div`
  width: 50px;
  height: 50px;

  background: linear-gradient(
      147.42deg,
      rgba(64, 72, 93, 0.4) 6.32%,
      rgba(96, 106, 130, 0.4) 92.25%
    ),
    linear-gradient(128deg, #e6e7ed -79.65%, #f7f8fa 151.25%);
  background-blend-mode: soft-light, normal;
  box-shadow: -7px -7px 16px #fafbfc, 4px 3px 19px #bdc1d1,
    inset -1px -1px 16px #f5f6fa, inset 1px 1px 16px #e9eaf2;
  border-radius: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: all 0.2s;
  :hover {
    opacity: 0.8;
  }
`;

export {
  Logo,
  FormContainer,
  Login,
  TextBig,
  SubmitButton,
  SignUpButton,
  SocialButton,
  Error,
};
