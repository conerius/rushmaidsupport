/* eslint-disable react/no-unescaped-entities */
import React, { useState, useEffect, useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  BookContent,
  BookTitle,
  Tabs,
  Tab,
  Appointment,
  DateTab,
  LocationTab,
  FlexBetween,
  Flex,
  AppointmentRestart,
  DisinfectionAppointment,
} from './AllApointmentsStyled';

import repeat from '../../assets/icons/repeat.svg';
import corona from '../../assets/icons/coronavirus.svg';
import { Link } from 'react-router-dom';

import { getAppointments } from '../../actions/appointmentsActions';
import { getCategories } from '../../actions/categoriesActions';

import moment from 'moment';

import NProgress from 'nprogress';
import { momentDate } from '../../helperFunction';

const Appointments = ({ appointments }) => {
  return (
    <>
      {appointments &&
        appointments.map(
          (appointment) =>
            appointment.status === 'completed' && (
              <Link
                to={`/CleaningDetails/${appointment.selfUID}`}
                key={appointment.selfUID}
              >
                <Appointment>
                  <FlexBetween style={{ borderBottom: '1px solid #DEDEDE' }}>
                    <DateTab>
                      <div>
                        <p>date</p>

                        <h3>{momentDate(appointment?.date)}</h3>
                      </div>
                    </DateTab>
                  </FlexBetween>
                  <Flex>
                    <LocationTab>
                      <div>
                        <p>CLEANING ADDRESS</p>
                        <h3>{appointment.address}</h3>
                      </div>
                    </LocationTab>
                  </Flex>

                  <AppointmentRestart>
                    <img src={repeat} alt='repeat' />
                  </AppointmentRestart>
                </Appointment>
              </Link>
            )
        )}
    </>
  );
};

const AppointmentsUpcoming = ({ appointments }) => {
  return (
    <div>
      {appointments &&
        appointments.map(
          (appointment) =>
            appointment.status !== 'completed' &&
            !appointment.feets && (
              <Link
                to={`/CleaningDetails/${appointment.selfUID}`}
                key={appointment.selfUID}
              >
                <Appointment>
                  <FlexBetween style={{ borderBottom: '1px solid #DEDEDE' }}>
                    <DateTab>
                      <div>
                        <p>date</p>

                        <h3>{momentDate(appointment?.date)}</h3>
                      </div>
                    </DateTab>
                  </FlexBetween>
                  <Flex>
                    <LocationTab>
                      <div>
                        <p>CLEANING ADDRESS</p>
                        <h3>{appointment.address}</h3>
                      </div>
                    </LocationTab>
                  </Flex>
                </Appointment>
              </Link>
            )
        )}
    </div>
  );
};

const AppointmentDisinfection = ({ appointments }) => {
  return (
    <div>
      {appointments &&
        appointments.map(
          (appointment) =>
            appointment.feets && (
              <DisinfectionAppointment key={appointment.selfUID}>
                <FlexBetween style={{ borderBottom: '1px solid #DEDEDE' }}>
                  <DateTab>
                    <div>
                      <p>date</p>
                      <h3>{momentDate(appointment?.date)}</h3>
                    </div>
                  </DateTab>
                  <img style={{ marginRight: 30 }} src={corona} alt='corona' />
                </FlexBetween>
                <Flex>
                  <LocationTab>
                    <div>
                      <p>CLEANING ADDRESS</p>
                      <h3>{appointment.address}</h3>
                    </div>
                  </LocationTab>
                </Flex>
              </DisinfectionAppointment>
            )
        )}
    </div>
  );
};

const AllApointments = () => {
  const [selected, setSelected] = useState(2);

  const dispatch = useDispatch();

  const { appointments, loading } = useSelector((state) => state.appointments);

  // const { categories } = useSelector((state) => state.categories);

  useEffect(() => {
    dispatch(getAppointments());
  }, []);

  useEffect(() => {
    loading ? NProgress.start() : NProgress.done();
  });

  return (
    <>
      <BookContent>
        <BookTitle>All Appointments</BookTitle>
        <Tabs>
          <Tab
            selected={selected === 2 ? true : false}
            onClick={() => setSelected(2)}
          >
            <p>UPCOMING</p>
          </Tab>
          <Tab
            selected={selected === 1 ? true : false}
            onClick={() => setSelected(1)}
          >
            <p>PAST</p>
          </Tab>
        </Tabs>
        {selected === 1 ? (
          <Appointments appointments={appointments} />
        ) : (
          <div>
            <AppointmentsUpcoming appointments={appointments} />
            <AppointmentDisinfection appointments={appointments} />
          </div>
        )}
      </BookContent>
    </>
  );
};

export default AllApointments;
