/* eslint-disable react/no-unescaped-entities */
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  getCategories,
  checkCategory,
  getCategoriesTotal,
} from '../../actions/categoriesActions';

import {
  BookContent,
  BookTitle,
  NextButton,
  BookTab,
  Flex,
  FlexBetween,
  CollapseHidden,
  CollapseArrow,
  Price,
} from './ExtraServicesStyled';
import arrow from '../../assets/icons/arrow.svg';
import check from '../../assets/icons/check.svg';
import { Link } from 'react-router-dom';
import Checkbox from 'react-custom-checkbox';
import { Collapse } from 'reactstrap';

import { AnimationMulti, AnimationGroup } from '../../components/Animation';

const TabCollapse = ({ tab }) => {
  const [collapse, setCollapse] = useState(false);
  const [status, setStatus] = useState(false);
  const [checked, setChecked] = useState(false);
  const onEntering = () => setStatus('Opening...');
  const onEntered = () => setStatus(true);
  const onExiting = () => setStatus(false);
  const onExited = () => setStatus(false);
  const toggle = () => setCollapse(!collapse);
  const toggleChecked = () => setChecked(!checked);

  const { selfID, image_url, name, description, price } = tab;

  const dispatch = useDispatch();

  return (
    <BookTab collapsed={status}>
      <FlexBetween>
        <Flex onClick={toggle}>
          <img
            src={image_url}
            alt='Area'
            style={{ width: selfID === 'autoID3' ? '20px' : '35px' }}
          />
          <p style={{ marginLeft: 20 }}>{name}</p>
          <CollapseArrow
            collapsed={status}
            src={arrow}
            alt='arrow'
            className='ml-3'
            style={{ cursor: 'pointer' }}
          />
        </Flex>
        <Flex>
          <Checkbox
            checked={checked}
            borderWidth={0}
            borderRadius={20}
            style={{ overflow: 'hidden', cursor: 'pointer' }}
            size={32}
            labelStyle={{
              boxShadow:
                ' inset -2px -2px 6px #FFFFFF, inset 2px 2px 3px #BDC1D1',
              marginBottom: '0px',
            }}
            onChange={() => {
              toggleChecked();
              dispatch(checkCategory(selfID));
            }}
            // className='checkShadow'
            className={checked ? 'checkShadow2' : 'checkShadow'}
            icon={
              <div
                style={{
                  display: 'flex',
                  flex: 1,
                  backgroundColor:
                    'linear-gradient(129.67deg, rgba(122, 127, 133, 0.4) 14.52%, rgba(255, 255, 255, 0.4) 100%), #EBECF0',
                  alignSelf: 'stretch',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <img src={check} alt='Area' />
              </div>
            }
          />
        </Flex>
      </FlexBetween>
      <Collapse
        isOpen={collapse}
        onEntering={onEntering}
        onEntered={onEntered}
        onExiting={onExiting}
        onExited={onExited}
      >
        <CollapseHidden>
          <p>{description}</p>
          <Price>${price}</Price>
        </CollapseHidden>
      </Collapse>
    </BookTab>
  );
};

const ExtraServices = ({ match }) => {
  const dispatch = useDispatch();

  const { loading, categories, error } = useSelector(
    (state) => state.categories
  );

  useEffect(() => {
    dispatch(getCategories(match.params.id));
  }, [dispatch, match]);

  return (
    <>
      <BookContent>
        <BookTitle>Do you need some extra services?</BookTitle>
        <AnimationGroup>
          {categories &&
            categories.map((tab) => (
              <AnimationMulti id={tab.selfID} key={tab.selfID}>
                <TabCollapse id={tab.selfID} key={tab.selfID} tab={tab} />
              </AnimationMulti>
            ))}
        </AnimationGroup>
        <Link
          to='/AdditionalQuestions'
          onClick={() => dispatch(getCategoriesTotal(categories))}
        >
          <NextButton>next</NextButton>
        </Link>
      </BookContent>
    </>
  );
};

export default ExtraServices;
