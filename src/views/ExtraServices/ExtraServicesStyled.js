import styled from 'styled-components';

const BookNow = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  padding: 100px 0;
`;
const BookContent = styled.div`
  max-width: 800px;
  width: 100%;
`;
const BookTitle = styled.div`
  font-size: 20px;
  margin-bottom: 30px;
`;

const BookTab = styled.div`
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    linear-gradient(133.76deg, #e6e7ed -18.17%, #f7f8fa 140.27%);
  background-blend-mode: soft-light, normal;
  // box-shadow: -2px -3px 8px #FAFAFC, 1px 1px 5px #B1B5C6, inset 1px 1px 3px #EBECF0;
  box-shadow: ${(props) =>
    props.collapsed
      ? '1px 1px 7px #EBECF0, inset -5px -4px 10px #FAFBFC, inset 3px 3px 8px #BDC1D1;'
      : '-2px -3px 8px #FAFAFC, 1px 1px 5px #B1B5C6, inset 1px 1px 3px #EBECF0;'};

  border-radius: 8px;
  // display: flex;
  // align-items:center;
  // justify-content: space-between;
  // height: 66px;
  padding: 20px;
  margin-bottom: 15px;
  p {
    font-weight: bold;
    font-size: 13px;
    // margin-left: 20px;
  }
`;

const BookTabButton = styled.button`
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    #ffffff;
  background-blend-mode: soft-light, normal;
  box-shadow: -2px -3px 8px #fafafc, 1px 1px 5px rgba(177, 181, 198, 0.339052),
    inset 1px 1px 3px #ebecf0;
  border-radius: 50%;
  width: 35px;
  height: 35px;
  display: flex;
  align-items: center;
  justify-content: center;
  outline: none;
  border: none;
`;

const BookTabNum = styled.h3`
  color: #b09aff;
  font-size: 24px !important;
  font-weight: normal;
  margin: 0 20px;
`;
const Flex = styled.div`
  display: flex;
  align-items: center;
`;
const FlexBetween = styled.div`
  display: flex;
  justify-content: space-between;
`;
const TimeDateTab = styled.div`
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    linear-gradient(133.76deg, #e6e7ed -18.17%, #f7f8fa 140.27%);
  background-blend-mode: soft-light, normal;
  box-shadow: -2px -3px 8px #fafafc, 1px 1px 5px #b1b5c6,
    inset 1px 1px 3px #ebecf0;
  border-radius: 8px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 66px;
  padding: 15px;
  margin-bottom: 15px;
  width: calc(50% - 5px);
  p {
    font-size: 14px;
    text-transform: uppercase;
    color: #9a7df8;
  }
  h3 {
    font-weight: 600;
    font-size: 16px;
    color: #000;
  }
`;
const NextButton = styled.button`
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    linear-gradient(135deg, #b4b2f1 0%, #7838ff 100%);
  background-blend-mode: soft-light, normal;
  box-shadow: -2px -3px 8px #fafafc, 1px 1px 5px #b1b5c6,
    inset 1px 1px 3px #ebecf0;
  text-transform: uppercase;
  border-radius: 8px;
  width: 100%;
  height: 50px;
  padding: 0 19px;
  margin: 0 0 20px 0;
  outline: none;
  font-size: 16px;
  border: none;
  color: #ffffff;
  font-weight: bold;
  transition: all 0.2s;
  :hover {
    opacity: 0.8;
  }
`;
const CollapseHidden = styled.div`
  margin-top: 30px;
  p {
    font-size: 14px;
    color: #757e95;
    margin-bottom: 20px !important;
    font-weight: 400;
  }
`;

const CollapseArrow = styled.img`
  transition: all 0.2s ease;
  transform: ${(props) =>
    props.collapsed ? 'rotate(180deg)' : 'rotate(0deg)'};
  user-select: none;
  -moz-user-select: none;
  -khtml-user-select: none;
  -webkit-user-select: none;
  -o-user-select: none;
`;

const Price = styled.h5`
  font-weight: 600;
  font-size: 18px;
  line-height: 20px;
  color: #000000;
  text-align: right;
`;

export {
  Price,
  BookNow,
  BookContent,
  BookTitle,
  BookTab,
  BookTabButton,
  BookTabNum,
  Flex,
  TimeDateTab,
  FlexBetween,
  NextButton,
  CollapseHidden,
  CollapseArrow,
};
