/* eslint-disable react/no-unescaped-entities */
import React, { useState, useEffect } from 'react';
import NProgress from 'nprogress';

import { Link } from 'react-router-dom';
import facebook from '../../assets/images/fb.svg';
import logo1 from '../../assets/images/logo2text.svg';
import apple from '../../assets/images/apple.svg';
import {
  Login,
  Logo,
  FormContainer,
  TextBig,
  SubmitButton,
  SignUpButton,
  SocialButton,
  Error,
} from './loginStyled';
import InputField from '../../components/InputField';

import Animation from '../../components/Animation';

import { useDispatch, useSelector } from 'react-redux';
import { login, appleLogin, facebookLogin } from '../../actions/authActions';

const SignInView = ({ history }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const dispatch = useDispatch();

  const { loading, error } = useSelector((state) => state.userInfo);

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(login(email, password, history));
    setEmail('');
    setPassword('');
  };

  useEffect(() => {
    loading ? NProgress.start() : NProgress.done();
  }, [loading]);

  return (
    <Animation>
      <Login>
        <Logo>
          <img src={logo1} alt='logo' />
        </Logo>
        <FormContainer>
          <form onSubmit={handleSubmit}>
            <TextBig className='mb-4'>
              Don’t stress less Rushmaid clean that mess
            </TextBig>
            <Error>
              <p>
                {error && error}
                {/* {authError !== null
                  ? authError
                  : '' + authErrorFB + authErrorAPPLE} */}
              </p>
            </Error>
            <InputField
              id={'email'}
              type={'email'}
              placeholder={'Email'}
              name={'email'}
              value={email}
              onChange={setEmail}
            />
            <InputField
              id={'password'}
              type={'password'}
              placeholder={'Password'}
              name={'password'}
              value={password}
              onChange={setPassword}
              label={'Password'}
            />
            {/* <Link to='/ZipCode'> */}
            <SubmitButton type='submit'>sign in</SubmitButton>
            {/* </Link> */}
            {/* <div
              style={{
                width: '100%',
                display: 'flex',
                justifyContent: 'space-between',
              }}
            >
              <div
                onClick={() => dispatch(appleLogin())}
                style={{ width: '50px', cursor: 'pointer' }}
              >
                <SocialButton>
                  <img src={apple} alt='apple' />
                </SocialButton>
              </div>
              <div
                onClick={() => dispatch(facebookLogin())}
                style={{ width: '50px', cursor: 'pointer' }}
              >
                <SocialButton>
                  <img src={facebook} alt='apple' />
                </SocialButton>
              </div>
              <Link to='/register' style={{ width: '191px' }}>
                <SignUpButton>sign up</SignUpButton>
              </Link>
            </div> */}
          </form>
        </FormContainer>
      </Login>
    </Animation>
  );
};

export default SignInView;
