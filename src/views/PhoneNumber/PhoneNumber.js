/* eslint-disable react/no-unescaped-entities */
import React, { useState, useRef } from 'react';
import { useHistory } from 'react-router';
import { useSelector } from 'react-redux';
import {
  BookNow,
  NextButton,
  ProfilePicContainer,
  Inner,
  Icon,
  SubTitle,
  Logo,
  FormContainer,
  Stage,
  Error,
} from './PhoneNumberStyled';
import phone from '../../assets/icons/phoneCircle.svg';
import stage from '../../assets/icons/step2.svg';
import stage3 from '../../assets/icons/step3.svg';

import PhoneInput from 'react-phone-number-input';
import 'react-phone-number-input/style.css';

import logo1 from '../../assets/images/logo2text.svg';
import styled from 'styled-components';

import ReactCodeInput from 'react-code-input';

import { auth, authUninvoked, getUser } from '../../config/firebase';

import Animation from '../../components/Animation';

const PhoneNumber = (props) => {
  const [phoneNumber, setPhoneNumber] = useState(null);
  const [code, setCode] = useState('');
  const [confirmationResult, setConfirmationResult] = useState(null);
  const [phoneSignInError, setPhoneSignInError] = useState(null);
  const [userCredentials, setUserCredentials] = useState(null);
  const [verified, setVerified] = useState(null);
  // const [verified, setVerified] = useState(null);

  const captchaRef = useRef();

  const history = useHistory();

  const userInfo = useSelector((state) => state.userInfo);

  const handleSendCode = async () => {
    setPhoneSignInError(null);
    setCode('');
    const appVerifier = new authUninvoked.RecaptchaVerifier('sign-in-button', {
      size: 'invisible',
      callback: function (response) {
        console.log('It works!');
      },
    });
    try {
      const confirmationResult_ = await auth.currentUser.linkWithPhoneNumber(
        phoneNumber,
        appVerifier
      );

      setPhoneNumber(phoneNumber);
      setConfirmationResult(confirmationResult_);
      appVerifier.clear();
      // confirmationResult
      // props.updateVerification(confirmationResult, phoneNumber);
    } catch (err) {
      err && appVerifier.clear();
      setPhoneSignInError(err.message);
      console.log(phoneSignInError);
    }
  };

  const handleVerify = async () => {
    try {
      const userCredentials_ = await confirmationResult.confirm(code);
      setVerified(true);

      const phoneNumber = props.verification.phoneNumber;
      const zipCode = props.user.zipCode;

      getUser(userInfo.user.uid)
        .update({
          phoneNumber,
          phoneVerified: true,
        })
        .then(() => {
          history.push('/');
        })
        .catch((e) => {
          console.log(e.message);
        });

      setUserCredentials(userCredentials_);

      setPhoneSignInError('');
    } catch (err) {
      setCode('');
      setPhoneSignInError(err.message);
    }
  };

  // const goBack = () => {
  //   props.setCurrentStep(props.currentStep - 1);
  // };

  return (
    <Animation>
      <BookNow>
        <Logo>
          <img src={logo1} alt='logo' />
        </Logo>
        <FormContainer>
          <ProfilePicContainer>
            <Inner>
              <Icon src={phone} alt='redeem' />
            </Inner>
          </ProfilePicContainer>
          <SubTitle>
            Please enter your phone number so we can send you verification code
          </SubTitle>
          <Error>
            <p>{phoneSignInError}</p>
          </Error>

          {confirmationResult === null ? (
            <InputContainer>
              <PhoneInput
                country='US'
                defaultCountry='US'
                value={phoneNumber}
                onChange={setPhoneNumber}
                placeholder='(305) 123 4567'
              />
            </InputContainer>
          ) : null}

          <div ref={captchaRef} />

          {confirmationResult && (
            <>
              <ReactCodeInput fields={6} value={code} onChange={setCode} />
            </>
          )}

          {confirmationResult === null ? (
            <NextButton
              id='sign-in-button'
              className='mt-2'
              onClick={() => handleSendCode()}
            >
              Send Code
            </NextButton>
          ) : null}

          {confirmationResult && (
            <>
              {' '}
              <NextButton className='mt-2' onClick={() => handleVerify()}>
                Submit
              </NextButton>
            </>
          )}

          {/* <BackButton className='mt-2' onClick={(e) => goBack(e)}>
          back
        </BackButton> */}

          {confirmationResult === null ? (
            <Stage src={stage} alt='step 2' />
          ) : null}
          {confirmationResult && (
            <>
              <Stage src={stage3} alt='step 3' />
            </>
          )}
        </FormContainer>
      </BookNow>
    </Animation>
  );
};

export default PhoneNumber;

const Input = styled(PhoneInput)`
  width: 100%;
  border: none;
  background: none;
  outline: none;
  font-size: 14px;
`;

const InputContainer = styled.div`
  background: linear-gradient(
      147.42deg,
      rgba(64, 72, 93, 0.4) 6.32%,
      rgba(96, 106, 130, 0.4) 92.25%
    ),
    linear-gradient(128deg, #e6e7ed -79.65%, #f7f8fa 151.25%);
  background-blend-mode: soft-light, normal;
  box-shadow: -7px -7px 16px #fafbfc, 4px 3px 19px #bdc1d1,
    inset -1px -1px 16px #f5f6fa, inset 1px 1px 16px #e9eaf2;
  border-radius: 8px;
  width: 100%;
  height: 50px;
  display: flex;
  align-items: center;
  padding: 0 19px;
  margin: 0 0 20px 0;
  > * {
    width: 100%;
  }
  input {
    width: 100%;
    border: none;
    background: none;
    outline: none;
    font-size: 16px;
  }
`;
