/* eslint-disable react/no-unescaped-entities */
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';

import ErrorMessage from '../../components/ErrorMessage';

import { getCustomer } from '../../actions/stripeActions';

import {
  BookNow,
  BookContent,
  NextButton,
  ProfilePicContainer,
  Inner,
  Icon,
  SubTitle,
} from './RedeemGiftCodeStyled';
import reedee from '../../assets/icons/redee.svg';

import AdditionalInputField from '../../components/AdditionalInputField';

import NProgress from 'nprogress';

const API_KEY = process.env.REACT_APP_API_URL;

const Profile = ({ history }) => {
  const [email, setEmail] = useState('');
  const [errorMsg, setErrorMsg] = useState('');
  const [loading, setLoading] = useState(false);

  const dispatch = useDispatch();

  const { user } = useSelector((state) => state.userInfo);
  const { customer } = useSelector((state) => state.stripeInfo);

  const createVoucher = async (email, user, customer) => {
    try {
      setLoading(true);

      const voucher = {
        receiverEmail: email,
        senderUserId: user.uid,
        amount: 200,
        sourceId: customer.default_source,
        customerId: customer.id,
      };

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.token}`,
        },
      };

      const { data } = await axios.post(
        `${API_KEY}/voucher/create`,
        voucher,
        config
      );

      setErrorMsg('');

      alert(`Successfully sent gift to ${email}`);

      setLoading(false);
    } catch (error) {
      setLoading(false);
      setErrorMsg(error.response.data.message);
    }
  };

  const redeemGift = () => {
    if (!customer) {
      history.push('/BillingInfo');
    } else {
      if (!email) {
        setErrorMsg('Please enter receiver email');
      } else {
        setErrorMsg('');
        setEmail('');
        createVoucher(email, user, customer);
      }
    }
  };

  useEffect(() => {
    user.customerID && dispatch(getCustomer(user.customerID));
    loading ? NProgress.start() : NProgress.done();
  }, [loading]);

  return (
    <BookNow>
      <BookContent>
        <ProfilePicContainer>
          <Inner>
            <Icon src={reedee} alt='redeem' />
          </Inner>
        </ProfilePicContainer>
        <SubTitle>Redeem Gift Code</SubTitle>
        <AdditionalInputField
          type={'text'}
          placeholder={'Enter receiver email'}
          value={email || ''}
          onChange={(e) => setEmail(e.target.value)}
        />

        <NextButton className='mt-2' onClick={redeemGift}>
          Submit
        </NextButton>
        <ErrorMessage error={errorMsg} />
      </BookContent>
    </BookNow>
  );
};

export default Profile;
