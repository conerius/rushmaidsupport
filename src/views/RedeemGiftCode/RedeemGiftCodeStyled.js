import styled from 'styled-components';

const BookNow = styled.div`
  max-width: 800px;
  width: 100%;
  display: flex;
  justify-content: center;
`;
const BookContent = styled.div`
  width: 361px;
`;
const BookTitle = styled.div`
  font-size: 20px;
  margin-bottom: 30px;
`;
const SubTitle = styled.div`
  font-weight: bold;
  font-size: 30px;
  line-height: 41px;
  text-align: center;
  margin-bottom: 30px;
`;
const ProfileTab = styled.div`
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    linear-gradient(133.76deg, #e6e7ed -18.17%, #f7f8fa 140.27%);
  background-blend-mode: soft-light, normal;
  // box-shadow: -2px -3px 8px #FAFAFC, 1px 1px 5px #B1B5C6, inset 1px 1px 3px #EBECF0;
  box-shadow: ${(props) =>
    props.collapsed
      ? '1px 1px 7px #EBECF0, inset -5px -4px 10px #FAFBFC, inset 3px 3px 8px #BDC1D1;'
      : '-2px -3px 8px #FAFAFC, 1px 1px 5px #B1B5C6, inset 1px 1px 3px #EBECF0;'};

  border-radius: 8px;
  // display: flex;
  // align-items:center;
  // justify-content: space-between;
  // height: 66px;
  padding: 20px;
  margin-bottom: 50px;
  p {
    font-size: 14px;
    text-transform: uppercase;
    color: #9a7df8;
  }
  h3 {
    font-weight: 600;
    font-size: 16px;
    color: #000;
  }
`;
const BookTabButton = styled.button`
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    #ffffff;
  background-blend-mode: soft-light, normal;
  box-shadow: -2px -3px 8px #fafafc, 1px 1px 5px rgba(177, 181, 198, 0.339052),
    inset 1px 1px 3px #ebecf0;
  border-radius: 50%;
  width: 35px;
  height: 35px;
  display: flex;
  align-items: center;
  justify-content: center;
  outline: none;
  border: none;
`;
const BookTabNum = styled.h3`
  color: #b09aff;
  font-size: 24px !important;
  font-weight: normal;
  margin: 0 20px;
`;
const Flex = styled.div`
  display: flex;
  align-items: center;
`;
const FlexBetween = styled.div`
  display: flex;
  justify-content: space-between;
`;
const TimeDateTab = styled.div`
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    linear-gradient(133.76deg, #e6e7ed -18.17%, #f7f8fa 140.27%);
  background-blend-mode: soft-light, normal;
  box-shadow: -2px -3px 8px #fafafc, 1px 1px 5px #b1b5c6,
    inset 1px 1px 3px #ebecf0;
  border-radius: 8px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 66px;
  padding: 15px;
  margin-bottom: 15px;
  width: calc(50% - 5px);
  p {
    font-size: 14px;
    text-transform: uppercase;
    color: #9a7df8;
  }
  h3 {
    font-weight: 600;
    font-size: 16px;
    color: #000;
  }
`;
const NextButton = styled.button`
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    linear-gradient(135deg, #b4b2f1 0%, #7838ff 100%);
  background-blend-mode: soft-light, normal;
  box-shadow: -2px -3px 8px #fafafc, 1px 1px 5px #b1b5c6,
    inset 1px 1px 3px #ebecf0;
  text-transform: uppercase;
  border-radius: 8px;
  width: 100%;
  height: 50px;
  padding: 0 19px;
  margin: 0 0 20px 0;
  outline: none;
  font-size: 16px;
  border: none;
  color: #ffffff;
  font-weight: bold;
  transition: all 0.2s;
  :hover {
    opacity: 0.8;
  }
`;
const CollapseHidden = styled.div`
  margin: 30px 0;
  p {
    font-size: 14px;
    color: #757e95;
    margin-bottom: 20px !important;
    font-weight: 400;
  }
`;
const CollapseArrow = styled.img`
  transition: all 0.2s ease;
  transform: ${(props) =>
    props.collapsed ? 'rotate(180deg)' : 'rotate(0deg)'};
  user-select: none;
  -moz-user-select: none;
  -khtml-user-select: none;
  -webkit-user-select: none;
  -o-user-select: none;
`;
const ProfilePicContainer = styled.div`
  background: linear-gradient(
      147.42deg,
      rgba(64, 72, 93, 0.4) 6.32%,
      rgba(96, 106, 130, 0.4) 92.25%
    ),
    linear-gradient(128deg, #e6e7ed -79.65%, #f7f8fa 151.25%);
  background-blend-mode: soft-light, normal;
  box-shadow: -7px -7px 16px #fafbfc, 4px 3px 19px #bdc1d1,
    inset -1px -1px 16px #f5f6fa, inset 1px 1px 16px #e9eaf2;
  border-radius: 50%;
  width: 166px;
  height: 166px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 50px auto 30px auto;
  position: relative;
`;
const Inner = styled.div`
  width: 101px;
  height: 101px;
  background: #ebecf0;
  box-shadow: 1px 1px 7px #ebecf0, inset -5px -4px 10px #fafbfc,
    inset 3px 3px 8px #bdc1d1;
  border-radius: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
`;
const Icon = styled.img`
  filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
`;

export {
  BookNow,
  BookContent,
  BookTitle,
  ProfileTab,
  BookTabButton,
  BookTabNum,
  Flex,
  TimeDateTab,
  FlexBetween,
  NextButton,
  CollapseHidden,
  CollapseArrow,
  ProfilePicContainer,
  Icon,
  Inner,
  SubTitle,
};
