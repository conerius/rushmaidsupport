/* eslint-disable react/no-unescaped-entities */
import React, { useState } from 'react';
import { useHistory } from 'react-router';
import { useSelector } from 'react-redux';

import {
  BookNow,
  NextButton,
  ProfilePicContainer,
  Inner,
  Icon,
  SubTitle,
  Logo,
  FormContainer,
  Stage,
  Error,
} from './ZipCodeStyled';

import location from '../../assets/icons/locationCircle.svg';
import stage from '../../assets/icons/step1.svg';
import AdditionalInputField from '../../components/InputField';
import logo1 from '../../assets/images/logo2text.svg';

import { getUser } from '../../config/firebase';

import Animation from '../../components/Animation';

const ZipCode = () => {
  const [zipCode, setZipCode] = useState('');
  const [error, setError] = useState('');

  const history = useHistory();

  const userInfo = useSelector((state) => state.userInfo);

  const handleClick = async (event) => {
    event.preventDefault();
    if (zipCode.length === 0) {
      setError('Please fill in the Zip Code field');
    } else {
      setError('');

      await getUser(userInfo.user.uid)
        .update({
          zipCode,
        })
        .then(() => {
          history.push('/phoneNumber');
        })
        .catch((e) => {
          console.log(e.message);
        });
    }
  };
  return (
    <Animation>
      <BookNow>
        <Logo>
          <img src={logo1} alt='logo' />
        </Logo>
        <FormContainer>
          <ProfilePicContainer>
            <Inner>
              <Icon src={location} alt='redeem' />
            </Inner>
          </ProfilePicContainer>
          <SubTitle>Please enter your Zip Code</SubTitle>
          <Error>
            <p>{error}</p>
          </Error>

          <AdditionalInputField
            type={'text'}
            placeholder={'Zip Code'}
            name={'ZipCode'}
            value={zipCode}
            onChange={setZipCode}
          />
          <NextButton className='mt-2' onClick={(e) => handleClick(e)}>
            Submit
          </NextButton>
          <Stage src={stage} alt='step 1' />
        </FormContainer>
      </BookNow>
    </Animation>
  );
};

export default ZipCode;
