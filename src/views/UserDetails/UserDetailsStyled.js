import styled from 'styled-components';

const User = styled.div`
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    linear-gradient(133.76deg, #e6e7ed -18.17%, #f7f8fa 140.27%);
  background-blend-mode: soft-light, normal;
  box-shadow: -2px -3px 8px #fafafc, 1px 1px 5px #b1b5c6,
    inset 1px 1px 3px #ebecf0;
  border-radius: 8px;
  margin-bottom: 15px;
  position: relative;
  p {
    font-size: 14px;
    text-transform: uppercase;
    color: #9a7df8;
  }
  h3 {
    font-weight: 600;
    font-size: 16px;
    color: #000;
  }
`;

const FlexBetween = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`;

const FlexBetweenResponsive = styled.div`
  display: flex;
  justify-content: space-between;
  @media (max-width: 1100px) {
    flex-direction: column;
  }
`;

const BetweenIn = styled.div`
  width: calc(50% - 25px);
  @media (max-width: 1100px) {
    width: 100%;
  }
`;

const TimeDateTab = styled.div`
  display: flex;
  align-items: center;
  // justify-content: space-between;
  min-height: 66px;
  padding: 15px;
  p {
    font-size: 14px;
    text-transform: uppercase;
    color: #9a7df8;
  }
  h3 {
    font-weight: 600;
    font-size: 16px;
    color: #000;
  }
  img {
    margin-right: 20px;
  }
`;
const DateTab = styled(TimeDateTab)`
  width: 60%;
`;
const TimeTab = styled(TimeDateTab)`
  width: 40%;
`;
const LocationTab = styled(TimeDateTab)`
  width: 100%;
`;

const Flex = styled.div`
  display: flex;
  align-items: center;
`;

const ProfilePicUser = styled.div`
  background: linear-gradient(
      147.42deg,
      rgba(64, 72, 93, 0.4) 6.32%,
      rgba(96, 106, 130, 0.4) 92.25%
    ),
    linear-gradient(128deg, #e6e7ed -79.65%, #f7f8fa 151.25%);
  background-blend-mode: soft-light, normal;
  box-shadow: -7px -7px 16px #fafbfc, 4px 3px 19px #bdc1d1,
    inset -1px -1px 16px #f5f6fa, inset 1px 1px 16px #e9eaf2;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 8px;
  /* margin: 0 auto 30px auto; */
  img {
    object-fit: contain;
    width: 100%;
    border-radius: 8px;
  }
`;

const BookContent = styled.div`
  max-width: 800px;
  width: 100%;
`;
const BookTitle = styled.div`
  font-size: 20px;
  margin-bottom: 30px;
`;

export {
  BookContent,
  BookTitle,
  User,
  DateTab,
  TimeTab,
  LocationTab,
  FlexBetween,
  Flex,
  FlexBetweenResponsive,
  BetweenIn,
  ProfilePicUser,
};
