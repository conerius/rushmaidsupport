/* eslint-disable react/no-unescaped-entities */
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import NProgress from 'nprogress';

import {
  BookContent,
  BookTitle,
  User,
  DateTab,
  TimeTab,
  LocationTab,
  FlexBetween,
  Flex,
  BetweenIn,
  FlexBetweenResponsive,
  ProfilePicUser,
} from './UserDetailsStyled';

import { getMaids } from '../../actions/maidsActions';
import { getCustomers } from '../../actions/customersActions';

import logo1 from '../../assets/images/Logo1.svg';

const Maid = ({ user, loading }) => {
  return (
    <>
      {!loading && (
        <User>
          <FlexBetween style={{ borderBottom: '1px solid #DEDEDE' }}>
            <DateTab>
              <div>
                <p>First Name</p>
                <h3>{user?.firstName}</h3>
              </div>
            </DateTab>
            <TimeTab>
              <div>
                <p>Last Name</p>
                <h3>{user?.lastName}</h3>
              </div>
            </TimeTab>
          </FlexBetween>
          <Flex style={{ borderBottom: '1px solid #DEDEDE' }}>
            <LocationTab>
              <div>
                <p>Email</p>
                <h3>{user?.email}</h3>
              </div>
            </LocationTab>
          </Flex>
          <Flex style={{ borderBottom: '1px solid #DEDEDE' }}>
            <LocationTab>
              <div>
                <p>phone number</p>
                <h3>{user?.phoneNumber}</h3>
              </div>
            </LocationTab>
          </Flex>
          <Flex style={{ borderBottom: '1px solid #DEDEDE' }}>
            <LocationTab>
              <div>
                <p>status</p>
                <h3>{user?.status}</h3>
              </div>
            </LocationTab>
          </Flex>
          <Flex style={{ borderBottom: '1px solid #DEDEDE' }}>
            <LocationTab>
              <div>
                <p>ZIP code</p>
                <h3>{user?.zipCode}</h3>
              </div>
            </LocationTab>
          </Flex>
        </User>
      )}
    </>
  );
};

const Customer = ({ user, loading }) => {
  // const { user_id } = user;
  // console.log(user_id);
  return (
    <>
      {!loading && (
        <User>
          <FlexBetween style={{ borderBottom: '1px solid #DEDEDE' }}>
            <DateTab>
              <div>
                <p>First Name</p>
                <h3>{user?.first_name}</h3>
              </div>
            </DateTab>
            <TimeTab>
              <div>
                <p>Last Name</p>
                <h3>{user?.last_name}</h3>
              </div>
            </TimeTab>
          </FlexBetween>
          <Flex style={{ borderBottom: '1px solid #DEDEDE' }}>
            <LocationTab>
              <div>
                <p>email</p>
                <h3>{user?.email}</h3>
              </div>
            </LocationTab>
          </Flex>
          <Flex style={{ borderBottom: '1px solid #DEDEDE' }}>
            <LocationTab>
              <div>
                <p>phone number</p>
                <h3>{user?.phone_number}</h3>
              </div>
            </LocationTab>
          </Flex>
          <Flex style={{ borderBottom: '1px solid #DEDEDE' }}>
            <LocationTab>
              <div>
                <p>Appointment Number</p>
                <h3>{user?.appointments.length}</h3>
              </div>
            </LocationTab>
          </Flex>
          <Flex style={{ borderBottom: '1px solid #DEDEDE' }}>
            <LocationTab>
              <div>
                <p>zip codes</p>
                <h3>
                  {user?.zip_codes.map((zip, i) => {
                    return <div key={i}>{zip}</div>;
                  })}
                </h3>
              </div>
            </LocationTab>
          </Flex>
          {/* {user?.favorite_maid && (
            <Flex style={{ borderBottom: '1px solid #DEDEDE' }}>
              <LocationTab>
                <div>
                  <p>favorite maid</p>
                  <h3>{user?.favorite_maid}</h3>
                </div>
              </LocationTab>
            </Flex>
          )} */}
        </User>
      )}
    </>
  );
};

const UserDetails = ({ match }) => {
  const dispatch = useDispatch();
  const { maids, loadingMaids } = useSelector((state) => state.maids);
  const { customers, loadingCustomers } = useSelector(
    (state) => state.customers
  );

  // const user = maids?.find((app) => app.uid === match.params.id);

  const user =
    match?.params?.role === 'maid'
      ? maids?.find((app) => app.uid === match.params.id)
      : customers?.find((app) => app.user_id === match.params.id);

  // useEffect(() => {
  //   // console.log(match?.params?.role);
  //   // console.log(match?.params?.id);
  //   if (match?.params?.role === 'maid') {
  //     dispatch(getMaids());
  //   } else {
  //     dispatch(getCustomers());
  //   }
  // }, [dispatch]);

  // useEffect(() => {
  //   if (match?.params?.role === 'maid') {
  //     loadingMaids ? NProgress.start() : NProgress.done();
  //   } else {
  //     loadingCustomers ? NProgress.start() : NProgress.done();
  //   }
  // });

  const UserComponent = () => {
    if (match?.params?.role === 'maid') {
      return <Maid user={user} loading={loadingMaids} />;
    } else {
      return <Customer user={user} loading={loadingCustomers} />;
    }
  };

  console.log(user);

  return (
    <BookContent>
      <BookTitle>User Details</BookTitle>
      {/* {loading && loadingMaids && ( */}
      <FlexBetweenResponsive>
        <BetweenIn>
          <ProfilePicUser>
            {match?.params?.role === 'maid' ? (
              <img src={user?.imageURL ?? logo1} alt='user' />
            ) : (
              <img src={user?.image_url ?? logo1} alt='user' />
            )}
          </ProfilePicUser>
        </BetweenIn>
        <BetweenIn>
          <UserComponent />
        </BetweenIn>
      </FlexBetweenResponsive>
      {/* )} */}
    </BookContent>
  );
};

export default UserDetails;
