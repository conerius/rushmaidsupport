/* eslint-disable react/no-unescaped-entities */
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  BookContent,
  BookTitle,
  Maid,
  Columns,
  MaidMobile,
} from './MaidsStyled';

import { getMaids } from '../../actions/maidsActions';

import NProgress from 'nprogress';
import { Link } from 'react-router-dom';

const Maids = ({ maids }) => {
  return (
    <>
      {maids &&
        maids.map((maid) => (
          <div key={maid?.uid}>
            <Link
              to={`/UserDetails/${maid?.role}/${maid?.uid}`}
              key={maid?.uid}
            >
              <Maid>
                <div style={{ width: '15%' }}>{maid?.firstName}</div>
                <div style={{ width: '17%' }}>{maid?.lastName}</div>
                <div style={{ width: '30%' }}>{maid?.email}</div>
                <div style={{ width: '25%' }}>{maid?.phoneNumber}</div>
                <div style={{ width: '13%' }}>{maid?.status}</div>
              </Maid>
              <MaidMobile>
                <div>
                  <p>First Name</p>
                  {maid?.firstName}
                </div>
                <div>
                  <p>Last Name</p>
                  {maid?.lastName}
                </div>
                <div>
                  <p>Email</p>
                  {maid?.email}
                </div>
                <div>
                  <p>Phone Number</p>
                  {maid?.phoneNumber}
                </div>
                <div>
                  <p>Cleaning Status</p>
                  {maid?.status}
                </div>
              </MaidMobile>
            </Link>
          </div>
        ))}
    </>
  );
};

const AllMaids = () => {
  const dispatch = useDispatch();

  const { maids, loadingMaids } = useSelector((state) => state.maids);

  useEffect(() => {
    dispatch(getMaids());
  }, [dispatch]);

  useEffect(() => {
    loadingMaids ? NProgress.start() : NProgress.done();
  });

  // console.log(maids);

  return (
    <>
      <BookContent>
        <BookTitle>Maids</BookTitle>
        <Columns>
          <div style={{ width: '15%' }}>First Name</div>
          <div style={{ width: '17%' }}>Last Name</div>
          <div style={{ width: '30%' }}>Email</div>
          <div style={{ width: '25%' }}>Phone Number</div>
          <div style={{ width: '13%' }}>Cleaning Status</div>
        </Columns>
        <Maids maids={maids} />
      </BookContent>
    </>
  );
};

export default AllMaids;
