import styled from 'styled-components';

const BookNow = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  padding: 100px 0;
`;
const BookContent = styled.div`
  max-width: 800px;
  width: 100%;
`;
const BookTitle = styled.div`
  font-size: 20px;
  margin-bottom: 30px;
`;

const Subtitle = styled.h2`
  font-weight: bold;
  font-size: 18px;
  color: #9a7df8;
  margin-bottom: 30px !important;
`;
const Text = styled.p`
  font-weight: normal;
  font-size: 14px;
  line-height: 21px;
  color: #252631;
  margin-bottom: 20px !important ;
`;

export { BookNow, BookContent, BookTitle, Subtitle, Text };
