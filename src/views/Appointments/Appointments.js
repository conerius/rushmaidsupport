/* eslint-disable react/no-unescaped-entities */
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  BookContent,
  BookTitle,
  Maid,
  Columns,
  MaidMobile,
  UserImage,
} from './AppointmentsStyled';

import { getMaids } from '../../actions/maidsActions';

import NProgress from 'nprogress';
import { getAppointments } from '../../actions/appointmentsActions';
import { momentDate, momentTime } from '../../helperFunction';
import { getCustomers } from '../../actions/customersActions';

import logo1 from '../../assets/images/Logo1.svg';

import { Link } from 'react-router-dom';

const Appointments = ({ appointments, customers }) => {
  return (
    <>
      {appointments &&
        appointments.map((appointment, i) => {
          const customer = customers?.filter((id) => {
            return id?.user_id === appointment?.userUID;
          });

          // console.log(customer[0].image_url);

          return (
            <div key={i}>
              <Link
                to={`/CleaningDetails/${appointment.selfUID}`}
                key={appointment.selfUID}
              >
                <Maid>
                  <div style={{ width: '15%' }}>{appointment?.bookingId}</div>
                  <div
                    style={{
                      width: '22%',
                      display: 'flex',
                      alignItems: 'center',
                    }}
                  >
                    <UserImage>
                      <img
                        src={
                          customers && customer[0]?.image_url
                            ? customers && customer[0]?.image_url
                            : logo1
                        }
                        alt='user'
                      />
                    </UserImage>{' '}
                    {customers && customer[0]?.first_name}{' '}
                    {customers && customer[0]?.last_name}
                  </div>
                  <div style={{ width: '25%' }}>{appointment?.address}</div>
                  <div style={{ width: '25%' }}>
                    {momentDate(appointment?.date)}{' '}
                    {momentTime(appointment?.date)}
                  </div>
                  <div style={{ width: '13%' }}>{appointment?.status}</div>
                </Maid>
                <MaidMobile>
                  <div>
                    <p>Booking ID</p>
                    {appointment?.bookingId}
                  </div>
                  <div>
                    <p>Customer</p>
                    {appointment?.userUID}
                  </div>
                  <div>
                    <p>Address</p>
                    {appointment?.address}
                  </div>
                  <div>
                    <p>Date & Time</p>
                    {momentDate(appointment?.date)}{' '}
                    {momentTime(appointment?.date)}
                  </div>
                  <div>
                    <p>Status</p>
                    {appointment?.status}
                  </div>
                </MaidMobile>
              </Link>
            </div>
          );
        })}
    </>
  );
};

const AllAppointments = () => {
  const dispatch = useDispatch();

  const { appointments, loading } = useSelector((state) => state.appointments);
  const { customers, loadingCustomers } = useSelector(
    (state) => state.customers
  );

  useEffect(() => {
    dispatch(getAppointments());
    dispatch(getCustomers());
  }, [dispatch]);

  useEffect(() => {
    loading && loadingCustomers ? NProgress.start() : NProgress.done();
  });

  // console.log(maids);

  return (
    <>
      <BookContent>
        <BookTitle>Appointments</BookTitle>
        <Columns>
          <div style={{ width: '15%' }}>Booking ID</div>
          <div style={{ width: '22%' }}>Customer</div>
          <div style={{ width: '25%' }}>Address</div>
          <div style={{ width: '25%' }}>Date & Time</div>
          <div style={{ width: '13%' }}>Status</div>
        </Columns>
        {customers && appointments && (
          <Appointments appointments={appointments} customers={customers} />
        )}
      </BookContent>
    </>
  );
};

export default AllAppointments;
