import styled from 'styled-components';

const BookNow = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  padding: 100px 0;
`;
const BookContent = styled.div`
  max-width: 1500px;
  width: 100%;
`;
const BookTitle = styled.div`
  font-size: 20px;
  margin-bottom: 30px;
`;

const SelectContainer = styled.div`
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    linear-gradient(133.76deg, #e6e7ed -18.17%, #f7f8fa 140.27%);
  background-blend-mode: soft-light, normal;
  box-shadow: -2px -3px 8px #fafafc, 1px 1px 5px #b1b5c6,
    inset 1px 1px 3px #ebecf0;
  border-radius: 8px;
`;

const TimeDateTab = styled.div`
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    linear-gradient(133.76deg, #e6e7ed -18.17%, #f7f8fa 140.27%);
  background-blend-mode: soft-light, normal;
  box-shadow: -2px -3px 8px #fafafc, 1px 1px 5px #b1b5c6,
    inset 1px 1px 3px #ebecf0;
  border-radius: 8px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 55px;
  /* width: calc(50% - 10px); */
  padding: 15px;
  margin-bottom: 20px;
  p {
    font-size: 14px;
    text-transform: uppercase;
    color: #9a7df8;
  }
  h3 {
    font-weight: 600;
    font-size: 16px;
    color: #000;
  }
`;

const Maid = styled.div`
  display: flex;
  align-items: center;
  // justify-content: space-between;
  height: 66px;
  padding: 15px;
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    linear-gradient(133.76deg, #e6e7ed -18.17%, #f7f8fa 140.27%);
  background-blend-mode: soft-light, normal;
  box-shadow: -2px -3px 8px #fafafc, 1px 1px 5px #b1b5c6,
    inset 1px 1px 3px #ebecf0;
  border-radius: 8px;
  margin-bottom: 15px;
  font-weight: 600;
  font-size: 16px;
  line-height: 20px;
  color: #000000;
  word-break: break-all;
  @media (max-width: 1300px) {
    display: none;
  }
`;
const MaidMobile = styled.div`
  display: none;
  /* padding: 15px; */
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    linear-gradient(133.76deg, #e6e7ed -18.17%, #f7f8fa 140.27%);
  background-blend-mode: soft-light, normal;
  box-shadow: -2px -3px 8px #fafafc, 1px 1px 5px #b1b5c6,
    inset 1px 1px 3px #ebecf0;
  border-radius: 8px;
  margin-bottom: 15px;
  font-weight: 600;
  font-size: 16px;
  line-height: 20px;
  color: #000000;
  word-break: break-all;
  @media (max-width: 1300px) {
    display: block;
  }

  div {
    font-weight: 600;
    font-size: 16px;
    line-height: 20px;
    color: #000000;
    padding: 15px;
    border-bottom: 1px solid #d8d8d8;
  }
  p {
    font-weight: normal;
    font-size: 13px;
    line-height: 18px;
    color: #9a7df8;
    margin-bottom: 6px;
  }
`;

const Columns = styled.div`
  font-weight: normal;
  font-size: 13px;
  line-height: 18px;
  color: #9a7df8;
  display: flex;
  align-items: center;
  margin-bottom: 15px;
  height: 33px;
  padding: 15px;
  background: linear-gradient(
      129.67deg,
      rgba(122, 127, 133, 0.4) 14.52%,
      rgba(255, 255, 255, 0.4) 100%
    ),
    linear-gradient(133.76deg, #e6e7ed -18.17%, #f7f8fa 140.27%);
  background-blend-mode: soft-light, normal;
  box-shadow: -2px -3px 8px #fafafc, 1px 1px 5px #b1b5c6,
    inset 1px 1px 3px #ebecf0;
  border-radius: 8px;
  @media (max-width: 1300px) {
    display: none;
  }
`;

const FlexBetween = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  @media (max-width: 850px) {
    display: block;
  }
`;

const Label = styled.div`
  font-size: 14px;
  text-transform: uppercase;
  color: #9a7df8;
`;

const HalfBox = styled.div`
  width: calc(50% - 10px);
  @media (max-width: 850px) {
    width: 100%;
  }
`;

const UserImage = styled.div`
  width: 50px;
  height: 50px;
  display: inline-block;
  margin-right: 10px;
  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
    border-radius: 50%;
  }
`;

export {
  BookNow,
  BookContent,
  BookTitle,
  Maid,
  MaidMobile,
  Columns,
  TimeDateTab,
  FlexBetween,
  SelectContainer,
  Label,
  HalfBox,
  UserImage,
};
