import styled from 'styled-components';

const ChatWrapper = styled.div`
  width: 100%;
  height: calc(100vh - 210px);
  overflow-y: auto;
`;

const Message = styled.div`
  background-blend-mode: soft-light, normal;
  box-shadow: -7px -7px 16px #fafbfc, 4px 3px 19px #bdc1d1,
    inset -1px -1px 16px #f5f6fa, inset 1px 1px 16px #e9eaf2;
  border-radius: 8px;
  padding: 20px;
  background: linear-gradient(
      147.42deg,
      rgba(64, 72, 93, 0.4) 6.32%,
      rgba(96, 106, 130, 0.4) 92.25%
    ),
    linear-gradient(128deg, #e6e7ed -79.65%, #f7f8fa 151.25%);
  max-width: 60%;
  /* width: 100%; */
  margin: 0 20px;

  p {
    font-weight: normal;
    font-size: 16px;
    line-height: 22px;
    color: #000000;
    margin: 0 20px 30px 20px !important;
  }
  h5 {
    font-weight: 300;
    font-size: 13px;
    line-height: 18px;

    color: #000000;
    margin: 0;
  }
`;

const MessageOp = styled(Message)`
  max-width: calc(60% + 86px);
  background: linear-gradient(
      147.42deg,
      rgba(64, 72, 93, 0.4) 6.32%,
      rgba(96, 106, 130, 0.4) 92.25%
    ),
    linear-gradient(128deg, #f7f8fa -26.74%, #e8ddff 151.25%);
`;

const MsgWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  margin-bottom: 30px;
`;

const MsgWrapperOp = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  margin-bottom: 30px;
`;

const MsgWrapperOpInner = styled.div`
  width: 100%;
`;

const ChatProfile = styled.div`
  width: 58px;
  height: 58px;
  flex: none;
  margin-right: 8px;
  img {
    object-fit: cover;
    width: 100%;
    height: 100%;
    border-radius: 50%;
  }
`;

const ChatInput = styled.div`
  padding: 30px;
  border-top: 1px solid #d9d9d9;
  display: flex;
  align-items: center;
  position: absolute;
  bottom: -30;
  width: 100%;
  img {
    flex: none;
  }
`;

export {
  Message,
  MessageOp,
  MsgWrapper,
  MsgWrapperOp,
  MsgWrapperOpInner,
  ChatProfile,
  ChatWrapper,
  ChatInput,
};
