/* eslint-disable react/no-unescaped-entities */
import React, { useEffect, useState, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { getMaids } from '../../actions/maidsActions';

import NProgress from 'nprogress';
import { Link } from 'react-router-dom';
import { BookContent, BookTitle, FlexBetween } from '../Maids/MaidsStyled';

import {
  Message,
  MessageOp,
  MsgWrapper,
  MsgWrapperOp,
  MsgWrapperOpInner,
  ChatProfile,
  ChatWrapper,
  ChatInput,
} from './ChatStyled';
import {
  addAdmin,
  getConversations,
  sendImageChat,
  sendMessageChat,
  setLastMessageRead,
} from '../../actions/chatActions';
import { momentDate, momentTime } from '../../helperFunction';

import userImg from '../../assets/images/profile.jpg';
import imageUpload from '../../assets/icons/imageIcon.png';
import send from '../../assets/icons/messageSend.svg';

import AdditionalInputField from '../../components/AdditionalInputField';
import InputFieldChat from '../../components/InputFieldChat';
import { database } from '../../config/firebase';
import { NextButton } from '../NewDisinfection/NewDisinfectionStyled';

import Alert from '../../components/Alert';

const Messages = ({ message, user }) => {
  const owner = message?.ownerID;
  // console.log(owner);
  const [chatUser, setChatUser] = useState({});
  const [loading, setLoading] = useState(false);

  // console.log(filteredUid);

  useEffect(() => {
    setLoading(true);
    const ref = database.ref(`/users`);
    ref
      .orderByChild('uid')
      .equalTo(owner)
      .once('value', function (snapshot) {
        const conversationUser = Object.values(snapshot.val());
        setChatUser(conversationUser[0]);
        setLoading(false);
      });
  }, []);

  // console.log(message);

  return (
    <>
      {message?.ownerID === user.uid ? (
        <MsgWrapper key={message?.id}>
          <Message>
            {message?.contentType === 0 && <p>{message?.message}</p>}
            {message?.contentType === 1 && (
              <div style={{ width: '100%' }}>
                <img
                  src={message?.profilePicLink}
                  style={{
                    objectFit: 'contain',
                    marginBottom: '30px',
                    width: '100%',
                  }}
                  alt='im'
                />
              </div>
            )}

            <h5>
              {momentDate(message?.timestamp) +
                ' ' +
                momentTime(message?.timestamp)}
            </h5>
          </Message>
        </MsgWrapper>
      ) : (
        <MsgWrapperOp key={message?.id}>
          <ChatProfile>
            {chatUser?.imageURL && <img src={chatUser?.imageURL} alt='user' />}
          </ChatProfile>
          <MessageOp>
            <MsgWrapperOpInner>
              {message?.contentType === 0 && <p>{message?.message}</p>}
              {message?.contentType === 1 && (
                <img src={message?.profilePicLink} alt='im' />
              )}
              <h5>
                {momentDate(message?.timestamp) +
                  ' ' +
                  momentTime(message?.timestamp)}
              </h5>
            </MsgWrapperOpInner>
          </MessageOp>
        </MsgWrapperOp>
      )}
    </>
  );
};

const Chat = ({ match }) => {
  const id = match.params.id;
  const { user } = useSelector((state) => state.userInfo);
  const { adminAdd, addAdminLoading, error } = useSelector(
    (state) => state.conversations
  );

  const dispatch = useDispatch();
  const [message, setMessage] = useState('');

  const [messages, setMessages] = useState([]);
  const [conversation, setConversation] = useState(null);

  const [opponentUser, setOpponentUser] = useState(null);

  // const [noMessages, setNoMessages] = useState([]);

  const [loading, setLoading] = useState(false);

  // console.log(user?.uid);

  const imageRef = useRef(null);

  const imageSelect = () => {
    imageRef.current.click();
  };

  const handleChange = (event) => {
    const fileUploaded = event.target.files[0];
    const extension = fileUploaded.name.substr(fileUploaded.name.length - 3); // => "jpg"

    let reader = new FileReader();
    reader.onload = function (e) {
      const base64result = e.target.result.split(',')[1];

      dispatch(
        sendImageChat(extension, base64result, user?.uid, match?.params?.id)
      );
    };
    reader.readAsDataURL(event.target.files[0]);
  };

  const handleMessage = (e) => {
    e.preventDefault();
    dispatch(sendMessageChat(user?.uid, match?.params?.id, 0, message));
    setMessage('');
  };

  useEffect(() => {
    setLoading(true);
    if (id) {
      const ref = database.ref(`/conversations/${match?.params?.id}`);
      ref.on('value', function (snapshot) {
        const data = snapshot.val();
        if (data) {
          //SET MESSAGES IS READ
          // console.log(snapshot.val()?.isRead[user?.uid]);
          if (snapshot.val()?.isRead[user?.uid] === false) {
            dispatch(setLastMessageRead(user?.uid, match?.params?.id));
          }
          ///////
          //PUT MESSAGES IN STATE
          // if(snapshot?.)
          const messagess =
            snapshot?.val()?.messages &&
            Object.values(snapshot?.val()?.messages);

          setMessages(messagess);

          // console.log(messagess);
          ///////
          //PUT CONVERSATION IN STATE

          if (snapshot?.val()) {
            setConversation(snapshot?.val());
          }

          /////////

          //GET OPPONENT USER OBJECT
          const opponentID = snapshot.val()?.userIDs?.filter((chat, i) => {
            return chat !== user?.uid && i < 2;
          });
          const ref = database.ref(`/users`);
          ref
            .orderByChild('uid')
            .equalTo(opponentID[0])
            .once('value', function (snapshot) {
              const conversationUser = Object.values(snapshot.val());
              setOpponentUser(conversationUser[0]);
            });
          ////////
        }
        setLoading(false);
        // console.log(snapshot.val());
        // console.log(messagess);
      });
    }
  }, []);
  console.log(messages);

  useEffect(() => {
    loading ? NProgress.start() : NProgress.done();
  });

  const messagesEndRef = useRef(null);

  // const scrollToBottom = () => {
  //   messagesEndRef?.current?.scrollIntoView({ behavior: 'smooth' });
  // };

  // useEffect(scrollToBottom, [messages]);

  useEffect(() => {
    messagesEndRef?.current?.scrollIntoView({ behavior: 'smooth' });
  }, [messages]);

  return (
    <>
      {!loading && (
        <>
          {adminAdd && <Alert message={adminAdd} />}
          {error && <Alert message={error} />}
          <BookContent
            style={{ position: 'relative', height: 'calc(100vh - 60px)' }}
          >
            <div
              style={{
                width: '100%',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}
            >
              <BookTitle>
                {opponentUser?.firstName + ' ' + opponentUser?.lastName}
              </BookTitle>

              {conversation?.userIDs.length < 3 && (
                <NextButton
                  style={{ width: '200px' }}
                  onClick={() => dispatch(addAdmin(match?.params?.id))}
                >
                  Add Admin
                </NextButton>
              )}
            </div>
            <ChatWrapper>
              {messages?.length === 0 && (
                <div style={{ textAlign: 'center' }}>no messages yet</div>
              )}
              {messages
                ?.sort((a, b) => {
                  return a?.timestamp - b?.timestamp;
                })
                ?.map((message, i) => (
                  <Messages key={i} message={message} user={user} />
                ))}
              <div ref={messagesEndRef} />
            </ChatWrapper>
            <form onSubmit={(e) => handleMessage(e)}>
              <ChatInput>
                <div>
                  <input
                    ref={imageRef}
                    type='file'
                    style={{ display: 'none' }}
                    onChange={handleChange}
                  />
                  <img
                    style={{
                      marginRight: '13px',
                      width: 30,
                      cursor: 'pointer',
                    }}
                    src={imageUpload}
                    alt='settings'
                    onClick={imageSelect}
                  />
                </div>
                <InputFieldChat
                  value={message}
                  onChange={(e) => setMessage(e.target.value)}
                />
                <img
                  style={{ marginLeft: '18px' }}
                  src={send}
                  alt='send'
                  onClick={() => handleMessage()}
                />
              </ChatInput>
            </form>
          </BookContent>
        </>
      )}
    </>
  );
};

export default Chat;
// const Maids = ({ maids }) => {
//   return (
//     <>
//       {maids &&
//         maids.map((maid) => (
//           <div key={maid?.uid}>
//             <Link
//               to={`/UserDetails/${maid?.role}/${maid?.uid}`}
//               key={maid?.uid}
//             >
//               <Maid>
//                 <div style={{ width: '15%' }}>{maid?.firstName}</div>
//                 <div style={{ width: '17%' }}>{maid?.lastName}</div>
//                 <div style={{ width: '30%' }}>{maid?.email}</div>
//                 <div style={{ width: '25%' }}>{maid?.phoneNumber}</div>
//                 <div style={{ width: '13%' }}>{maid?.status}</div>
//               </Maid>
//               <MaidMobile>
//                 <div>
//                   <p>First Name</p>
//                   {maid?.firstName}
//                 </div>
//                 <div>
//                   <p>Last Name</p>
//                   {maid?.lastName}
//                 </div>
//                 <div>
//                   <p>Email</p>
//                   {maid?.email}
//                 </div>
//                 <div>
//                   <p>Phone Number</p>
//                   {maid?.phoneNumber}
//                 </div>
//                 <div>
//                   <p>Cleaning Status</p>
//                   {maid?.status}
//                 </div>
//               </MaidMobile>
//             </Link>
//           </div>
//         ))}
//     </>
//   );
// };
