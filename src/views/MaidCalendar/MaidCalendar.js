/* eslint-disable react/no-unescaped-entities */
import React, { useState, useEffect, useLayoutEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  BookContent,
  BookTitle,
  FlexBetween,
  Maid,
  MaidMobile,
  TimeDateTab,
  SelectContainer,
  Label,
  HalfBox,
} from './MaidCalendarStyled';

import { getMaids } from '../../actions/maidsActions';
import { getUnavailability } from '../../actions/unavailabilityActions';

import NProgress from 'nprogress';

import { SingleDatePicker } from 'react-dates';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import { NextButton } from '../NewDisinfection/NewDisinfectionStyled';
import arrow from '../../assets/icons/arrow.svg';
import Select from 'react-select';
import ErrorMessage from '../../components/ErrorMessage';
import { colourStyles } from '../../styles/SelectStyle';
import { momentTime } from '../../helperFunction';
import moment from 'moment-timezone';
// var moment = require('moment-timezone');

const AllMaids = () => {
  const dispatch = useDispatch();

  const { maids, loadingMaids } = useSelector((state) => state.maids);
  const { unavailability, loadingUnavailability } = useSelector(
    (state) => state.unavailability
  );

  const [error, setError] = useState('');
  const [date, setDate] = useState(moment.tz('Etc/GMT+4'));
  const [focused, setFocused] = useState(null);
  const [select, setSelect] = useState(null);

  useEffect(() => {
    dispatch(getMaids());
  }, [dispatch]);

  useEffect(() => {
    loadingMaids ? NProgress.start() : NProgress.done();
    loadingUnavailability ? NProgress.start() : NProgress.done();
  });

  // const time = moment.tz('Etc/GMT+4');

  const maidOptions = maids?.map((maid, i) => {
    return { label: `${maid?.firstName} ${maid?.lastName}`, value: maid?.uid };
  });

  // const handleSubmit = () => {
  //   if (select && date) {
  //     dispatch(
  //       getUnavailability(
  //         select?.value,
  //         moment(date).startOf('day').unix(),
  //         moment(date).endOf('day').unix()
  //       )
  //     );
  //   } else {
  //     setError('Please fill all fields');
  //     setTimeout(() => {
  //       setError('');
  //     }, 3000);
  //   }
  // };
  useLayoutEffect(() => {
    if (select && date) {
      dispatch(
        getUnavailability(
          select?.value,
          moment(date).startOf('day').unix(),
          moment(date).endOf('day').unix()
        )
      );
    }
  }, [select, date]);

  return (
    <>
      <BookContent>
        <BookTitle>Maid Calendar</BookTitle>

        <FlexBetween>
          <HalfBox>
            <Label>Choose maid</Label>
            <SelectContainer>
              <Select
                label='Status'
                options={maidOptions}
                styles={colourStyles}
                onChange={(e) => {
                  setSelect(e);
                }}
                value={select}
                components={{
                  IndicatorSeparator: () => null,
                }}
                placeholder='Select'
                className='mb-2'
              />
            </SelectContainer>
          </HalfBox>

          <HalfBox>
            <Label>Select Date</Label>
            <TimeDateTab onClick={({ focused }) => setFocused(!focused)}>
              <div className='dateInp'>
                {/* <p>To</p> */}
                <SingleDatePicker
                  date={date}
                  onDateChange={(date) => setDate(date)}
                  focused={focused}
                  onFocusChange={({ focused }) => setFocused(focused)}
                  numberOfMonths={1}
                  displayFormat={() => 'MMMM D, YYYY'}
                  isOutsideRange={() => false}
                />
              </div>
              <img
                src={arrow}
                alt='arrow'
                className='arrow-picker'
                onClick={({ focused }) => setFocused(!focused)}
              />
            </TimeDateTab>
          </HalfBox>
        </FlexBetween>

        {/* <NextButton onClick={() => handleSubmit()}>
          Check unavailability
        </NextButton> */}
        {error && <ErrorMessage error={error} />}
        {unavailability?.length === 0 ? (
          <p>No unavailability found</p>
        ) : (
          <>
            {unavailability && (
              <>
                <p>List of unavailabilities:</p>
                {unavailability?.map((unavailable, i) => (
                  <Maid key={i}>
                    {momentTime(unavailable?.startTime)} -{' '}
                    {momentTime(unavailable?.endTime)}
                  </Maid>
                ))}
              </>
            )}
          </>
        )}
      </BookContent>
    </>
  );
};

export default AllMaids;
