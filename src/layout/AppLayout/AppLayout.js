/* eslint-disable react/no-unescaped-entities */
import React, { useState } from 'react';
import Sidebar from '../../components/Sidebar/Sidebar';
import { Layout, Main, FixedSidebar, TopNav } from './AppLayoutStyled';
import SideMenu from 'react-sidebar';
import bt from '../../assets/icons/menu.svg';
import logo from '../../assets/images/logo2text.svg';
import Animation from '../../components/Animation';
import { Link } from 'react-router-dom';

const AppLayout = ({ children }) => {
  const [open, setOpen] = useState(false);

  return (
    <div
      style={{
        width: '100%',
        height: '100vh',
        position: 'fixed',
        top: 0,
        bottom: 0,
        paddingBottom: '100px',
      }}
    >
      <SideMenu
        touch={false}
        sidebar={<Sidebar />}
        open={open}
        onSetOpen={setOpen}
        styles={{
          sidebar: {
            background: 'none',
            // top: '0',
            // bottom: '0',
            boxShadow: '1px 1px 5px rgba(177, 181, 198, 0)',
            zIndex: '101',
          },
        }}
      >
        <Layout>
          <TopNav>
            <img
              src={bt}
              alt='sidebar'
              // style={{ height: 40 }}
              onClick={() => setOpen(!open)}
            />
            {/* <Link
              to='/BookNow'
              style={{ display: 'flex', justifyContent: 'flex-end' }}
            > */}
            <img src={logo} alt='logo' style={{ height: 40 }} />
            {/* </Link> */}
          </TopNav>
          <FixedSidebar>
            <Sidebar />
          </FixedSidebar>
          <Animation>
            <Main>{children}</Main>
          </Animation>
        </Layout>
      </SideMenu>
    </div>
  );
};

export default AppLayout;
