import styled from 'styled-components';

const Layout = styled.div`
  display: flex;
  width: 100%;
  min-height: 100vh;
  @media (max-width: 690px) {
    flex-direction: column;
  }
`;

const Main = styled.div`
  /* margin: 50px; */
  padding: 30px;
  width: calc(100% - 300px);
  @media (max-width: 1100px) {
    padding: 20px;
    margin: 2%;
  }
  @media (max-width: 690px) {
    width: 100%;
    padding: 20px;
    margin: 0;
    /* padding-bottom: 150px; */
  }
`;

const FixedSidebar = styled.div`
  @media (max-width: 690px) {
    display: none;
  }
`;

const TopNav = styled.div`
  display: none;
  width: 100%;
  height: 50px;
  padding: 10px;
  @media (max-width: 690px) {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
`;

export { Layout, Main, FixedSidebar, TopNav };
