const {
  getAccessToken,
  setNewAccessToken,
  getRefreshToken,
} = require('./localStorage-helper.js');

window.axios = require('axios');

const API_KEY = process.env.GOOGLE_API_KEY;

window.axios.defaults.headers.common['Authorization'] =
  'Bearer ' + getAccessToken();

const refreshTokenFromLS = getRefreshToken();

async function refreshToken() {
  try {
    const configToken = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    };

    const response = await window.axios.post(
      `https://securetoken.googleapis.com/v1/token?key=${API_KEY}&grant_type=refresh_token&refresh_token=${refreshTokenFromLS}`,
      configToken
    );

    console.log(response);

    let newAccessToken = response.data.data.access_token;
    setNewAccessToken(newAccessToken);
    window.axios.defaults.headers.common[
      'Authorization'
    ] = `Bearer ${newAccessToken}`;
  } catch (error) {
    console.log(error.response);
  }
}

window.axios.interceptors.request.use(
  function (config) {
    const tokenFromLS = getAccessToken();

    if (tokenFromLS) {
      config.headers.Authorization = `Bearer ${tokenFromLS}`;
    } else {
      config.headers.Authorization = config.headers.Authorization;
    }

    return config;
  },
  function (err) {
    console.log(err);
    return err;
  }
);

window.axios.interceptors.response.use(
  function (response) {
    const { status, data, config } = response;
    console.log(`Response from ${config.url}:`, {
      code: status,
      ...data,
    });
    return response;
  },
  async function (error) {
    if (error.response) {
      const { status, data } = error.response;

      switch (status) {
        case 403:
          if (data.message === 'Unauthorized') {
            try {
              await refreshToken();
              const config = error.config;
              return await window.axios({
                method: config.method,
                url: config.url,
                data: config.data,
              });
            } catch (e) {
              return (window.location.href = '/');
            }
          }
        // else {
        //   console.log(data);
        // return (window.location.href = '/');
        // }
        default:
          return Promise.reject(error);
      }
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      return Promise.reject(error);
    } else {
      // Something happened in setting up the request that triggered an Error
      return Promise.reject(error);
    }
  }
);
