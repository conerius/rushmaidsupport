import {
  GET_CUSTOMERS_REQUEST,
  GET_CUSTOMERS_SUCCESS,
  GET_CUSTOMERS_FAIL,
} from '../constants/customersConstants';

export const customers = (state = {}, action) => {
  switch (action.type) {
    case GET_CUSTOMERS_REQUEST:
      return {
        ...state,
        loadingCustomers: true,
      };

    case GET_CUSTOMERS_SUCCESS: {
      return {
        ...state,
        loadingCustomers: false,
        customers: action.payload,
      };
    }

    case GET_CUSTOMERS_FAIL:
      return {
        ...state,
        loadingCustomers: false,
        error: action.payload,
      };

    default:
      return state;
  }
};
