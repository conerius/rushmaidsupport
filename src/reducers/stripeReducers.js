import {
  GET_CUSTOMER_REQUEST,
  GET_CUSTOMER_SUCCESS,
  GET_CUSTOMER_FAIL,
  CREATE_CUSTOMER_REQUEST,
  CREATE_CUSTOMER_SUCCESS,
  CREATE_CUSTOMER_FAIL,
  CREATE_CREDIT_CARD_REQUEST,
  CREATE_CREDIT_CARD_SUCCESS,
  CREATE_CREDIT_CARD_FAIL,
} from '../constants/stripeConstants';

export const stripeInfo = (state = { loadingToken: false }, action) => {
  switch (action.type) {
    case GET_CUSTOMER_REQUEST:
      return {
        ...state,
        customerLoading: true,
      };

    case GET_CUSTOMER_SUCCESS:
      return {
        ...state,
        customerLoading: false,
        customer: action.payload,
      };

    case GET_CUSTOMER_FAIL:
      return {
        ...state,
        customerLoading: false,
        errorCustomer: action.payload.message,
      };

    case CREATE_CREDIT_CARD_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case CREATE_CREDIT_CARD_SUCCESS:
      return {
        ...state,
        loading: false,
      };

    case CREATE_CREDIT_CARD_FAIL:
      return {
        ...state,
        loading: false,
        error:
          action.payload.message.code === 'token_already_used'
            ? 'Credit card card is already taken'
            : action.payload.message.code,
      };

    case CREATE_CUSTOMER_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case CREATE_CUSTOMER_SUCCESS:
      return {
        ...state,
        loading: false,
        customerId: action.payload,
      };

    case CREATE_CUSTOMER_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload.message,
      };

    default:
      return state;
  }
};
