import {
  GET_BOOKING_ADDRESS,
  GET_QUESTIONS,
} from '../constants/additionalQuestionConstants';

export const additionalQuestions = (state = {}, action) => {
  switch (action.type) {
    case GET_BOOKING_ADDRESS:
      return {
        bookingAddress: action.payload.address,
        bookingCoordinates: action.payload.coordinates,
      };

    case GET_QUESTIONS: {
      return {
        ...state,
        questions: action.payload.questions,
        pets: action.payload.pets,
        airBnb: action.payload.airBnb,
        zipCode: action.payload.zipCode,
      };
    }

    default:
      return state;
  }
};
