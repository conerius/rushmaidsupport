import {
  INCREMENT,
  DECREMENT,
  GET_TOTAL_CLEANING,
  SET_BOOKING_DATE,
  SET_BOOKING_TIME,
  GET_BOOKING_TOTAL_PRICE,
  SET_BOOKING_PROMO_CODE,
  SET_BOOKING_PROMO_CODE_ID,
  SET_BOOKING_DISCOUNT,
  CREATE_BOOKING_REQUEST,
  CREATE_BOOKING_SUCCESS,
  CREATE_BOOKING_FAIL,
  CREATE_FREE_BOOKING_REQUEST,
  CREATE_FREE_BOOKING_SUCCESS,
  CREATE_FREE_BOOKING_FAIL,
} from '../constants/bookingConstants';

import bedroom from '../assets/icons/bedroom.svg';
import bathroom from '../assets/icons/bathroom.svg';
import kitchen from '../assets/icons/kitchen.svg';
import livingArea from '../assets/icons/bed.svg';
import balcony from '../assets/icons/balcony.svg';

import moment from 'moment';

const initialState = {
  tabs: [
    {
      id: 1,
      title: 'Bedroom',
      icon: bedroom,
      num: 1,
      price: 60,
      total: 60,
      number: 1,
    },
    {
      id: 2,
      title: 'Bathroom',
      icon: bathroom,
      num: 1,
      price: 60,
      total: 60,
      number: 1,
    },
    {
      id: 3,
      title: 'Kitchen',
      icon: kitchen,
      num: 0,
      price: 10,
      total: 0,
      number: 0,
    },
    {
      id: 4,
      title: 'Living Area',
      icon: livingArea,
      num: 0,
      price: 10,
      total: 0,
      number: 0,
    },
    {
      id: 5,
      title: 'Balcony',
      icon: balcony,
      num: 0,
      price: 15,
      total: 0,
      number: 0,
    },
  ],
  bookingTotal: 120,
  bookingTotalPrice: 0,
  bookingDate: moment(),
  bookingTime: moment(),
  bookingPromoCode: '',
  bookingPromoCodeId: '',
  bookingDiscount: '',
};

export const bookings = (state = initialState, action) => {
  switch (action.type) {
    case INCREMENT:
      return {
        ...state,
        tabs: state.tabs.map((tab) => {
          if (tab.id === action.payload) {
            if (tab.id === 1 || tab.id === 2) {
              return {
                ...tab,
                num: tab.num + 1,
                total:
                  tab.num === 1
                    ? (tab.total += 35)
                    : tab.num >= 5
                    ? (tab.total += 40)
                    : (tab.total += 35),
              };
            } else if (tab.id === 3 || tab.id === 4) {
              return {
                ...tab,
                num: tab.num + 1,
                total: tab.num > 0 ? (tab.total += tab.price) : tab.total,
              };
            } else {
              return {
                ...tab,
                num: tab.num + 1,
                total: (tab.total += tab.price),
              };
            }
          } else {
            return tab;
          }
        }),
      };

    case DECREMENT:
      return {
        ...state,
        tabs: state.tabs.map((tab) => {
          if (tab.id === action.payload) {
            if (tab.id === 1 || tab.id === 2) {
              return {
                ...tab,
                num: tab.num !== tab.number ? tab.num - 1 : tab.number,
                total:
                  tab.num === 1
                    ? tab.price
                    : tab.num >= 5
                    ? (tab.total -= 40)
                    : (tab.total -= 35),
              };
            } else if (tab.id === 3 || tab.id === 4) {
              return {
                ...tab,
                num: tab.num !== tab.number ? tab.num - 1 : tab.number,
                total: tab.num > 1 ? (tab.total -= tab.price) : tab.total,
              };
            } else {
              return {
                ...tab,
                num: tab.num !== tab.number ? tab.num - 1 : tab.number,
                total: tab.num === 0 ? tab.price : (tab.total -= 15),
              };
            }
          } else {
            return tab;
          }
        }),
      };

    case SET_BOOKING_DATE:
      return {
        ...state,
        bookingDate: action.payload,
      };

    case SET_BOOKING_TIME:
      return {
        ...state,
        bookingTime: action.payload,
      };

    case GET_TOTAL_CLEANING:
      return {
        ...state,
        bookingTotal: action.payload,
      };

    case GET_BOOKING_TOTAL_PRICE:
      return {
        ...state,
        bookingTotalPrice: action.payload,
      };

    case SET_BOOKING_PROMO_CODE:
      return {
        ...state,
        bookingPromoCode: action.payload,
      };

    case SET_BOOKING_PROMO_CODE_ID:
      return {
        ...state,
        bookingPromoCodeId: action.payload,
      };

    case SET_BOOKING_DISCOUNT:
      return {
        ...state,
        bookingDiscount: action.payload,
      };
    case CREATE_BOOKING_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case CREATE_BOOKING_SUCCESS:
      return {
        ...state,
        loading: false,
        booking: action.payload.data,
        message: action.payload.message,
      };

    case CREATE_BOOKING_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case CREATE_BOOKING_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case CREATE_BOOKING_SUCCESS:
      return {
        ...state,
        loading: false,
        booking: action.payload.data,
        message: action.payload.message,
      };

    case CREATE_BOOKING_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case CREATE_FREE_BOOKING_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case CREATE_FREE_BOOKING_SUCCESS:
      return {
        ...state,
        loading: false,
        booking: action.payload.data,
        message: action.payload.message,
      };

    case CREATE_FREE_BOOKING_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};
