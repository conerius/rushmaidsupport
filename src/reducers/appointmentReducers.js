import {
  GET_APPOINTMENTS_REQUEST,
  GET_APPOINTMENTS_SUCCESS,
  GET_APPOINTMENTS_FAIL,
} from '../constants/appointmentConstants';

export const appointments = (state = {}, action) => {
  switch (action.type) {
    case GET_APPOINTMENTS_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case GET_APPOINTMENTS_SUCCESS: {
      return {
        ...state,
        loading: false,
        appointments: action.payload,
      };
    }

    case GET_APPOINTMENTS_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    default:
      return state;
  }
};
