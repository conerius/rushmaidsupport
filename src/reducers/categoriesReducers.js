import {
  GET_CATEGORIES_REQUEST,
  GET_CATEGORIES_SUCCESS,
  GET_CATEGORIES_FAIL,
  CHECK_CATEGORY,
  GET_CATEGORIES_TOTAL,
  GET_SELECTED_INDEX,
} from '../constants/categoriesConstants';

export const categories = (state = { categoriesTotal: 0 }, action) => {
  switch (action.type) {
    case GET_CATEGORIES_REQUEST:
      return {
        loading: true,
      };

    case GET_CATEGORIES_SUCCESS:
      return {
        ...state,
        loading: false,
        categories: action.payload,
        categoriesTotal: 0,
        selectedIndex: '',
      };

    case GET_CATEGORIES_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case CHECK_CATEGORY:
      return {
        ...state,
        categories: state.categories.map((category) =>
          category.selfID === action.payload
            ? { ...category, checked: !category.checked }
            : category
        ),
      };

    case GET_CATEGORIES_TOTAL:
      return {
        ...state,
        categoriesTotal: action.payload,
      };

    case GET_CATEGORIES_TOTAL:
      return {
        ...state,
        categoriesTotal: action.payload,
      };

    case GET_SELECTED_INDEX:
      return {
        ...state,
        selectedIndex: action.payload,
      };
    default:
      return state;
  }
};
