import {
  CREATE_DISINFECTION_REQUEST,
  CREATE_DISINFECTION_SUCCESS,
  CREATE_DISINFECTION_FAIL,
  SET_DISINFECTION_DATE,
  SET_DISINFECTION_TIME,
  SET_DISINFECTION_ADDRESS,
  SET_DISINFECTION_TOTAL_PRICE,
  SET_DISINFECTION_ZIP_CODE,
  SET_DISINFECTION_PROMO_CODE,
  SET_DISINFECTION_FEETS,
  SET_DISINFECTION_DISCOUNT,
  SET_DISINFECTION_PROMO_CODE_ID,
} from '../constants/disnifectionConstants';

import moment from 'moment';

const initialState = {
  loading: false,
  disinfectionTime: moment(),
  disinfectionDate: moment(),
  value: 0,
  disinfectionTotalPrice: (0).toFixed(2),
  disinfectionAddress: '',
  disinfectionCoordinates: {
    lat: null,
    lng: null,
  },
  disinfectionZipCode: '',
  disinfectionPromoCode: '',
  disinfectionDiscount: '',
  disinfectionPromoCodeId: '',
};

export const disinfections = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_DISINFECTION_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case CREATE_DISINFECTION_SUCCESS:
      return {
        ...state,
        loading: false,
        disinfection: action.payload.data,
        message: action.payload.message,
      };

    case CREATE_DISINFECTION_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case SET_DISINFECTION_FEETS:
      return {
        ...state,
        value: action.payload,
      };

    case SET_DISINFECTION_DATE:
      return {
        ...state,
        disinfectionDate: action.payload,
      };

    case SET_DISINFECTION_TIME:
      return {
        ...state,
        disinfectionTime: action.payload,
      };

    case SET_DISINFECTION_ADDRESS:
      return {
        ...state,
        disinfectionAddress: action.payload.address,
        disinfectionCoordinates: action.payload.coordinates,
      };

    case SET_DISINFECTION_ZIP_CODE:
      return {
        ...state,
        disinfectionZipCode: action.payload,
      };

    case SET_DISINFECTION_PROMO_CODE:
      return {
        ...state,
        disinfectionPromoCode: action.payload,
      };

    case SET_DISINFECTION_PROMO_CODE_ID:
      return {
        ...state,
        disinfectionPromoCodeId: action.payload,
      };

    case SET_DISINFECTION_DISCOUNT:
      return {
        ...state,
        disinfectionDiscount: action.payload,
      };

    case SET_DISINFECTION_TOTAL_PRICE:
      return {
        ...state,
        disinfectionTotalPrice: action.payload,
      };

    default:
      return state;
  }
};
