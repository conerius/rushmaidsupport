import {
  GET_PROMO_CODE_REQUEST,
  GET_PROMO_CODE_SUCCESS,
  GET_PROMO_CODE_FAIL,
} from '../constants/promoCodesConstants';

export const promoCodes = (state = { codes: [] }, action) => {
  switch (action.type) {
    case GET_PROMO_CODE_REQUEST:
      return {
        loading: true,
      };

    case GET_PROMO_CODE_SUCCESS:
      return {
        ...state,
        loading: false,
        promoCodes: action.payload,
      };

    case GET_PROMO_CODE_FAIL:
      return {
        ...state,
        loading: false,
        errorPromoCodes: action.payload,
      };

    default:
      return state;
  }
};
