import {
  GET_CONVERSATIONS_REQUEST,
  GET_CONVERSATIONS_SUCCESS,
  GET_CONVERSATIONS_FAIL,
  ADD_ADMIN_REQUEST,
  ADD_ADMIN_SUCCESS,
  ADD_ADMIN_FAIL,
} from '../constants/chatConstants';

import { CLEAR_ALERT, CLEAR_MESSAGE } from '../constants/alertConstants';

export const conversations = (state = {}, action) => {
  switch (action.type) {
    case GET_CONVERSATIONS_REQUEST:
      return {
        ...state,
        loadingConversations: true,
      };

    case GET_CONVERSATIONS_SUCCESS: {
      return {
        ...state,
        loadingConversations: false,
        conversations: action.payload,
      };
    }

    case GET_CONVERSATIONS_FAIL:
      return {
        ...state,
        loadingConversations: false,
        error: action.payload,
      };

    case ADD_ADMIN_REQUEST:
      return {
        ...state,
        addAdminLoading: true,
      };

    case ADD_ADMIN_SUCCESS: {
      return {
        ...state,
        addAdminLoading: false,
        adminAdd: action.payload,
      };
    }

    case ADD_ADMIN_FAIL:
      return {
        ...state,
        addAdminLoading: false,
        error: action.payload,
      };

    case CLEAR_MESSAGE:
      return {
        ...state,
        adminAdd: action.payload,
      };

    case CLEAR_ALERT:
      return {
        ...state,
        error: action.payload,
      };

    default:
      return state;
  }
};
