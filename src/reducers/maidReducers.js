import {
  GET_MAIDS_REQUEST,
  GET_MAIDS_SUCCESS,
  GET_MAIDS_FAIL,
} from '../constants/maidsConstants';

export const maids = (state = {}, action) => {
  switch (action.type) {
    case GET_MAIDS_REQUEST:
      return {
        ...state,
        loadingMaids: true,
      };

    case GET_MAIDS_SUCCESS: {
      return {
        ...state,
        loadingMaids: false,
        maids: action.payload,
      };
    }

    case GET_MAIDS_FAIL:
      return {
        ...state,
        loadingMaids: false,
        error: action.payload,
      };

    default:
      return state;
  }
};
