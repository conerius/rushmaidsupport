import {
  GET_UNAVAILABILITY_REQUEST,
  GET_UNAVAILABILITY_SUCCESS,
  GET_UNAVAILABILITY_FAIL,
} from '../constants/unavailabilityConstants';

export const unavailability = (state = {}, action) => {
  switch (action.type) {
    case GET_UNAVAILABILITY_REQUEST:
      return {
        ...state,
        loadingUnavailability: true,
      };

    case GET_UNAVAILABILITY_SUCCESS: {
      return {
        ...state,
        loadingUnavailability: false,
        unavailability: action.payload,
      };
    }

    case GET_UNAVAILABILITY_FAIL:
      return {
        ...state,
        loadingUnavailability: false,
        error: action.payload,
      };

    default:
      return state;
  }
};
