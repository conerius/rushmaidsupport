import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

const PrivateRoute = ({ render: Component, ...rest }) => {
  const userInfo = useSelector((state) => state.userInfo);
  return (
    <Route
      {...rest}
      render={(props) =>
        !userInfo.user ? <Redirect to='/' /> : <Component {...props} />
      }
    />
  );
};

export default PrivateRoute;
