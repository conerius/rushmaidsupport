import React from 'react';

import { createGlobalStyle } from 'styled-components';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import axiosInterceptor from './axios-helper';

import PrivateRoute from './routing/PrivateRoute';

import ListOfAllJobs from './views/ListOfAllJobs/ListOfAllJobs';
import Login from './views/Login/Login';
import AllMaids from './views/Maids/Maids';
import MaidCalendar from './views/MaidCalendar/MaidCalendar';

// import ZipCode from './views/ZipCode/ZipCode';
// import PhoneNumber from './views/PhoneNumber/PhoneNumber';
// import ExtraServices from './views/ExtraServices/ExtraServices';
// import AdditionalQuestions from './views/AdditionalQuestions/AdditionalQuestions';
// import CompleteBooking from './views/CompleteBooking/CompleteBooking';
// import Profile from './views/Profile/Profile';
// import Settings from './views/Settings/Settings';
// import TermsAndCondition from './views/TermsAndCondition/TermsAndCondition';
// import PrivacyPolicy from './views/PrivacyPolicy/PrivacyPolicy';
// import BillingInfo from './views/BillingInfo/BillingInfo';
// import RedeemGiftCode from './views/RedeemGiftCode/RedeemGiftCode';
// import NewDisinfection from './views/NewDisinfection/NewDisinfection';
// import AllApointments from './views/AllApointments/AllApointments';
import CleaningDetails from './views/CleaningDetails/CleaningDetails';
import AppointmentQuestions from './views/AppointmentQuestions/AppointmentQuestions';

import Layout from './layout/AppLayout/AppLayout';

import { View } from './styles/viewContainer';
import Customers from './views/Customers/Customers';
import AllApointments from './views/AllApointments/AllApointments';
import AllAppointments from './views/Appointments/Appointments';
import UserDetails from './views/UserDetails/UserDetails';
import ChatList from './views/ChatList/ChatList';
import Chat from './views/Chat/Chat';

const GlobalStyles = createGlobalStyle`
* { 
    box-sizing: border-box;
    margin:0;
    font-family:  'Nunito Sans', -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu';
  }
*:focus {
    outline: none;
}
`;

function App() {
  return (
    <Router>
      <GlobalStyles />
      <View>
        <Route exact path='/' render={(props) => <Login {...props} />} />

        <PrivateRoute
          path='/list-of-all-jobs'
          render={(props) => (
            <Layout>
              <AllAppointments {...props} />
            </Layout>
          )}
        />

        <PrivateRoute
          path='/all-maids'
          render={(props) => (
            <Layout>
              <AllMaids {...props} />
            </Layout>
          )}
        />

        <PrivateRoute
          path='/all-customers'
          render={(props) => (
            <Layout>
              <Customers {...props} />
            </Layout>
          )}
        />

        <PrivateRoute
          exact
          path='/CleaningDetails/:id'
          render={(props) => (
            <Layout>
              <CleaningDetails {...props} />
            </Layout>
          )}
        />

        <PrivateRoute
          exact
          path='/UserDetails/:role/:id'
          render={(props) => (
            <Layout>
              <UserDetails {...props} />
            </Layout>
          )}
        />

        <PrivateRoute
          exact
          path='/AppointmentQuestions'
          render={(props) => (
            <Layout>
              <AppointmentQuestions {...props} />
            </Layout>
          )}
        />
        <PrivateRoute
          exact
          path='/get-calendar-for-maid'
          render={(props) => (
            <Layout>
              <MaidCalendar {...props} />
            </Layout>
          )}
        />

        <PrivateRoute
          exact
          path='/chat-support'
          render={(props) => (
            <Layout>
              <ChatList {...props} />
            </Layout>
          )}
        />

        <PrivateRoute
          exact
          path='/chat/:id'
          render={(props) => (
            <Layout>
              <Chat {...props} />
            </Layout>
          )}
        />

        {/* <PrivateRoute
          path='/ExtraServices/:id'
          render={(props) => (
            <Layout>
              <ExtraServices {...props} />
            </Layout>
          )}
        />

        <PrivateRoute
          exact
          path='/NewDisinfection'
          render={(props) => (
            <Layout>
              <NewDisinfection {...props} />
            </Layout>
          )}
        />

        <PrivateRoute
          path='/Settings'
          render={(props) => (
            <Layout>
              <Settings {...props} />
            </Layout>
          )}
        />

        <PrivateRoute
          path='/PrivacyPolicy'
          render={(props) => (
            <Layout>
              <PrivacyPolicy {...props} />
            </Layout>
          )}
        />

        <PrivateRoute
          path='/TermsAndCondition'
          render={(props) => (
            <Layout>
              <TermsAndCondition {...props} />
            </Layout>
          )}
        />

        <PrivateRoute
          path='/AdditionalQuestions'
          render={(props) => (
            <Layout>
              <AdditionalQuestions {...props} />
            </Layout>
          )}
        />

        <PrivateRoute
          path='/CompleteBooking'
          render={(props) => (
            <Layout>
              <CompleteBooking {...props} />
            </Layout>
          )}
        />

        <PrivateRoute
          path='/RedeemGift'
          render={(props) => (
            <Layout>
              <RedeemGiftCode {...props} />
            </Layout>
          )}
        />

        <PrivateRoute
          exact
          path='/AllAppointments'
          render={(props) => (
            <Layout>
              <AllApointments {...props} />
            </Layout>
          )}
        />

        <PrivateRoute
          exact
          path='/CleaningDetails/:id'
          render={(props) => (
            <Layout>
              <CleaningDetails {...props} />
            </Layout>
          )}
        />

       

        <PrivateRoute
          path='/Profile'
          render={(props) => (
            <Layout>
              <Profile {...props} />
            </Layout>
          )}
        />

        <PrivateRoute
          exact
          path='/BillingInfo/:zipCode?'
          render={(props) => (
            <Layout>
              <BillingInfo {...props} />
            </Layout>
          )}
        />

        <Route exact path='/register' component={Register} />

        <Route exact path='/zipCode' component={ZipCode} />

        <Route exact path='/phoneNumber' component={PhoneNumber} /> */}
      </View>
    </Router>
  );
}

export default App;
