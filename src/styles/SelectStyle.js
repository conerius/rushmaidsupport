export const colourStyles = {
  control: (styles) => ({
    ...styles,
    backgroundColor: 'none',
    border: 'none',
    borderRadius: 8,
    minHeight: 55,
    padding: '0 9px',
    boxShadow: 'none',
    marginBottom: '20px',
    fontWeight: '600',
    color: '#000',
  }),
  option: (styles, { data, isDisabled, isFocused, isSelected }) => {
    return {
      ...styles,
      background: isDisabled
        ? null
        : isSelected
        ? '#b4b2f1'
        : isFocused
        ? 'rgba(45, 45, 45, 0.05)'
        : null,
      color: isDisabled
        ? '#000'
        : isSelected
        ? '#fff'
        : isFocused
        ? '#000'
        : '#000',
      cursor: isDisabled ? 'not-allowed' : 'default',
      fontWeight: 600,
      // ':active': {
      //   ...styles[':active'],
      //   backgroundColor:
      //     !isDisabled && (isSelected ? data.color : color.alpha(0.3).css()),
      // },
    };
  },
  input: (styles) => ({
    ...styles,
    backgroundColor: 'none',
    color: '#000000',
  }),
  // placeholder: (styles) => ({ ...styles, ...dot() }),
  singleValue: (styles, { data }) => ({ ...styles, color: '#000' }),
};
