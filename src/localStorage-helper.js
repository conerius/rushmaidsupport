const store = (key, value) => {
  return localStorage.setItem(key, value);
};

const read = (key) => {
  return localStorage.getItem(key);
};

const getTokens = () => {
  return JSON.parse(read('userRushmaidSupport'))?.token;
};

const getAccessToken = () => {
  return JSON.parse(read('userRushmaidSupport'))?.idToken;
};

const getRefreshToken = () => {
  return JSON.parse(read('userRushmaidSupport'))?.refreshToken;
};

const setNewAccessToken = (accessToken) => {
  let userRushmaidSupport = JSON.parse(read('userRushmaidSupport'));
  userRushmaidSupport.token = accessToken;
  store('userRushmaidSupport', JSON.stringify(userRushmaidSupport));
};

module.exports = {
  read,
  getTokens,
  getAccessToken,
  setNewAccessToken,
  getRefreshToken,
};
