import React, { createContext, useState } from 'react';
import axios from 'axios';

export const BillingInfoContext = createContext();

const API_KEY = process.env.REACT_APP_API_URL;

const user =
  JSON.parse(localStorage.getItem('user')) &&
  JSON.parse(localStorage.getItem('user'));

const BillingInfoContextProvider = ({ children }) => {
  const [cardNumber, setCardNumber] = useState('');
  const [cardExpireDate, setCardExpireDate] = useState('');
  const [cardCvc, setCardCvc] = useState('');
  const [cardToken, setCardToken] = useState('');
  const [customer, setCustomer] = useState('');
  const [error, setError] = useState('');
  const [creditCardLoading, setCreditCardLoading] = useState(false);

  const getCardToken = async () => {
    try {
      setCreditCardLoading(true);
      const cardInfo = {
        cardNumber: cardNumber,
        expireMonth: cardExpireDate.split('/')[0],
        expireYear: cardExpireDate.split('/')[1],
        cvc: cardCvc,
      };

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.stsTokenManager.accessToken}`,
        },
      };

      const { data } = await axios.post(
        `${API_KEY}/stripe/get-token`,
        cardInfo,
        config
      );

      setError('');

      setCreditCardLoading(false);

      return data.data.id;
    } catch (error) {
      setCreditCardLoading(false);
      setError(error.response.data.message);
    }
  };

  const getCustomer = async (customerId) => {
    try {
      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.stsTokenManager.accessToken}`,
        },
      };

      const { data } = await axios.get(
        `${API_KEY}/stripe/customer/${customerId}`,
        config
      );

      setCustomer(data.data);
    } catch (error) {
      setError(error.response.data.message);
    }
  };

  const createCustomer = async (cardTokenId) => {
    try {
      const cardInfo = {
        name: user.displayName === null ? '' : user.displayName,
        email: user.email,
        source: cardTokenId,
        description: 'iOS Customer',
      };

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.stsTokenManager.accessToken}`,
        },
      };

      const { data } = await axios.post(
        `${API_KEY}/stripe/customer/create`,
        cardInfo,
        config
      );

      setCustomer(data.data);

      localStorage.setItem('customer', JSON.stringify(data.data));

      return data.data;
    } catch (error) {
      setError(error.response.data.message);
    }
  };

  const createCreditCard = async (customer, cardTokenId) => {
    try {
      if (!cardTokenId) {
        setError('Please fill all fields');
      } else {
        setCreditCardLoading(true);

        const userInfo = {
          customerId: customer.id,
          source: cardTokenId,
        };

        const config = {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${user.stsTokenManager.accessToken}`,
          },
        };

        const { data } = await axios.post(
          `${API_KEY}/stripe/customer/create-credit-card`,
          userInfo,
          config
        );

        getCustomer(data.data.customer);

        setCreditCardLoading(false);
      }

      // localStorage.setItem('customer', JSON.stringify(data.data));
    } catch (error) {
      setCreditCardLoading(false);
      setError(error.response.data.message);
    }
  };

  const updateCreditCard = async (customer, cardId) => {
    try {
      const userInfo = {
        customerId: customer.id,
        default_source: cardId,
      };

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.stsTokenManager.accessToken}`,
        },
      };

      const { data } = await axios.post(
        `${API_KEY}/stripe/customer/update`,
        userInfo,
        config
      );
    } catch (error) {
      console.log(error.message);
    }
  };

  return (
    <BillingInfoContext.Provider
      value={{
        cardNumber,
        cardExpireDate,
        cardCvc,
        setCardNumber,
        setCardExpireDate,
        setCardCvc,
        cardToken,
        getCardToken,
        customer,
        error,
        setError,
        getCustomer,
        createCustomer,
        createCreditCard,
        creditCardLoading,
      }}
    >
      {children}
    </BillingInfoContext.Provider>
  );
};

export default BillingInfoContextProvider;
