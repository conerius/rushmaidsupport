import React, { useState, createContext } from 'react';
import axios from 'axios';

export const ExtraServicesContext = createContext();

const API_KEY = process.env.REACT_APP_API_URL;

const user =
  JSON.parse(localStorage.getItem('user')) &&
  JSON.parse(localStorage.getItem('user'));

const ExtraServicesContextProvider = ({ children, match }) => {
  const [categories, setCategories] = useState([]);
  const [extraServicesTotal, setExtraServicesTotal] = useState(0);
  const [selectedId, setSelectedId] = useState('');
  const [loading, setLoading] = useState(false);

  const getCategories = async (id) => {
    try {
      setLoading(true);

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.stsTokenManager.accessToken}`,
        },
      };

      const { data } = await axios.get(`${API_KEY}/category/${id}`, config);

      setCategories(
        data.data.map((category) => ({ ...category, checked: false }))
      );

      setLoading(false);
    } catch (error) {
      setLoading(false);
      console.log(error.message);
    }
  };

  const onHandleClick = (id) => {
    setCategories(
      categories.map((category) =>
        category.selfID === id
          ? { ...category, checked: !category.checked }
          : category
      )
    );
    getSelectedIndex();
  };

  const getCategoriesTotal = (categories) => {
    let sum = 0;

    categories.forEach((category) => {
      if (category.checked) {
        sum += category.price;
      }
    });

    setExtraServicesTotal(sum);
  };

  const getSelectedIndex = () => {
    let categoryIndex = '';

    categories.forEach((category) => {
      if (category.checked) {
        categoryIndex += category.selfID + ', ';
      }
    });
    const editedId = categoryIndex.slice(0, -2);

    setSelectedId(editedId);
  };

  return (
    <ExtraServicesContext.Provider
      value={{
        categories,
        getCategories,
        onHandleClick,
        extraServicesTotal,
        getCategoriesTotal,
        selectedId,
        getSelectedIndex,
        loading,
      }}
    >
      {children}
    </ExtraServicesContext.Provider>
  );
};

export default ExtraServicesContextProvider;
