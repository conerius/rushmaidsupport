import React, { createContext, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';

export const AdditionalQuestionsContext = createContext();

const AdditionalQuestionsContextProvider = ({ children }) => {
  const [pets, setPets] = useState(true);
  const [airBnb, setAirbnb] = useState(true);
  const [questionKey, setQuestionKey] = useState('');
  const [questionUrgent, setQuestionUrgent] = useState('');
  const [questionEntry, setQuestionEntry] = useState('');
  const [questionSupplies, setQuestionSupplies] = useState('');
  const [notes, setNotes] = useState('');
  const [questionList, setQuestionList] = useState({});

  const setPetsTrue = () => {
    setPets(true);
  };

  const setPetsFalse = () => {
    setPets(false);
  };

  const setAirBnbTrue = () => {
    setAirbnb(true);
  };

  const setAirBnbFalse = () => {
    setAirbnb(false);
  };

  const questionsAll = () => {
    const q1 = uuidv4();
    const q2 = uuidv4();
    const q3 = uuidv4();
    const q4 = uuidv4();
    const q5 = uuidv4();
    const q6 = uuidv4();
    const q7 = uuidv4();

    const questions = [
      {
        answer: pets ? 'Yes' : 'No',
        index: 0,
      },
      {
        answer: questionKey,
        index: 1,
      },
      {
        answer: questionUrgent,
        index: 2,
      },
      {
        answer: questionEntry,
        index: 3,
      },
      {
        answer: questionSupplies,
        index: 4,
      },
      {
        answer: notes,
        index: 5,
      },
      {
        answer: airBnb ? 'Yes' : 'No',
        index: 6,
      },
    ];

    let questionsMap = new Map();
    questionsMap[q1] = questions[0];
    questionsMap[q2] = questions[1];
    questionsMap[q3] = questions[2];
    questionsMap[q4] = questions[3];
    questionsMap[q5] = questions[4];
    questionsMap[q6] = questions[5];
    questionsMap[q7] = questions[6];

    setQuestionList(questionsMap);
  };

  return (
    <AdditionalQuestionsContext.Provider
      value={{
        questionKey,
        questionUrgent,
        questionEntry,
        questionSupplies,
        setQuestionKey,
        setQuestionUrgent,
        setQuestionEntry,
        setQuestionSupplies,
        notes,
        setNotes,
        pets,
        setPetsTrue,
        setPetsFalse,
        airBnb,
        setAirBnbTrue,
        setAirBnbFalse,
        questionList,
        questionsAll,
      }}
    >
      {children}
    </AdditionalQuestionsContext.Provider>
  );
};

export default AdditionalQuestionsContextProvider;
