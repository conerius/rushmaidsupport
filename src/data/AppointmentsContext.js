import React, { createContext, useState } from 'react';
import axios from 'axios';

export const AppointmentsContext = createContext();

const API_KEY = process.env.REACT_APP_API_URL;

const user =
    JSON.parse(localStorage.getItem('user')) &&
    JSON.parse(localStorage.getItem('user'));

const AppointmentsContextProvider = ({ children }) => {
    const [appointments, setAppointments] = useState([]);

    const getAppointments = async () => {
        try {

            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${user.stsTokenManager.accessToken}`,
                }
            }

            const { data } = await axios.get(
                `${API_KEY}/booking/${user.uid}`,
                config
            );

            setAppointments(data.data);

        } catch (error) {
            console.log(error.message);
        }
    }

    return (
        <AppointmentsContext.Provider
            value={{
                appointments,
                getAppointments
            }}
        >
            {children}
        </AppointmentsContext.Provider>
    )
}

export default AppointmentsContextProvider;

