import React, { createContext, useState, useEffect } from 'react';
import axios from 'axios';
import moment from 'moment';

import bedroom from '../assets/icons/bedroom.svg';
import bathroom from '../assets/icons/bathroom.svg';
import kitchen from '../assets/icons/kitchen.svg';
import livingArea from '../assets/icons/bed.svg';
import balcony from '../assets/icons/balcony.svg';

import NProgress from 'nprogress';

const user =
  JSON.parse(localStorage.getItem('user')) &&
  JSON.parse(localStorage.getItem('user'));

export const BookingContext = createContext();

const API_URL = process.env.REACT_APP_API_URL;

const BookingContextProvider = ({ children }) => {
  const [tabs, setTabs] = useState([
    {
      id: 1,
      title: 'Bedroom',
      icon: bedroom,
      num: 1,
      price: 60,
      total: 60,
      number: 1,
    },
    {
      id: 2,
      title: 'Bathroom',
      icon: bathroom,
      num: 1,
      price: 60,
      total: 60,
      number: 1,
    },
    {
      id: 3,
      title: 'Kitchen',
      icon: kitchen,
      num: 0,
      price: 10,
      total: 0,
      number: 0,
    },
    {
      id: 4,
      title: 'Living Area',
      icon: livingArea,
      num: 0,
      price: 10,
      total: 0,
      number: 0,
    },
    {
      id: 5,
      title: 'Balcony',
      icon: balcony,
      num: 0,
      price: 10,
      total: 0,
      number: 0,
    },
  ]);
  const dateNow = moment();
  const [date, setDate] = useState(dateNow);
  const [time, setTime] = useState(moment());
  const [totalCleaning, setTotalCleaning] = useState(120);
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);

  const setMessage = (data, history) => {
    const confirm = window.confirm(data.message);
    if (confirm) {
      // window.open(data.data, "_blank");
      history.push('/AllAppointments');
    } else {
      return false;
    }
  };

  const format = 'HH:mm A';

  const increment = (id) => {
    setTabs(
      tabs.map((tab) => {
        if (tab.id === id) {
          if (tab.id === 1 || tab.id === 2) {
            return {
              ...tab,
              num: tab.num + 1,
              total:
                tab.num === 1
                  ? (tab.total += 35)
                  : tab.num >= 5
                  ? (tab.total += 40)
                  : (tab.total += 35),
            };
          } else {
            return {
              ...tab,
              num: tab.num + 1,
              total: (tab.total += tab.price),
            };
          }
        } else {
          return tab;
        }
      })
    );
  };

  const decrement = (id) => {
    setTabs(
      tabs.map((tab) => {
        if (tab.id === id) {
          if (tab.id === 1 || tab.id === 2) {
            return {
              ...tab,
              num: tab.num !== tab.number ? tab.num - 1 : tab.number,
              total:
                tab.num === 1
                  ? tab.price
                  : tab.num >= 5
                  ? (tab.total -= 40)
                  : (tab.total -= 35),
            };
          } else {
            return {
              ...tab,
              num: tab.num !== tab.number ? tab.num - 1 : tab.number,
              total: tab.num === 0 ? tab.price : (tab.total -= 10),
            };
          }
        } else {
          return tab;
        }
      })
    );
  };

  const getTotalCleaning = (tabs, history) => {
    let total = 0;

    tabs.forEach((tab) => {
      total = total + tab.total;
    });

    setTotalCleaning(total);

    history.push(`/ExtraServices/${tabs[0].num}`);
  };

  const createBooking = async (
    customer,
    numOfBedrooms,
    numOfBathrooms,
    numOfKitchens,
    numOfLivingAreas,
    numOfBalconies,
    additionalServiceIDs,
    address,
    lng,
    lat,
    price,
    questions,
    history,
    promoCodeId,
    zipCode
  ) => {
    const timestamp = moment(date).unix();

    setLoading(true);

    try {
      const booking = {
        stripe: {
          amount: 50,
          source: customer.default_source,
          description: 'iOS customer booking',
          receiptEmail: user.email,
          customer: customer.id,
        },
        booking: {
          date: timestamp,
          address: address,
          longitude: lng,
          latitude: lat,
          numberOfBedrooms: numOfBedrooms,
          numberOfBathrooms: numOfBathrooms,
          numberOfKitchens: numOfKitchens,
          numberOfLivingAreas: numOfLivingAreas,
          numberOfBalconies: numOfBalconies,
          additionalServiceIDs: additionalServiceIDs,
          price: price,
          userUID: user.uid,
          status: 'notCleaning',
          details: '',
          promo_code_id: promoCodeId ? promoCodeId : 'autoID',
          questions: questions,
          zipCode: zipCode.toString(),
        },
      };

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.stsTokenManager.accessToken}`,
        },
      };

      console.log(booking);

      const { data } = await axios.post(
        `${API_URL}/booking/create`,
        booking,
        config
      );

      setLoading(false);

      const result = await axios.get(
        `${API_URL}/booking/receipt/${data.data}`,
        config
      );

      setMessage(result.data, history);

      setError('');
    } catch (error) {
      setLoading(false);
      setError(error.response.data.message);
    }
  };

  const createFreeBooking = async (
    numOfBedrooms,
    numOfBathrooms,
    numOfKitchens,
    numOfLivingAreas,
    numOfBalconies,
    additionalServiceIDs,
    address,
    lng,
    lat,
    price,
    questions,
    history,
    promoCodeId,
    zipCode
  ) => {
    const timestamp = moment(date).unix();

    setLoading(true);

    try {
      const booking = {
        booking: {
          date: timestamp,
          address: address,
          longitude: lng,
          latitude: lat,
          numberOfBedrooms: numOfBedrooms,
          numberOfBathrooms: numOfBathrooms,
          numberOfKitchens: numOfKitchens,
          numberOfLivingAreas: numOfLivingAreas,
          numberOfBalconies: numOfBalconies,
          additionalServiceIDs: additionalServiceIDs,
          price: price,
          userUID: user.uid,
          status: 'notCleaning',
          details: '',
          promo_code_id: promoCodeId,
          questions: questions,
          zipCode: zipCode.toString(),
        },
      };

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.stsTokenManager.accessToken}`,
        },
      };

      const { data } = await axios.post(
        `${API_URL}/booking/create-payless`,
        booking,
        config
      );

      setLoading(false);

      const result = await axios.get(
        `${API_URL}/booking/receipt/${data.data}`,
        config
      );

      setMessage(result.data, history);

      setError('');
    } catch (error) {
      setLoading(false);
      console.log(error);
      setError(error.response.data.message);
    }
  };

  return (
    <BookingContext.Provider
      value={{
        tabs,
        increment,
        decrement,
        date,
        setDate,
        time,
        setTime,
        format,
        createBooking,
        createFreeBooking,
        totalCleaning,
        getTotalCleaning,
        error,
        setError,
        loading,
      }}
    >
      {children}
    </BookingContext.Provider>
  );
};

export default BookingContextProvider;
