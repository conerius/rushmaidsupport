import React, { createContext, useState, useContext } from 'react';
import axios from 'axios';
import moment from 'moment';

export const NewDisinfectionContext = createContext();

const API_KEY = process.env.REACT_APP_API_URL;

const user =
  JSON.parse(localStorage.getItem('user')) &&
  JSON.parse(localStorage.getItem('user'));

const dateNow = moment();

const NewDisinfectionContextProvider = ({ children }) => {
  const [value, setValue] = useState(20);
  const [totalPrice, setTotalPrice] = useState(value * 0.2);
  const [date, setDate] = useState(dateNow);
  const [disinfection, setDisinfection] = useState({});
  const [address, setAddress] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [coordinates, setCoordinates] = useState({
    lat: null,
    lng: null,
  });
  const [error, setError] = useState('');

  const setMessage = (data, history) => {
    const confirm = window.confirm(data.message);
    if (confirm) {
      // window.open(data.data, "_blank");
      history.push('/AllAppointments');
    } else {
      return false;
    }
  };

  const createDisinfection = async (
    customer,
    promoCodeId,
    zipCode,
    history
  ) => {
    try {
      setIsLoading(true);

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.stsTokenManager.accessToken}`,
        },
      };

      const timestamp = moment(date).unix();

      const disinfectionInfo = {
        date: timestamp,
        feets: value,
        address: address,
        longitude: coordinates.lng,
        latitude: coordinates.lat,
        userUID: user.uid,
        credit: user.credit ? user.credit : 0,
        receipt_email: user.email,
        description: 'iOS customer disinfection booking',
        customer_id: customer.id,
        source_id: customer.default_source,
        promo_code_id: promoCodeId ? promoCodeId : 'promoID123',
        price: totalPrice,
        zip_code: zipCode.toString(),
      };

      const { data } = await axios.post(
        `${API_KEY}/disinfection/create`,
        disinfectionInfo,
        config
      );

      setDisinfection(data.data);

      setIsLoading(false);

      const result = await axios.get(
        `${API_KEY}/booking/receipt/${data.data.disinfectionId}`,
        config
      );

      setMessage(result.data, history);

      setAddress('');
    } catch (error) {
      setIsLoading(false);
      setError(error.response.data.message);
    }
  };

  const createFreeDisinfection = async (promoCodeId, zipCode, history) => {
    try {
      setIsLoading(true);

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.stsTokenManager.accessToken}`,
        },
      };

      const timestamp = moment(date).unix();

      const disinfectionInfo = {
        date: timestamp,
        feets: value,
        address: address,
        longitude: coordinates.lng,
        latitude: coordinates.lat,
        userUID: user.uid,
        promo_code_id: promoCodeId,
        zip_code: zipCode.toString(),
      };

      const { data } = await axios.post(
        `${API_KEY}/disinfection/create-payless`,
        disinfectionInfo,
        config
      );

      setDisinfection(data.data);

      setIsLoading(false);

      const message = {
        message: 'Successfully created disinfection!',
      };

      setMessage(message, history);
    } catch (error) {
      setIsLoading(false);
      setError(error.response.data.message);
    }
  };

  return (
    <NewDisinfectionContext.Provider
      value={{
        createDisinfection,
        createFreeDisinfection,
        value,
        setValue,
        date,
        setDate,
        disinfection,
        totalPrice,
        setTotalPrice,
        address,
        setAddress,
        coordinates,
        setCoordinates,
        error,
        setError,
        isLoading,
      }}
    >
      {children}
    </NewDisinfectionContext.Provider>
  );
};

export default NewDisinfectionContextProvider;
