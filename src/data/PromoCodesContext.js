import React, { useState, useContext, createContext } from 'react';
import { CompleteBookingContext } from './CompleteBookingContext';
import axios from 'axios';

import NProgress from 'nprogress';

export const PromoCodesContext = createContext();

const API_KEY = process.env.REACT_APP_API_URL;

const user =
    JSON.parse(localStorage.getItem('user')) &&
    JSON.parse(localStorage.getItem('user'));

const PromoCodesContextProvider = ({ children }) => {
    const [discount, setDiscount] = useState(0);
    const [promoCode, setPromoCode] = useState('');
    const [promoCodeId, setPromoCodeId] = useState('');
    const [error, setError] = useState('');

    const fetchPromoCode = async () => {
        const result = await getPromoCode();

        if (result[0].code === promoCode) {
            alert(`You have discount of ${result[0].discount}% !`);
            setDiscount(result[0].discount);
            setPromoCodeId(result[0].selfID);
        } else {
            alert(`You dont have discount`);
            setDiscount(0);
            setPromoCodeId('');
            setPromoCode('');
        }
    }

    const getPromoCode = async () => {
        const config = {
            headers: {
                Authorization: `Bearer ${user.stsTokenManager.accessToken}`,
            }
        }

        const { data } = await axios.get(
            `${API_KEY}/booking/promo-codes/${user.uid}`,
            config
        );

        return data.data;
    }

    return (
        <PromoCodesContext.Provider
            value={{
                discount,
                setDiscount,
                promoCode,
                setPromoCode,
                promoCodeId,
                setPromoCodeId,
                fetchPromoCode
            }}
        >
            {children}
        </PromoCodesContext.Provider>
    )
}

export default PromoCodesContextProvider;

