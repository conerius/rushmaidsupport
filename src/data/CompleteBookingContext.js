import React, { createContext, useState } from 'react';

export const CompleteBookingContext = createContext();

const CompleteBookingContextProvider = ({ children }) => {
    const [totalPrice, setTotalPrice] = useState(0);

    const getTotalPrice = (pets, airBnb, totalCleaning, extraServicesTotal, discount) => {
        // pets
        //     ?
        setTotalPrice(totalCleaning + extraServicesTotal + (pets ? 10 : 0) + (airBnb ? 10 : 0))
        //     :
        //     setTotalPrice(totalCleaning + extraServicesTotal);


        if (discount) {
            let totalValue = ((discount / 100) * totalPrice);
            setTotalPrice(totalPrice - totalValue);
        }
    }

    return (
        <CompleteBookingContext.Provider
            value={{
                totalPrice,
                getTotalPrice,
                setTotalPrice
            }}
        >
            {children}
        </CompleteBookingContext.Provider>
    )
}

export default CompleteBookingContextProvider;

