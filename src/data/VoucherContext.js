import React, { createContext, useState } from 'react';
import axios from 'axios';

import NProgress from 'nprogress';

export const VoucherContext = createContext();

const API_KEY = process.env.REACT_APP_API_URL;

const user =
  JSON.parse(localStorage.getItem('user')) &&
  JSON.parse(localStorage.getItem('user'));

const customer =
  JSON.parse(localStorage.getItem('customer')) &&
  JSON.parse(localStorage.getItem('customer'));

const VoucherContextProvider = ({ children }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState('');

  return (
    <VoucherContext.Provider
      value={{
        createVoucher,
        error,
        setError,
      }}
    >
      {children}
    </VoucherContext.Provider>
  );
};

export default VoucherContextProvider;
