import React, { createContext, useState } from 'react';
import axios from 'axios';
import { geocodeByAddress, getLatLng } from 'react-places-autocomplete';

export const LocationContext = createContext();

const user =
  JSON.parse(localStorage.getItem('user')) &&
  JSON.parse(localStorage.getItem('user'));

const API_URL = process.env.REACT_APP_API_URL;

const LocationContextProvider = ({ children }) => {
  const [loading, setLoading] = useState(false);
  const [zipCode, setZipCode] = useState('');
  const [zipCodeResponse, setZipCodeResponse] = useState({});
  const [address, setAddress] = useState('');
  const [coordinates, setCoordinates] = useState({
    lat: null,
    lng: null,
  });

  const getZipCodeStatus = async (zipCode) => {
    try {
      setLoading(true);

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.stsTokenManager.accessToken}`,
        },
      };

      const { data } = await axios.get(
        `${API_URL}/booking/zip-code-availability/${zipCode}`,
        config
      );

      setLoading(false);
      // setZipCodeResponse(data);
      // setZipCode(zipCode);
      return data;
    } catch (error) {
      setLoading(false);
      return error.response.data;
    }
  };

  const handleSelectComplete = async (value) => {
    // setError("");

    const result = await geocodeByAddress(value);
    const latlng = await getLatLng(result[0]);

    setAddress(value);
    setCoordinates(latlng);
  };

  return (
    <LocationContext.Provider
      value={{
        getZipCodeStatus,
        zipCode,
        setZipCode,
        handleSelectComplete,
        address,
        setAddress,
        coordinates,
        zipCodeResponse,
        loading,
      }}
    >
      {children}
    </LocationContext.Provider>
  );
};

export default LocationContextProvider;
