import React, { createContext, useState } from 'react';
import axios from 'axios';

export const ProfileContext = createContext();

const API_KEY = process.env.REACT_APP_API_URL;

const userFromLocalStorage =
  JSON.parse(localStorage.getItem('user')) &&
  JSON.parse(localStorage.getItem('user'));

const ProfileContextProvider = ({ children }) => {
  const [user, setUser] = useState({});
  const [profileImage, setProfileImage] = useState('');
  const [error, setError] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const getUser = async () => {
    try {
      setIsLoading(true);
      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${userFromLocalStorage.stsTokenManager.accessToken}`,
        },
      };

      const { data } = await axios.get(
        `${API_KEY}/user/${userFromLocalStorage.uid}`,
        config
      );

      setUser(data.data);

      setIsLoading(false);
    } catch (error) {
      setError(error.response.data.message);
    }
  };

  const updateProfileImage = async (extension, image, user) => {
    try {
      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${userFromLocalStorage.stsTokenManager.accessToken}`,
        },
      };

      const imageData = {
        extension: extension,
        data: image,
        userId: user.uid,
      };

      const { data } = await axios.post(
        `${API_KEY}/user/image`,
        imageData,
        config
      );

      setProfileImage(data.data);
    } catch (error) {
      setError(error.response.data.message);
    }
  };

  const updateUserProfile = async ({
    firstName,
    lastName,
    email,
    userId,
    imageUrl,
  }) => {
    try {
      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${userFromLocalStorage.stsTokenManager.accessToken}`,
        },
      };

      const userData = {
        firstName: firstName,
        lastName: lastName,
        email: email,
        zipCode: 34000,
        userId: userId,
        imageUrl: imageUrl,
      };

      const { data } = await axios.put(`${API_KEY}/user`, userData, config);

      // localStorage.setItem('userInfo', JSON.stringify(data.data));

      getUser(data.data.userId);
    } catch (error) {
      setError(error.response.data.message);
    }
  };

  return (
    <ProfileContext.Provider
      value={{
        user,
        profileImage,
        updateProfileImage,
        updateUserProfile,
        error,
        setError,
        getUser,
      }}
    >
      {children}
    </ProfileContext.Provider>
  );
};

export default ProfileContextProvider;
