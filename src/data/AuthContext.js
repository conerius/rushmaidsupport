import React, { createContext, useState } from 'react';
import axios from 'axios';

import { auth, database, authUninvoked } from '../config/firebase';

const API_KEY = process.env.REACT_APP_API_URL;

export const AuthContext = createContext();

const AuthContextProvider = (props) => {
  const [authError, setAuthError] = useState(null);
  const [authErrorFB, setAuthErrorFB] = useState('');
  const [authErrorAPPLE, setAuthErrorAPPLE] = useState('');
  const [loading, setLoading] = useState(false);

  const signInRegular = async (email, password) => {
    try {
      setLoading(true);

      const user = await auth.signInWithEmailAndPassword(email, password);

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.user.za}`,
        },
      };

      const { data } = await axios.get(
        `${API_KEY}/user/${user.user.uid}`,
        config
      );

      if (data.data.role === 'user') {
        if (!data.data.phoneVerified) {
          window.location.href = '/phoneNumber';
        } else {
          window.location.href = '/';
        }
        localStorage.setItem('user', JSON.stringify(user.user));
      } else if (data.data.role === 'admin') {
        setAuthError('This account is not allowed to log in with admin role');
      } else if (data.data.role === 'maid') {
        setAuthError('This account is not allowed to log in with maid role');
      }
      setLoading(false);
    } catch (error) {
      setLoading(false);
      setAuthError(error.message);
    }
  };

  //////////////////////////SIGN UP//////////////////////////

  const asyncLocalStorage2 = {
    setItem: function (user) {
      return Promise.resolve().then(function () {
        localStorage.setItem('user', JSON.stringify(user));
      });
    },
  };

  const signUp = (email, password, userObject) => {
    auth
      .createUserWithEmailAndPassword(email, password)
      .then((data) => {
        const {
          user: { uid },
        } = data;
        const usersRef = database.ref('users');
        delete userObject.password;
        usersRef
          .child(uid)
          .set({
            ...userObject,
            uid,
            role: 'user',
          })
          .then(() => {
            asyncLocalStorage2.setItem(data.user).then(function () {
              window.location.href = '/zipCode';
            });
          })
          .catch((e) => {
            console.log(e.message);
            setAuthError(e.message);
          });
      })
      .catch((e) => {
        console.log(e.message);
        setAuthError(e.message);
      });
  };
  ////////////////////////////////////////////////////

  ////////////////////LOGOUT//////////////////////

  const asyncLocalStorageRemove = {
    setItem: function (user) {
      return Promise.resolve().then(function () {
        localStorage.removeItem(user);
      });
    },
  };

  const logout = () => {
    auth.signOut().then(() => {
      asyncLocalStorageRemove.setItem('user').then(function () {
        window.location.href = '/';
      });
    });
  };

  ///////////////////////////////////////////////

  const recoverPassword = (email) => {
    auth
      .sendPasswordResetEmail(email)
      .then(() => {
        setAuthError(
          'We sent you a password reset link. Please check your email.'
        );
      })
      .then(() => {
        setTimeout(() => {
          window.location.href = '/signin';
        }, 5000);
      })
      .catch(function (e) {
        console.log(e.message);
        setAuthError(e.message);
      });
  };

  const changePassword = (currentPassword, newPassword) => {
    auth.onAuthStateChanged(() => {
      const user = auth.currentUser;
      const credential = authUninvoked.EmailAuthProvider.credential(
        user.email,
        currentPassword
      );
      user
        .reauthenticateWithCredential(credential)
        .then(function () {
          user
            .updatePassword(newPassword)
            .then(function () {
              setAuthError(null);
            })
            .catch(function (error) {
              setAuthError(error.message);
            });
        })
        .catch(function (error) {
          setAuthError(error.message);
        });
    });
  };

  //////////////////////FACEBOOK, APPLE AUTH///////////////////////////////

  const facebookLogin = () => {
    let provider = new authUninvoked.FacebookAuthProvider();
    auth
      .signInWithPopup(provider)
      .then((result) => {
        console.log(result);
        let firstName;
        let lastName;
        let imageURL;
        let email;
        let uid;
        let sts;
        let apiKey;

        firstName =
          result.additionalUserInfo.profile.first_name ||
          result.additionalUserInfo.profile.name ||
          result.user.displayName;
        lastName = result.additionalUserInfo.profile.last_name;
        imageURL = result.additionalUserInfo.profile.picture.data.url;
        email = result.additionalUserInfo.profile.email;
        uid = result.user.uid;

        sts = result.credential.accessToken;
        apiKey = result.user.l;

        let userObject = {
          firstName,
          lastName,
          imageURL,
          email,
          uid,
        };

        let name = firstName + ' ' + lastName;

        if (result.additionalUserInfo.isNewUser) {
          const usersRef = database.ref('users');

          usersRef
            .child(uid)
            .set({
              ...userObject,
              uid,
              role: 'user',
            })
            .then(() => {
              asyncLocalStorage
                .setItem(email, uid, sts, apiKey, name)
                .then(function () {
                  window.location.href = '/zipCode';
                });
            })
            .catch((e) => {
              console.log(e.message);
            });
        } else {
          asyncLocalStorage
            .setItem(email, uid, sts, apiKey, name)
            .then(function () {
              window.location.href = '/';
            });
        }
      })
      .catch((error) => {
        setAuthErrorFB(error.message);
      });
  };

  const asyncLocalStorage = {
    setItem: function (ema, id, sts, apiKey, name) {
      return Promise.resolve().then(function () {
        localStorage.setItem(
          'user',
          JSON.stringify({
            email: ema,
            uid: id,
            stsTokenManager: {
              accessToken: sts,
              apiKey: apiKey,
            },
            displayName: name,
          })
        );
      });
    },
  };

  const appleLogin = () => {
    var provider = new authUninvoked.OAuthProvider('apple.com');

    auth
      .signInWithPopup(provider)
      .then((result) => {
        let user = result.user;
        console.log(result);
        if (result.additionalUserInfo.isNewUser) {
          let email = user.email;
          let firstName = user.displayName.split(' ').slice(0, -1).join(' ');
          let lastName = user.displayName.split(' ').slice(-1).join(' ');
          let uid = result.user.uid;
          let userObject = {
            firstName,
            lastName,
            email,
          };
          let name = user.displayName;
          let sts = result.credential.accessToken;
          let apiKey = result.user.l;

          const usersRef = database.ref('users');

          usersRef
            .child(uid)
            .set({
              ...userObject,
              uid,
              role: 'user',
            })
            .then(() => {
              asyncLocalStorage
                .setItem(email, uid, sts, apiKey, name)
                .then(function () {
                  window.location.href = '/zipCode';
                });
            })
            .catch((e) => {
              console.log(e.message);
            });
        } else {
          let name = user.displayName;
          let email = user.email;
          let uid = result.user.uid;
          let sts = result.credential.accessToken;
          let apiKey = result.user.l;

          asyncLocalStorage
            .setItem(email, uid, sts, apiKey, name)
            .then(function () {
              window.location.href = '/';
            });
        }
      })
      .catch((error) => {
        setAuthErrorAPPLE(error.message);
        var errorCode = error.code;
        var errorMessage = error.message;
        var email = error.email;
        var credential = error.credential;
        console.log(errorCode + errorMessage + email + credential);
      });
  };

  ////////////////////////////////////////////////////////////////

  return (
    <AuthContext.Provider
      value={{
        authError,
        authErrorFB,
        authErrorAPPLE,
        // signIn,
        logout,
        recoverPassword,
        signUp,
        database,
        changePassword,
        signInRegular,
        facebookLogin,
        appleLogin,
        loading,
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthContextProvider;
