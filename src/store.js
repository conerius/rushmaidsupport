import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { userInfo } from './reducers/authReducers';
// import { categories } from './reducers/categoriesReducers';
// import { bookings } from './reducers/bookingReducers';
// import { additionalQuestions } from './reducers/additionalQuestionReducers';
// import { promoCodes } from './reducers/promoCodeReducers';
// import { stripeInfo } from './reducers/stripeReducers';
// import { disinfections } from './reducers/disnfectionReducers';
import { appointments } from './reducers/appointmentReducers';
import { maids } from './reducers/maidReducers';
import { customers } from './reducers/customersReducers';
import { unavailability } from './reducers/unavailabilityReducers';
import { conversations } from './reducers/chatReducers';

const reducer = combineReducers({
  userInfo,
  // stripeInfo,
  // bookings,
  // categories,
  // additionalQuestions,
  // promoCodes,
  // disinfections,
  appointments,
  maids,
  customers,
  unavailability,
  conversations,
});

const userFromStorage = localStorage.getItem('userRushmaidSupport')
  ? JSON.parse(localStorage.getItem('userRushmaidSupport'))
  : null;

const customer = localStorage.getItem('customer')
  ? JSON.parse(localStorage.getItem('customer'))
  : null;

const initialState = {
  userInfo: {
    user: userFromStorage,
  },
  stripeInfo: {
    customerId: customer,
  },
};

const middleware = [thunk];

const store = createStore(
  reducer,
  initialState,
  composeWithDevTools(applyMiddleware(...middleware))
);

export default store;
